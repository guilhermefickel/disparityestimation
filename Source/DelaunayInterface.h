#ifndef DELAUNAYINTERFACE_H
#define DELAUNAYINTERFACE_H

#include <iostream>
#include "DataStructure/point_t.h"
#include "DataStructure/edge.h"
#include "DataStructure/triang.h"
#include "DataStructure/FastVec.h"
#include <vector>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_hierarchy_2.h>
#include <CGAL/Constrained_triangulation_plus_2.h>

//typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
//typedef Kernel::Point_2 Point;
//typedef CGAL::Delaunay_triangulation_2<Kernel> Delaunay;
//typedef Delaunay::Vertex_handle Vertex_handle;


typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_2<K> Vbb;
typedef CGAL::Triangulation_hierarchy_vertex_base_2<Vbb> Vb;
typedef CGAL::Constrained_triangulation_face_base_2<K> Fb;
typedef CGAL::Triangulation_data_structure_2<Vb,Fb> TDS;
typedef CGAL::Exact_predicates_tag Itag;
typedef CGAL::Constrained_Delaunay_triangulation_2<K,TDS,Itag> CDT;
typedef CGAL::Triangulation_hierarchy_2<CDT> CDTH;
typedef CGAL::Constrained_triangulation_plus_2<CDTH> Delaunay;
typedef Delaunay::Vertex_handle Vertex_handle;
typedef K::Point_2 Point;


class DelaunayInterface {
private:
    Delaunay* dt;
	
	FastVec<point_t>* pts_vec;
	FastVec<edge>* edges_vec;
	FastVec<triang>* triangs_vec;

    Delaunay::Vertex_handle nearest_vertex( Point p ) ;

public:
	DelaunayInterface();
	~DelaunayInterface();


	// called only one time
    void config( FastVec<point_t>* pts_vec, FastVec<edge>* edges_vec, FastVec<triang>* triangs_vec );
    Delaunay* updateTriang( FastVec<point_t>* pts_vec, vector<point_t>* pts, vector<int>* ptsToRm );
	Delaunay* updateTriang( vector<point_t>* pts  );

};

#endif
