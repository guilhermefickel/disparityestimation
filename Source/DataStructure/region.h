#ifndef REGION_H
#define REGION_H

#include <vector>

class region {
public:
	region* ptrFwd;
	region* ptrBack;
	int idx;
	int idxPos;

    int inverseIdx; // find what is the position of a given region idx. I.e. give idx 556 and discover that it is the third region within ptrList.

	int x[3];
	int y[3];
	float z[3];
	float zRef[3]; // z after refinement
	float zClosed[3]; // z after closing the mesh
	float zTemp[3]; // 
	float zTempRef[3]; // 
    double zKalmanX[3];
    double zKalmanP[3];
	int ptIdx[3];
	int numPts; // number os pts, i.e. its area

	float meanRGB[3]; // mean value for each channel, used in refinement step
    float meanLab[3]; // mean value for each channel, used in refinement step
    float initPerimeter;
    float initArea;
	bool used; // was deleted?
	bool stable; // is temporally stable?
    int age;

    double kalmanX;
    double kalmanP;

    std::vector<float> depth;
    std::vector< std::vector<float> > probs;
    std::vector< std::vector<float> > probsAgg;
    std::vector< std::vector<float> > probsTime;
    std::vector< std::vector<float> > ratioCost;  // confidence in the match, expressed by the ratio(gain) of the pixels values
    std::vector<float> sum_costs;
    std::vector<float> sum_costs_agg;
    std::vector<float> sum_costs_time;
    std::vector<float> meanR;
    std::vector<float> meanG;
    std::vector<float> meanB;
    std::vector<float> meanL;
    std::vector<float> meana;
    std::vector<float> meanb;


	region();
	~region();

    region& operator=(const region &rhs);
} ;

#endif
