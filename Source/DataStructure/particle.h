#ifndef PARTICLE_H
#define PARTICLE_H

#include <cstdlib>
#include <vector>
#include "../Aux/Constants.h"

class particle {
public:
    particle* ptrFwd;
    particle* ptrBack;
	int idx;
	int idxPos;
    //int nonDiscPt; // if this is a point within a disparity discontinuity, what is the closest pt
				   // that is not in a disparity discontinuity.

    std::vector<float> x;
    std::vector<float> y;

    std::vector<float> R;
    std::vector<float> G;
    std::vector<float> B;

    int beginTime;

    particle();
    ~particle();
	
    particle& operator=(const particle &rhs);
					
};

#endif
