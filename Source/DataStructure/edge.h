#ifndef EDGE_H
#define EDGE_H

class edge {
public:
	edge* ptrFwd;
	edge* ptrBack;
	int idx;
	int idxPos;

	float weightLink; // weight link used in particle tracking.
	int points_vec[2];
	int triangs_vec[2];
	int numTriangs;  // used to control triangs_vec list.
	bool used;
    bool constrained;

	edge();
	~edge();

    edge& operator=(const edge &rhs);
};

#endif
