#include "triang.h"

triang::triang() {
	for(int i=0; i<MAX_NUM_THREADS; i++)
		mark[i] = false;
	used=false;
	stillExists = false;
}

triang::~triang() {

}

triang& triang::operator=(const triang &rhs) {
	// Only do assignment if RHS is a different object from this.
	if (this != &rhs) {
		for(int i=0; i<MAX_NUM_THREADS; i++)
			this->mark[i] = rhs.mark[i];

		this->stillExists = rhs.stillExists;
		this->used = rhs.used;
        this->lastFrame = rhs.lastFrame;
        this->firstFrame = rhs.firstFrame;
		
		for(int i=0; i<3; i++) {
			this->points_vec[i] = rhs.points_vec[i];
			this->edges_vec[i] = rhs.edges_vec[i];
		}

	}

	return *this;
}
