#include "particle.h"

particle::particle() {

}

particle::~particle() {

}

particle& particle::operator=(const particle &rhs) {
	// Only do assignment if RHS is a different object from this.
	if (this != &rhs) {
        this->idx = rhs.idx;
        this->idxPos = rhs.idxPos;
        this->ptrFwd = rhs.ptrFwd;
        this->ptrBack = rhs.ptrBack;

		this->x = rhs.x;
		this->y = rhs.y;

        this->R = rhs.R;
        this->G = rhs.G;
        this->B = rhs.B;

        this->beginTime = rhs.beginTime;
	}

	return *this;
}
