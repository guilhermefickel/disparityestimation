#include "point_t.h"

point_t::point_t() {
	x = y = z[0] = z[1] = numEdges = numTriangs = 0; 
	
	isNew = false;
	used = false;
}

point_t::~point_t() {

}

point_t& point_t::operator=(const point_t &rhs) {
	// Only do assignment if RHS is a different object from this.
	if (this != &rhs) {
		this->x = rhs.x;
		this->y = rhs.y;
		this->oldX = rhs.oldX;
		this->oldY = rhs.oldY;
		this->z[0] = rhs.z[0];
		this->z[1] = rhs.z[1];
		this->numEdges = rhs.numEdges;
		this->numTriangs = rhs.numTriangs;
		this->dispDisc = rhs.dispDisc;
		this->nonDiscPt = rhs.nonDiscPt;
		this->used = rhs.used;
		this->isNew = rhs.isNew;
        this->edge = rhs.edge;
        this->radius = rhs.radius;

		for(int i=0; i<rhs.numEdges; i++)
			this->edges_vec[i] = rhs.edges_vec[i];
		for(int i=0; i<rhs.numTriangs; i++)
			this->triangs_vec[i] = rhs.triangs_vec[i];
	}

	return *this;
}
