#ifndef TRIANG_H
#define TRIANG_H

#include "../Aux/Constants.h"

class triang {
public:
	triang* ptrFwd;
	triang* ptrBack;
	int idx;
	int idxPos;

	int edges_vec[3];
	int points_vec[3];
	bool mark[MAX_NUM_THREADS];
	bool used;
	bool stillExists;
    int lastFrame;
    int firstFrame;
    bool bad;
	
	triang();
	~triang();

    triang& operator=(const triang &rhs);
};

#endif
