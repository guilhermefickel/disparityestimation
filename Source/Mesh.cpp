﻿#include "Mesh.h"

Mesh3D::Mesh3D() {
	triOp = new TriangleOperations;
	scaleMap = new ScaleMap;
	refinement = new Refinement;
	delobject = new DelaunayInterface;

	firstRun = true;

    points_vec.reserve(NUM_MAX_POINTS);
    edges_vec.reserve(NUM_MAX_EDGES);
    triangs_vec.reserve(NUM_MAX_TRIANGS);
    orthoTriangs_vec.reserve(NUM_MAX_TRIANGS);

	delobject->config( &points_vec, &edges_vec, &triangs_vec );

	//weightLinks.reserve( NUM_MAX_POINTS*MAX_EDGES_TO_POINT );

	cout << "Warning: allocing " << (MAX_EDGES_TO_POINT*NUM_MAX_POINTS*4 )/1024 << " KBytes only for weightLinks!" << endl;

	// WARNING!!! Must set configs BEFORE use this class!!!!!!!!!
}

Mesh3D::~Mesh3D() {

}

// WARNING!!! Must set configs BEFORE use this class!!!!!!!!! Only call this ONCE!!!
void Mesh3D::setConfig(sbl::Config* config, Raster* raster, FastVec<region>* regions_vec, ImagesDaemon* imgDaemon, ParticleTrack* particleTrack) {
	this->config = config;
	this->raster = raster;
	this->imgDaemon = imgDaemon;
	this->particleTrack = particleTrack;
	this->regions_vec = regions_vec;

	H = imgDaemon->getCenterImg()->rows;
	W = imgDaemon->getCenterImg()->cols;

    debugMode = config->readBool( "debug", false );

	ptsIdx = new int*[H];
	triangsIdx = new int*[H];
	for(int i=0; i<H; i++) {
		ptsIdx[i] = new int[W];
		triangsIdx[i] = new int[W];
		for(int j=0; j<W; j++) {
			ptsIdx[i][j] = -1;
			triangsIdx[i][j] = -1;
		}
	}

	maxDisp = config->readInt( "maxDisp" );

	// I'm just sending all the references of the Mesh3D atributes.
	triOp->setConfig(config, &points_vec, &edges_vec, &triangs_vec, &orthoTriangs_vec, regions_vec, raster, imgDaemon, H, W, ptsIdx, triangsIdx);
	refinement->setConfig(config, regions_vec, &points_vec, &edges_vec, &triangs_vec);
}

void Mesh3D::run( ) {
	int currentTime = imgDaemon->getCurrentTime();
	vector<point_t>* pts;
	int newPtsIdx;

    //cout << "############## Points count: " << points_vec.size() << endl << endl;

	// if first frame (no praticles/vertices yet)
    if( points_vec.size() == 0) {
        pts = scaleMap->firstRun(imgDaemon->getCenterImg());  // calc pts
        triangulate( ); // pass to CGAL, get back, update internal data structure

        particleTrack->addParticles( pts );
	}
	else {
        // check which triangles should stay the same and which one are allowed
        // to change.
        findConstrainedTriangles();
        //findBadPts();

        // update the vertices according to the particle tracker (remove)
        ptsToRM = particleTrack->getPtsToRM( );
        updateVertices( );

        // update Scale Map to add new particles
        pts = scaleMap->update( imgDaemon->getCenterImg(), &points_vec );

		// update triangular regions (color, point order, etc...) AND add
		// new points.
        updateTriangs( pts  );
	}

   // cout << "############## Points count: " << points_vec.size() << endl;
    //cout << "########################################################" << endl << endl;
}

void Mesh3D::triangulate( ) {
	vector<point_t>* pvec;
	int edgePts, borderPts;

	for(int i=0; i<H; i++) {
		for(int j=0; j<W; j++) {
			ptsIdx[i][j] = -1;
			triangsIdx[i][j] = -1;
		}
	}

	// the vertices are provided by the ScaleMap algorithm
	pvec = scaleMap->getPts(edgePts,borderPts);

	dt = delobject->updateTriang( pvec ); // update the delaunay from CGAL
    updateDataStructure( pvec );   // update my data structure

	// adjList used for searching neighbors
	triOp->createAdjList();

	firstRun = false;
}

void Mesh3D::refine() {
	refinement->refineMesh3D(regions_vec, this);
}

void Mesh3D::calcWeightLinks(FastVec<region> &regions_vec, cv::Mat *occMask ) {
	weightLinks.clear();
	vector<int> usedEdges;
	tuple<float,int,int> t;
	vector<int> usedPts;
	vector<int> valPts;
    vector<int> orderedVec;
    vector<int> diffVec;
	bool debugThisIter = false;
	cv::Mat im1 = cv::Mat(MAX_EDGES_TO_POINT,1,CV_8UC1);
	cv::Mat im2 = cv::Mat(MAX_EDGES_TO_POINT,1,CV_8UC1);   // release after!!!!

	for( FastVec<edge>::iterator it_e = edges_vec.begin(); it_e != edges_vec.end(); it_e++ )
        it_e->weightLink = -666;


// STARTING NEW CODE

    // for each point in the mesh
    for( FastVec<point_t>::iterator it_p = points_vec.begin(); it_p != points_vec.end() ; it_p++ ) {
        // for all points conected to it_p

        // now find the closest foreground point for all points within a disparity discontinuitie
        int numPtsOther = 0;
        int secondBestPt;

        // it is NOT a point within a disparity discontinuity? Skip it.
        if( it_p->iter < 2/*|| it_p->edge == false*/ ) {
            it_p->nonDiscPt = it_p->idx;
            it_p->dispDisc = false;
            continue;
        }

        // otherwise, find the point with the most similar disparity (that is not within a disparity discontinuity). If it doesn't find,
        // it will be setted as himself.
        it_p->dispDisc = true;
        it_p->nonDiscPt = it_p->idx;
        secondBestPt = it_p->idx;
        double highDisp = -1;
        for( int e = 0; e < it_p->numEdges; e++ ) {
            // if it is a weak connected point, discard it.
            int p2 = ( edges_vec[ it_p->edges_vec[ e ] ].points_vec[0] == it_p->idx ? edges_vec[ it_p->edges_vec[ e ] ].points_vec[1] :
                       edges_vec[ it_p->edges_vec[ e ] ].points_vec[0] );

            if( points_vec[p2].iter == 2 || occMask->data[ points_vec[p2].y*occMask->step + points_vec[p2].x ] != 0 )
                continue;

            int otherPt = p2;


            if( it_p->idx == otherPt) {
                cout << "Something bad! Weight links!" << endl;
                system("PAUSE");
            }
            if( highDisp < points_vec[p2].z[0] ) {
                highDisp = points_vec[p2].z[0];
                secondBestPt = otherPt;
            }
            // if the optical flow is saying that is in a disparity discontinuity, however if we don't have any other pt...
            // use the one that is within a homogeneouse disparity region. If I don't have any point like this either,
            // then we don't assign any neighbor point (we do this by assigning it to itself).
        }
        //if( it_p->idx == it_p->nonDiscPt )
            it_p->nonDiscPt = secondBestPt;

    }
        //cout << "I've found " << numPtsOther << " points within disparity discontinuity...." << endl;
}

void Mesh3D::updateDataStructure( vector<point_t>* pvec ) {
	Delaunay::Vertex_handle v[3];
	Delaunay::Point pts[3];
	int points[3];
    int H = imgDaemon->getCenterImg()->rows;

    //triOp->clearTriangulation();


    // inserting new points
    for( vector<point_t>::iterator it_p = pvec->begin(); it_p != pvec->end(); it_p++ ) {
            triOp->insert_point_xy( it_p->x, it_p->y, it_p->radius, -1, it_p->edge );
    }

    //if( abs( (int)points_vec.size() - particleTrack->numParticles() ) > 0 ) {
    //    cout << "points_vec.size() " << points_vec.size() << "      particle_vec.size() " << particleTrack->numParticles() << endl;
    //    cout.flush();
   // }

    if( debugMode ) checkPtsIdx();

	for( Delaunay::Finite_faces_iterator it_f = dt->finite_faces_begin(); it_f != dt->finite_faces_end(); it_f++ ) {
		v[0] = it_f->vertex( 0 );
		v[1] = it_f->vertex( 1 );
		v[2] = it_f->vertex( 2 );

		pts[0] = v[0]->point( );
		pts[1] = v[1]->point( );
		pts[2] = v[2]->point( );

		points[0] = ptsIdx[(int)pts[0].y()][(int)pts[0].x()];
		points[1] = ptsIdx[(int)pts[1].y()][(int)pts[1].x()];
		points[2] = ptsIdx[(int)pts[2].y()][(int)pts[2].x()];

        if( points[0]<0 || points[1]<0 || points[2]<0 ) {
            cout << points[0] << "(" << (int)pts[0].y() << "," << (int)pts[0].x() << ") ";
            cout << points[1] << "(" << (int)pts[1].y() << "," << (int)pts[1].x() << ") ";
            cout << points[2] << "(" << (int)pts[2].y() << "," << (int)pts[2].x() << ") ";
            cout << "\nERROR!!! STOP!!!" << endl;
        }

        int idx = triOp->sameTriangle( points );
        if( idx != -1 ) {
            // update the old triangles that still exist! Color, triang map ( the one that
            // gives the triangle for any given point in the image ) and re-orders the pts
            // I'm not updating yet, only re-doing everything for each frame
            triOp->update_region( idx );
        }

	}

    vector<int> triangsToRM;
    for( FastVec<triang>::iterator it = triangs_vec.begin(); it != triangs_vec.end(); it++ )
        if( it->lastFrame != imgDaemon->getCurrentTime() )
            triangsToRM.push_back(it->idx);

    for( int i=0; i<triangsToRM.size(); i++ )
        triOp->rmTriang( triangsToRM[i] );

    for( Delaunay::Finite_faces_iterator it_f = dt->finite_faces_begin(); it_f != dt->finite_faces_end(); it_f++ ) {
        v[0] = it_f->vertex( 0 );
        v[1] = it_f->vertex( 1 );
        v[2] = it_f->vertex( 2 );

        pts[0] = v[0]->point( );
        pts[1] = v[1]->point( );
        pts[2] = v[2]->point( );

        points[0] = ptsIdx[(int)pts[0].y()][(int)pts[0].x()];
        points[1] = ptsIdx[(int)pts[1].y()][(int)pts[1].x()];
        points[2] = ptsIdx[(int)pts[2].y()][(int)pts[2].x()];

        if( points[0]<0 || points[1]<0 || points[2]<0 ) {
            cout << points[0] << "(" << (int)pts[0].y() << "," << (int)pts[0].x() << ") ";
            cout << points[1] << "(" << (int)pts[1].y() << "," << (int)pts[1].x() << ") ";
            cout << points[2] << "(" << (int)pts[2].y() << "," << (int)pts[2].x() << ") ";
            cout << "\nERROR!!! STOP!!!" << endl;
        }

        int idx = triOp->sameTriangle( points );
        if( idx == -1 )
            triOp->insert_triang( points );

    }
}

void Mesh3D::findConstrainedTriangles() {
    for( FastVec<triang>::iterator it = triangs_vec.begin(); it != triangs_vec.end(); it++ ) {
        bool isConstrained = true;

        FastVec<region>::iterator it_r = regions_vec->getIt( it->idx );
        if( isDeformed( it_r )  )
            isConstrained = false;
//        if( badTriangle( it_r ) && isConstrained==false ) {
////            points_vec[it->points_vec[0]].bad = true;
////            points_vec[it->points_vec[1]].bad = true;
////            points_vec[it->points_vec[2]].bad = true;
//            it->bad = true;
//        } else {
////            points_vec[it->points_vec[0]].bad = false;
////            points_vec[it->points_vec[1]].bad = false;
////            points_vec[it->points_vec[2]].bad = false;
//            it->bad = false;
//        }
        for( int i=0; i<3; i++ ) {
            edges_vec[it->edges_vec[i]].constrained = isConstrained;
        }
    }
}

void Mesh3D::findBadPts() {
    for( FastVec<point_t>::iterator it = points_vec.begin(); it != points_vec.end(); it++ ) {
        int numBadTriangs=0;
        for(int i=0; i<it->numTriangs; i++) {
            if( triangs_vec[it->triangs_vec[i]].bad )
                numBadTriangs++;
        }
        if( numBadTriangs>5 && it->dispDisc==false )
            it->bad = true;
        else
            it->bad = false;
    }
}

bool Mesh3D::isDeformed( FastVec<region>::iterator it_r ) {
    if( mathOp.area( it_r ) < 50 )
        return true;//( fabs( mathOp.perimeter( it_r ) - it_r->initPerimeter ) > it_r->initPerimeter*1.2 );
    else
        return mathOp.perimeter( it_r )/sqrt( mathOp.area( it_r ) ) > 5.5;//( mathOp.area( it_r ) / mathOp.perimeter( it_r ) < 0.075 );//  &&
}

bool Mesh3D::badTriangle( FastVec<region>::iterator it_r ) {
    return mathOp.area( it_r )/mathOp.perimeter( it_r ) < 0.5 || it_r->numPts<25 ;
}

void Mesh3D::getSimRegions( vector<int>& simTriangs, vector<float>& simVal, int currTriang )  {
    int t, o;
    int num_neighbors=0;
    int threadIdx = 0;
    vector<int> currRegions;
    bool finish = false;
    int currT;

    simTriangs.clear();
    simVal.clear();

        // local variables for the search algorithm, one for each thread
    int marked_vec[NUM_MAX_TRIANGS];
    int endVec;
    int Q[NUM_MAX_TRIANGS];
    int beginQ;
    int endQ;

        currT = currTriang;
        beginQ = endQ = endVec = 0;

    //3      enqueue v onto Q
        Q[endQ++] = currT; // endQ points to the next empty space in Q


    //4      mark v
        triangs_vec[currT].mark[0] = true;
        marked_vec[endVec] = currT;  // endVec points to the last element
    //5      while Q is not empty:
        while(endQ != beginQ) { //  if they are equal, the queue is empty!!!
    //6          t ← Q.dequeue()
            t = Q[beginQ];
            beginQ = (beginQ+1) % NUM_MAX_TRIANGS;

            //list_neighbors[num_neighbors++] = t;
            currRegions.push_back(t);

    //9          for all edges e in G.incidentEdges(t) do
            for(int i=0; i<3; i++) {
    //10             o ← G.opposite(t,e)
                //o = adj_list->at(t)[i];
                o = -1;
                int e = triangs_vec[t].edges_vec[i];
                for(int j=0; j<edges_vec[e].numTriangs; j++) {
                    if(edges_vec[e].triangs_vec[j] != t)
                        o = edges_vec[e].triangs_vec[j];
                }
                if(o == -1)  continue;

        //11        if o is not marked:
                    //if((*triangs_vec)[o].mark == false && mathOp->distBetweenTriangs(points_vec, (*triangs_vec)[index], (*triangs_vec)[o]) <= radius) {
                if(triangs_vec[o].mark[0] == false && distBetweenTriangs(currT, o) < 128+128+64 && symBtwTriangs(currT, o) > 0.02  && num_neighbors < 50) { //symBtwTriangs(currT, o, *regions_vec)>0.05) {
        //12                  mark o
        //13                  enqueue o onto Q
                    triangs_vec[o].mark[0] = true;
                    marked_vec[++endVec] = o;
                    Q[endQ] = o;
                    endQ = (endQ+1) % (NUM_MAX_TRIANGS);
                    num_neighbors++;
                    simTriangs.push_back( o );
                    simVal.push_back( triOp->symBtwTriangs(currT, o) );
                }

            }
        }
        //regionsID.push_back(currRegions);
    //}

    // clear marks
    for(FastVec<triang>::iterator it_t=triangs_vec.begin(); it_t != triangs_vec.end(); it_t++)
        it_t->mark[0] = false;

}


void Mesh3D::updateVertices( ) {
	int currentTime = imgDaemon->getCurrentTime();
	vector<int> auxVec;
    int** particlesIdx;
    cv::Mat rmPtsImg;
    char imName[1024];

    rmPtsImg = cv::Mat::zeros(H,W,CV_8UC1);

    ptsToRM = particleTrack->getPtsToRM( );
    for( int i=0; i<ptsToRM->size(); i++ ) {
        FastVec<point_t>::iterator it;
        it = points_vec.getIt(ptsToRM->at(i));
        if( debugMode ) rmPtsImg.data[ rmPtsImg.step * it->y + it->x ] = 255;
        triOp->rmPt( ptsToRM->at(i) );
    }

    if( debugMode ) {
        sprintf( imName, "ptsRM_%d.png", imgDaemon->getCurrentTime() );
        cv::imwrite( imName, rmPtsImg );
    }

    //if( abs( (int)points_vec.size() - particleTrack->numParticles() ) > 0 ) {
    //    cout << "points_vec.size() " << points_vec.size() << "      particle_vec.size() " << particleTrack->numParticles() << endl;
    //    cout.flush();
    //}

    particleTrack->updateVertices( &points_vec, ptsIdx );

    if( debugMode ) checkPtsIdx();

    particlesIdx = particleTrack->getParticlesIdx();
    for( int i=0; i<H; i++ )
        for( int j=0; j<W; j++ )
            if( particlesIdx[i][j] != ptsIdx[i][j] ) {
                cout << "ParticlesIdx is different from ptsIdx!!!" << endl;
                cout.flush();
            }

}

// add new triangles (and respective edges), update color information and
// re-order points for each triangle (from top to bottom).
void Mesh3D::updateTriangs( vector<point_t>* pts ) {
	Delaunay::Vertex_handle v[3];
	Delaunay::Point p[3];
	int points[3];

    //triOp->clearTriangulation( );

	// updating the triangle index map.
    for( int i=0; i<H; i++ ) {
        for( int j=0; j<W; j++ )
            triangsIdx[ i ][ j ] = /*ptsIdx[ i ][ j ] =*/ -1;

    }

    particleTrack->addParticles( pts );

    //cout << "I had " << dt->number_of_vertices() << " in delaunay and " << points_vec.size() << " in PointsVec.";
    //cout << " Will add " << (int)pts->size() << " and RM " << ptsToRM->size() << endl;

	// update the delaunay triangulation
    dt = delobject->updateTriang( &points_vec, pts, ptsToRM );
    updateDataStructure( pts );


    ptsToRM->clear();

    //cout << "I have " << dt->number_of_vertices() << " in Delaunay and " << points_vec.size() << " in points_vec." << endl;

	// adjList used for searching neighbors
    triOp->createAdjList();
}


// save order:
// num triangles, num_triangles + orthogonal_triangles
// triangles coordinates (first regular, then orthogonal ones)
// image
void Mesh3D::saveMesh3D(FastVec<region> &regions_vec) {
	int numNormalTriangs;
	int numOrthogonalTriangs;
	int cropSize;
	char tempStr[512];
	int tempCurrIdx = imgDaemon->getCurrentFrame()%imgDaemon->getTempWindow();
	int centerCam = config->readInt( "centerCam" );
	int currFrame = imgDaemon->getCurrentFrame();
	int maxNumSideCams = imgDaemon->getMaxNumSideCams();

	// first step is to close the mesh
    triOp->closeMesh3D(&regions_vec);

	ofstream outfile;
    sprintf(tempStr, "meshes/triang_mesh_%d_%d.bin", centerCam, currFrame);
	outfile.open(tempStr,std::iostream::binary);

	numNormalTriangs = triangs_vec.size()+orthoTriangs_vec.size();
	numOrthogonalTriangs = triangs_vec.size();
	cropSize = config->readInt( "cropSize", 0 );
	outfile.write( (char*)&numNormalTriangs, sizeof(int));
	outfile.write( (char*)&numOrthogonalTriangs, sizeof(int));
	outfile.write( (char*)&cropSize, sizeof(int));

    //cout << " Saving:: "<< numNormalTriangs << " " << numOrthogonalTriangs << "  and normalizing with: " << maxNumSideCams << endl << endl;
    //cout << " But I got " << orthoTriangs_vec.size() << " orthogonal triangles!!!" << endl;
	for(FastVec<region>::iterator it_r = regions_vec.begin(); it_r!=regions_vec.end(); it_r++) {
				for(int i=0; i<3; i++) {
            float aux = it_r->zClosed[i]/maxNumSideCams;
            //float aux = it_r->zTempRef[i]/maxNumSideCams;
			outfile.write((char*)&points_vec[triangs_vec[it_r->idx].points_vec[i]].x, sizeof(int));
			outfile.write((char*)&points_vec[triangs_vec[it_r->idx].points_vec[i]].y, sizeof(int));
            outfile.write((char*)&aux, sizeof(float));
		}
	}
	for(FastVec<orthoTriang>::iterator it_ot=orthoTriangs_vec.begin(); it_ot!=orthoTriangs_vec.end(); it_ot++ ) {
		for(int p=0; p<3; p++) {
			float aux = it_ot->z[p]/maxNumSideCams;
			outfile.write((char*)&it_ot->x[p], sizeof(int));
			outfile.write((char*)&it_ot->y[p], sizeof(int));
			outfile.write((char*)&aux, sizeof(float));
		}
	}
	outfile.close();

	//outfileNonClosed.write( (char*)&numNormalTriangs, sizeof(int));
	//outfileNonClosed.write( (char*)&numOrthogonalTriangs, sizeof(int));
	//cout << " Saving:: "<< numNormalTriangs << " " << numOrthogonalTriangs << "  and normalizing with: " << maxNumSideCams << endl << endl;
	//for(int it = 0; it<numOrthogonalTriangs; it++) {
	//	for(int i=0; i<3; i++) {
	//		float aux = regions_vec[it].zRef[i]/maxNumSideCams;
	//		outfileNonClosed.write((char*)&points_vec[triangs_vec[it].points_vec[i]].x, sizeof(int));
	//		outfileNonClosed.write((char*)&points_vec[triangs_vec[it].points_vec[i]].y, sizeof(int));
	//		outfileNonClosed.write((char*)&aux, sizeof(float));
	//	}
	//}

	//outfile.write((char*)&img->rows, sizeof(int));
	//outfile.write((char*)&img->cols, sizeof(int));
	//for(int i=0; i<img->rows; i++)
	//	for(int j=0; j<img->cols; j++) {
	//		outfile.write((char*)&planes[0].ptr<uchar>(i)[j], sizeof(uchar));
	//		outfile.write((char*)&planes[1].ptr<uchar>(i)[j], sizeof(uchar));
	//		outfile.write((char*)&planes[2].ptr<uchar>(i)[j], sizeof(uchar));
	//	}
}

// used for the disparity estimation. Don't mix up with the triangulation data structure
void Mesh3D::createRegionsVec(FastVec<region> &regions_vec) {
	FastVec<region>::iterator it_r;
	int order[3];
	int auxTrash;

	// configure my raster function/object/class
	raster->setRegions(&regions_vec);
	regions_vec.clear();

	// HERE!!!!
	for(FastVec<triang>::iterator it_t=triangs_vec.begin(); it_t != triangs_vec.end(); it_t++) {
		it_r = regions_vec.itInsert(auxTrash);
		triOp->order_points(triangs_vec[it_r->idx].points_vec,order);
		// already reserving memory for this...
		it_r->probs.reserve(maxDisp);
		it_r->probsAgg.reserve(maxDisp);
		it_r->ratioCost.reserve(maxDisp);

		// saving the three points of the triangle. Possibly ordenate it first? For easier
		// access on rastering this region?
		it_r->x[0] = points_vec[triangs_vec[it_r->idx].points_vec[order[0]]].x;
		it_r->y[0] = points_vec[triangs_vec[it_r->idx].points_vec[order[0]]].y;
		it_r->x[1] = points_vec[triangs_vec[it_r->idx].points_vec[order[1]]].x;
		it_r->y[1] = points_vec[triangs_vec[it_r->idx].points_vec[order[1]]].y;
		it_r->x[2] = points_vec[triangs_vec[it_r->idx].points_vec[order[2]]].x;
		it_r->y[2] = points_vec[triangs_vec[it_r->idx].points_vec[order[2]]].y;
		it_r->z[0] = it_r->z[1] = it_r->z[2] = 0;
		it_r->ptIdx[0] = triangs_vec[it_r->idx].points_vec[order[0]];
		it_r->ptIdx[1] = triangs_vec[it_r->idx].points_vec[order[1]];
		it_r->ptIdx[2] = triangs_vec[it_r->idx].points_vec[order[2]];

		calcRegionRGB(imgDaemon->getCenterImg(), it_r->idx, it_r->meanRGB, it_r->numPts);
		it_r->probs.clear();
		it_r->probsAgg.clear();
		it_r->ratioCost.clear();
        it_r->meanR.clear();
        it_r->meanG.clear();
        it_r->meanB.clear();
        it_r->depth.clear();
        it_r->sum_costs.clear();
        it_r->sum_costs_agg.clear();
		it_r->used = true;
		it_r->stable = false;
	}
}

void Mesh3D::calcRegionRGB(cv::Mat* img, int it, float meanRGB[3], int& numPts) {
	int left, right;

	numPts = 0;
	meanRGB[0] = meanRGB[1] = meanRGB[2] = 0;

	// get vals to do an bresenham...
	raster->setTriangle(it);
	for(int i=points_vec[triangs_vec[it].points_vec[0]].y; i<= points_vec[triangs_vec[it].points_vec[2]].y; i++) {
		raster->getHorizLimits(i, left, right);
		for(int j=left; j<=right; j++) {
			meanRGB[0] += img->data[img->step * i + j*3 + 0];//(*planes)[0].ptr<unsigned char>(i)[j];
			meanRGB[1] += img->data[img->step * i + j*3 + 1];//(*planes)[1].ptr<unsigned char>(i)[j];
			meanRGB[2] += img->data[img->step * i + j*3 + 2];//(*planes)[2].ptr<unsigned char>(i)[j];
			numPts++;
			triangsIdx[i][j] = it;
		}
	}

	meanRGB[0] = float(meanRGB[0])/numPts;
	meanRGB[1] = float(meanRGB[1])/numPts;
	meanRGB[2] = float(meanRGB[2])/numPts;
}

float Mesh3D::distBetweenTriangs(int t1, int t2) {
	return mathOp.distBetweenTriangs(&points_vec, triangs_vec[t1], triangs_vec[t2]);
}

float Mesh3D::symBtwTriangs(int t1, int t2 ) {
    return triOp->symBtwTriangs( t1, t2 );
}

inline void Mesh3D::setWeightLink( int eidx, float val ) {
	if( edges_vec[ eidx ].weightLink == 0 )
		return;
	//else if( edges_vec[ eidx ].weightLink == -666 )
		edges_vec[ eidx ].weightLink = val;

}

/*************************************************************/
/// External access to the mesh elements

vector<point> Mesh3D::ptsForTriangulation() {
	vector<point> pts;
	vector<int> triangsToRm;

	// BUG!! This is only temporary... Remove after!
	for(FastVec<triang>::iterator it_t=triangs_vec.begin(); it_t!=triangs_vec.end(); it_t++) {
		for(int i=0; i<3; i++) {
			int idx = i%3;

			if( points_vec[it_t->points_vec[idx]].used == false ) {
				cout << "Mesh3D::ptsForTriangulation() -> Error! Triangle " << it_t->idx << " with an invalid point: " << it_t->points_vec[idx] << endl;
				triangsToRm.push_back( it_t->idx );
				break;
			}
		}
	}
	for(int i=0; i<triangsToRm.size(); i++) {
		triOp->rmTriang( triangsToRm[i] );
	}


	for(FastVec<triang>::iterator it_t=triangs_vec.begin(); it_t!=triangs_vec.end(); it_t++) {
		for(int i=0; i<3; i++) {
			point p;
			int idx = i%3;
			p.x = points_vec[it_t->points_vec[idx]].x;
			p.y = points_vec[it_t->points_vec[idx]].y;
			pts.push_back( p );
		}
	}

	return pts;
}

TriangleOperations* Mesh3D::getTriOp() {
	return triOp;
}

FastVec<point_t>* Mesh3D::getPtsVec() {
	return &points_vec;
}

int Mesh3D::getNumPts() {
	return points_vec.size();
}

FastVec<edge>* Mesh3D::getEdgesVec() {
	return &edges_vec;
}

int Mesh3D::getNumEdges() {
	return edges_vec.size();
}

FastVec<triang>* Mesh3D::getTriangsVec() {
	return &triangs_vec;
}

int Mesh3D::getNumTriangs() {
	return triangs_vec.size();
}

cv::Mat* Mesh3D::getEdgesImg() {
	scaleMap->runEdgeDetector( imgDaemon->getCenterImg() );
	return scaleMap->getEdgesImg();
}

vector< tuple<float,int,int> >* Mesh3D::getWeightLinks( ) {
	return &weightLinks;
}

int Mesh3D::find_triang_from_pt( int y, int x ) {
	return triangsIdx[y][x];
}

void Mesh3D::checkPtsIdx() {
    int count=0;
    for( int i=0; i<H; i++ )
        for( int j=0; j<W; j++ ) {
            if( ptsIdx[i][j] != -1 ) {
                if( points_vec[ ptsIdx[i][j] ].y != i || points_vec[ ptsIdx[i][j] ].x != j ) {
                    cout << "Problem with ptsIDX!!!";
                }
                count++;
            }
        }

    if( count != (int)points_vec.size() ) {
        cout << "Diff number of ptsIdx and points_vec!!!"  << endl;
    }
}
