#ifndef REFINEMENT_H
#define REFINEMENT_H

#include <vector>

#include "DataStructure/point_t.h"
#include "DataStructure/edge.h"
#include "DataStructure/triang.h"
#include "DataStructure/region.h"
#include "DataStructure/FastVec.h"
#include "Aux/MathOp.h"
#include <sbl/core/Config.h>
#include "Mesh.h"
#include <omp.h>

using namespace std;

#define PI				3.14159265358979323846

class Mesh3D; // forward declaration

class Refinement {
private:
	MathOp* mathOp;
	sbl::Config* config;
	FastVec<point_t>* points_vec;
	FastVec<edge>* edges_vec;
	FastVec<triang>* triangs_vec;
	FastVec<region>* regions_vec;
	int numRegions;
	int numPts;
	int numEdges;
	int numTriangs;
	float variance;
	int numSideCameras;
    int maxDisp;

	
	// local variables used for the refinement process
	float** A;
	float** Ainv;
	float** W;
	float** b;
	float** c;
	float** d;
	float** D;
	float** alfa;

	int list_triangs[MAX_NUM_THREADS][MAX_NUM_TRIANG_NEIGHBORS];
	int endListTriangs[MAX_NUM_THREADS];
	int list_vertices[MAX_NUM_THREADS][MAX_NUM_TRIANG_NEIGHBORS];
	int endListVertices[MAX_NUM_THREADS];
	// local variables used for the refinement process


	float* expColor;
	double* expColorVar;

public:
	Refinement();
	~Refinement();

	void setConfig(sbl::Config* config);
	void setRegions(FastVec<region>* regions_vec);//, point_t* points_vec, int numPts, edge* edges_vec, int numEdges, triang* triangs_vec, int numTriangs);
	void setConfig(sbl::Config* config, FastVec<region>* regions_vec, FastVec<point_t>* points_vec, FastVec<edge>* edges_vec, FastVec<triang>* triangs_vec);
	void refineMesh3D(FastVec<region>* regions_vec, Mesh3D* mesh);

	// configs...
	void setVariance(float var);
	float weightOmega(int r1, int r2, bool simple=false);
	

};

#endif
