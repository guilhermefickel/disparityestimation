#include "MatchFunction.h"

MatchFunction::MatchFunction() {

}

MatchFunction::~MatchFunction() {

}

void MatchFunction::setConfig(sbl::Config* config, FastVec<region>* regions_vec, ImagesDaemon* imgDaemon) {
	vector<cv::Mat> tvec[20];
	int leftCam, centerCam;

	this->imgDaemon = imgDaemon;
	this->config = config;



    sideImgs = imgDaemon->getSideImgs();
    centerImg = imgDaemon->getCenterImg();

	leftCam = config->readInt( "leftCam" );
	numSideCams = config->readInt( "numSideCams" );
	centerCam = config->readInt( "centerCam" );
	maxDisp = config->readInt( "maxDisp" );	
	this->regions_vec = regions_vec;
	//this->img_center = config->getImgCenterPtr();
	this->numLeftCameras = abs( leftCam - centerCam );
	this->maxNumSideCameras = imgDaemon->getMaxNumSideCams();
	this->ThresholdBadCam = config->readFloat( "thresholdBadCam", 0.0f );

}

void MatchFunction::setRaster(Raster* raster) {
	this->raster = raster;
}

float MatchFunction::match(int regIt, int delta, float& ratioCost, int threadIdx) {
	float sum_cost, final_cost;
    int numBadCamLeft=0;
    int numBadCamRight=0;
    int numRightCameras = numSideCams-numLeftCameras;

    //for(int i=0; i<numSideCams; i++)
    //    dist_vec[threadIdx][i] = 0;


	float temp_cost;
	temp_cost = 0;
	// for each side image (left and right images, possibly more than two)
	for(int side_img = 0; side_img<numSideCams; side_img++) {
        if(side_img < numLeftCameras) { // left images
            float multPos=1;
			if(numLeftCameras < maxNumSideCameras) multPos = (maxNumSideCameras-side_img-(maxNumSideCameras-numLeftCameras))/float(maxNumSideCameras);
			else		multPos = (maxNumSideCameras-side_img)/float(maxNumSideCameras);

            if( delta % int(round(1.0/multPos)) != 0 )
                continue;

			temp_cost = calcCostRGB(regIt, int(delta*multPos), LEFT_SIDE, centerImg, &(*sideImgs)[side_img],threadIdx);            
            if( temp_cost == -1 ) numBadCamLeft++;
		}
		else { // right images
			float multPos=1;
			if(numLeftCameras < maxNumSideCameras) multPos = (side_img-maxNumSideCameras+1+(maxNumSideCameras-numLeftCameras))/float(maxNumSideCameras); // Apparently OK
			else				multPos = (side_img-maxNumSideCameras+1)/float(maxNumSideCameras);

            if( delta % int(round(1.0/multPos)) != 0 )
                continue;

			temp_cost = calcCostRGB(regIt, int(delta*multPos), RIGHT_SIDE, centerImg, &(*sideImgs)[side_img],threadIdx);
            if( temp_cost == -1 ) numBadCamRight++;
		}
        if( delta == 0 ) temp_cost = 0;
        if( temp_cost != -1 ) dist_vec[threadIdx][side_img] = temp_cost;
        else dist_vec[threadIdx][side_img] = 0;
	}
	

	//calcTotalCost(regIt, delta);

	sum_cost = 0;
	final_cost = 1; 		
	float costR=0.0001, costL=0.0001;
	for(int i=0; i<numSideCams; i++) {
		if(i < numLeftCameras) 
            costL += dist_vec[threadIdx][i];
		else
            costR += dist_vec[threadIdx][i];
	}

    if( numSideCams-(numBadCamLeft+numBadCamRight) <= 0 )
        return -1;

	if(costL > costR) {
        if(costR/costL > 0.75)
            final_cost = (costL+costR)*(numSideCams/(numSideCams-numBadCamLeft-numBadCamRight))/numSideCams;
		else
            final_cost = 1.85*costL*(numLeftCameras/(numLeftCameras-numBadCamLeft))/numSideCams;
	}
	else {
        if(costL/costR > 0.75)
            final_cost = (costL+costR)*(numSideCams/(numSideCams-numBadCamLeft-numBadCamRight))/numSideCams;
		else
            final_cost = 1.85*costR*(numRightCameras/(numRightCameras-numBadCamRight))/numSideCams;
	}

	
    //final_cost = (costL+costR)/numSideCams;

	//for(int i=0; i<numSideCams; i++) {
	//	sum_cost += dist_vec[i];
	//	if( dist_vec[i]/float((*regions_vec)[regIt].numPts*3) > ThresholdBadCam || num_bad_matches >= maxNumSideCameras-1) {
	//		final_cost += dist_vec[i];
	//	}
	//	else num_bad_matches++;
	//	if(biggestCost < dist_vec[i])
	//		biggestCost = dist_vec[i];
	//}

	//if(num_bad_matches == numSideCams) {
	//	final_cost = biggestCost;
	//	ratioCost = final_cost/float((*regions_vec)[regIt].numPts*3);
	//}
	//else {
		//final_cost /= (numSideCams-num_bad_matches);
		ratioCost = final_cost/float((*regions_vec)[regIt].numPts*3);
	//}
			
	return final_cost;
}

float MatchFunction::calcCostRGB(int it, int deltax, unsigned char side, cv::Mat* img_center, cv::Mat* img_ptr, int threadIdx) {
	#define NUM_BINS	20

	int left, right;
	short int binsR[NUM_BINS+1];
	short int binsG[NUM_BINS+1];
	short int binsB[NUM_BINS+1];
	float deltax_bin = 0.2/float(NUM_BINS);
	float gain;
	int bin;
	bool discard=false;
	
	unsigned char* ptr1 = img_center->data;
	unsigned char* ptr2 = img_ptr->data;

	if(side == RIGHT_SIDE) {
		deltax = -deltax;
		maxDisp = -maxDisp;
	}

	int maxValR, maxValG, maxValB;	
	
	for(int i=0; i<NUM_BINS+1; i++)
		binsR[i] = binsG[i] = binsB[i] = 0;
	
	// for each row (scanline'ing the triangle)
	for(int row=(*regions_vec)[it].y[0]; row<(*regions_vec)[it].y[2]; row++) { 
		raster->getHorizLimits(row, left, right, threadIdx);
		// for each pixel in this line
		for(int col=left; col<=right; col++) {
			if(col + deltax < img_center->cols && col + deltax >= 0) {
				gain = (float(ptr1[img_center->step * row + col*3 ])+1.0f) / (float(ptr2[img_ptr->step * row + col*3 + deltax*3])+1.0f);
                //bool isValid = ((gain>=0.9) & (gain<=1.1));
                if(gain < 0.9)
                    bin = NUM_BINS;
                else if(gain > 1.1)
                    bin = NUM_BINS;
                else
                    bin = int((gain-0.9)/deltax_bin);
                if (bin>NUM_BINS-1)
                    bin = NUM_BINS;
                //bin = int((gain-0.9)/deltax_bin) * isValid + !isValid * NUM_BINS;
				binsR[bin]++;

				gain = (float(ptr1[img_center->step * row + col*3 + 1])+1.0f) / (float(ptr2[img_ptr->step * row + col*3 + 1 + deltax*3])+1.0f);
                //isValid = ((gain>=0.9) & (gain<=1.1));
                if(gain < 0.9)
                    bin = NUM_BINS;
                else if(gain > 1.1)
                    bin = NUM_BINS;
                else
                    bin = int((gain-0.9)/deltax_bin);
                if (bin>NUM_BINS-1)
                    bin = NUM_BINS;
                //bin = int((gain-0.9)/deltax_bin) * isValid + !isValid * NUM_BINS;
				binsG[bin]++;

				gain = (float(ptr1[img_center->step * row + col*3 + 2])+1.0f) / (float(ptr2[img_ptr->step * row + col*3 + 2 + deltax*3])+1.0f);
                //isValid = ((gain>=0.9) & (gain<=1.1));
                if(gain < 0.9)
                    bin = NUM_BINS;
                else if(gain > 1.1)
                    bin = NUM_BINS;
                else
                    bin = int((gain-0.9)/deltax_bin);
                if (bin>NUM_BINS-1)
                    bin = NUM_BINS;
                //bin = int((gain-0.9)/deltax_bin) * isValid + !isValid * NUM_BINS;
				binsB[bin]++;
			}
			else
                return -1;
                //discard = true;
		}
	}

    if(discard) return -1;

    maxValR = maxValG = maxValB = -5555;
    for(int i=1; i<NUM_BINS-2; i++)
        if(binsR[i-1] + binsR[i] + binsR[i+1] > maxValR)
            maxValR = binsR[i-1] + binsR[i] + binsR[i+1];

    for(int i=1; i<NUM_BINS-2; i++)
        if(binsG[i-1] + binsG[i] + binsG[i+1] > maxValG)
            maxValG = binsG[i-1] + binsG[i] + binsG[i+1];

    for(int i=1; i<NUM_BINS-2; i++)
        if(binsB[i-1] + binsB[i] + binsB[i+1] > maxValB)
            maxValB = binsB[i-1] + binsB[i] + binsB[i+1];

//    maxValR = maxValG = maxValB = 0;
//    for(int i=0; i<NUM_BINS-1; i++)
//        maxValR += binsR[i];

//    for(int i=0; i<NUM_BINS-1; i++)
//        maxValG += binsG[i];

//    for(int i=0; i<NUM_BINS-1; i++)
//        maxValB += binsB[i];


//    maxValR = maxValG = maxValB = 0;
//    for(int i=0; i<NUM_BINS-1; i++)
//        if( binsR[i] > maxValR ) maxValR = binsR[i];

//    for(int i=0; i<NUM_BINS-1; i++)
//        if( binsG[i] > maxValG ) maxValG = binsG[i];

//    for(int i=0; i<NUM_BINS-1; i++)
//        if( binsB[i] > maxValB ) maxValB = binsB[i];


	return (float)maxValR+maxValG+maxValB;
}
