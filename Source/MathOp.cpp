#include "MathOp.h"

MathOp::MathOp() {

}

MathOp::~MathOp() {

}

int MathOp::quickL2(int dx, int dy) {
	int min, max;
	if ( dx < 0 ) dx = -dx;
	if ( dy < 0 ) dy = -dy;

	if ( dx < dy )
	{
		min = dx;
		max = dy;
	} else {
		min = dy;
		max = dx;
	}

	// coefficients equivalent to ( 123/128 * max ) and ( 51/128 * min )
	return ((( max << 8 ) + ( max << 3 ) - ( max << 4 ) - ( max << 1 ) +
			( min << 7 ) - ( min << 5 ) + ( min << 3 ) - ( min << 1 )) >> 8 );
}

float MathOp::distBetweenTriangs(FastVec<point_t>* points_vec, triang& t1, triang& t2) {
	int dx = ((*points_vec)[t1.points_vec[0]].x+(*points_vec)[t1.points_vec[1]].x+(*points_vec)[t1.points_vec[2]].x)/3
		    -((*points_vec)[t2.points_vec[0]].x+(*points_vec)[t2.points_vec[1]].x+(*points_vec)[t2.points_vec[2]].x)/3;
	int dy = ((*points_vec)[t1.points_vec[0]].y+(*points_vec)[t1.points_vec[1]].y+(*points_vec)[t1.points_vec[2]].y)/3
		    -((*points_vec)[t2.points_vec[0]].y+(*points_vec)[t2.points_vec[1]].y+(*points_vec)[t2.points_vec[2]].y)/3;;
	int min, max;
	if ( dx < 0 ) dx = -dx;
	if ( dy < 0 ) dy = -dy;

	if ( dx < dy )
	{
		min = dx;
		max = dy;
	} else {
		min = dy;
		max = dx;
	}

	// coefficients equivalent to ( 123/128 * max ) and ( 51/128 * min )
	return ((( max << 8 ) + ( max << 3 ) - ( max << 4 ) - ( max << 1 ) +
			( min << 7 ) - ( min << 5 ) + ( min << 3 ) - ( min << 1 )) >> 8 );

	//return float(abs(points_vec[t1.points_vec[0]].x-points_vec[t2.points_vec[0]].x) +
	//	   abs(points_vec[t1.points_vec[0]].y-points_vec[t2.points_vec[0]].y));

	//p1.x = int( (points_vec[t1.points_vec[0]].x + points_vec[t1.points_vec[1]].x + points_vec[t1.points_vec[2]].x)/3.0f );
	//p1.y = int( (points_vec[t1.points_vec[0]].y + points_vec[t1.points_vec[1]].y + points_vec[t1.points_vec[2]].y)/3.0f );
	//
	//p2.x = int( (points_vec[t2.points_vec[0]].x + points_vec[t2.points_vec[1]].x + points_vec[t2.points_vec[2]].x)/3.0f );
	//p2.y = int( (points_vec[t2.points_vec[0]].y + points_vec[t2.points_vec[1]].y + points_vec[t2.points_vec[2]].y)/3.0f );

	//pf.x = p2.x-p1.x;
	//pf.y = p2.y-p1.y;
	//pf.z = 0;

	//return magnitudeVec(pf);
}

float MathOp::magnitudeVec(point_t v1) {
	return sqrt(float(v1.x*v1.x+v1.y*v1.y+v1.z[0]*v1.z[0]));
}

void MathOp::matrix_inverse(float **Min, float **Mout, int actualsize, int width, int threadIdx) {
	cv::Mat matA = cv::Mat::zeros(actualsize,actualsize,CV_32FC1);
	cv::Mat matOut = cv::Mat::zeros(actualsize,actualsize,CV_32FC1);
	if(actualsize<=0) {
		cout << "Opa, isso acontece normalmente?" << endl;
	}

	for(int i=0; i<actualsize; i++) 
		for(int j=0; j<actualsize; j++) 
			matA.ptr<float>(i)[j] = Min[threadIdx][(i*width)+j];
		
	matOut = matA.inv();
	
	for(int i=0; i<actualsize; i++) 
		for(int j=0; j<actualsize; j++) 
			Mout[threadIdx][(i*width)+j] = matOut.ptr<float>(i)[j];

}

void MathOp::matrix_mul(float** m1, float** m2, float** res, int rows, int cols, int rep, int sizeMat, int threadIdx) {
	if(cols > 1) {
		for(int l=0; l<rows; l++)
			for(int c=0; c<cols; c++) {
				res[threadIdx][(l*sizeMat)+c] = 0;
				for(int k=0; k<rep; k++) {
					res[threadIdx][(l*sizeMat)+c] += m1[threadIdx][(l*sizeMat)+k]*m2[threadIdx][(k*sizeMat)+c];
				}
			}
	}
	else {
		for(int l=0; l<rows; l++)
			for(int c=0; c<cols; c++) {
				res[threadIdx][l] = 0;
				for(int k=0; k<rep; k++) {
					res[threadIdx][l] += m1[threadIdx][(l*sizeMat)+k]*m2[threadIdx][k];
				}
			}
	}
}

point MathOp::crossProduct(point p1, point p2) {
	point res;
	res.x = p1.y*p2.z - p1.z*p2.y;
	res.y = p1.z*p2.x - p1.x*p2.z;
	res.z = p1.x*p2.y - p1.y*p2.x;

	return res;
}

int MathOp::dotProduct(point p1, point p2) {
	return int(p1.x*p2.x+p1.y*p2.y+p1.z*p2.z);
}

point MathOp::pointSub(point p1, point p2) {
	point res;
	res.x = p1.x-p2.x;
	res.y = p1.y-p2.y;
	res.z = p1.z-p2.z;

	return res;
}