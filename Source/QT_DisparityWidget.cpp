#include <QtGui>
#include "QT_DisparityWidget.h"
#include <stdlib.h>


QT_DisparityWidget::QT_DisparityWidget(QWidget *parent)
    : QWidget(parent)
{
	zoomFactor = 3.0f;
    setBackgroundRole(QPalette::Base);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

	xmax = ymax = 20;
	xmin = ymin = 10;

	isInit = false;
	centerImg = cv::Mat::zeros( 10, 10, CV_8UC3 );
	leftImg = cv::Mat::zeros( 10, 10, CV_8UC3 );
	rightImg = cv::Mat::zeros( 10, 10, CV_8UC3 );

	customPlot = new QCustomPlot;
	customPlot->addGraph();
	customPlot->show();

	QString windowName("Disparity Debug");
	setWindowTitle(windowName);

	createHorizontalGroupBox();
	createHorizontalGroupBox2();
	QVBoxLayout *mainLayout = new QVBoxLayout;
	mainLayout->addWidget(horizontalGroupBox);
	mainLayout->addWidget(horizontalGroupBox2);

	setLayout(mainLayout);

	connect(sliderDisp, SIGNAL(valueChanged(int)), this, SLOT(dispChanged(int)) );
	connect(sliderDisp, SIGNAL(valueChanged(int)), lcdNumberVarDisp, SLOT(display(int)));
	connect(this, SIGNAL(disparityChanged(int)), lcdNumberVarDisp, SLOT(display(int)));
	connect(this, SIGNAL(disparityChanged(int)), lcdNumberDisp, SLOT(display(int)));
}

void QT_DisparityWidget::dispChanged(int disp) {
	disparity = disp;

	update();
}

void QT_DisparityWidget::getMaxMin() {
	xmax = ymax = -999999999;
	xmin = ymin = 999999999;



	for(int i=0; i<3; i++) {
		if(triangs[i].x < xmin)
			xmin = triangs[i].x;
		if(triangs[i].x > xmax)
			xmax = triangs[i].x;
		if(triangs[i].y < ymin)
			ymin = triangs[i].y;
		if(triangs[i].y > ymax)
			ymax = triangs[i].y;
	}
}

void QT_DisparityWidget::changeCenterImg() {
	QImage img = qt_util.Mat2QImage3(centerImg);

	img = qt_util.cropPixmap(img, xmin-5, ymin-5, xmax+5, ymax+5);
	if(img.width() == 0 || img.height() == 0) return;
	img = img.scaled(img.width()*zoomFactor, img.height()*zoomFactor);
	

	viewCenter->setFixedSize(img.width()+2, img.height()+2);
	sceneCenter.setSceneRect(0, 0, img.width(), img.height());
	viewCenter->setScene(&sceneCenter);
	sceneCenter.clear();
	for(int i=0; i<3 && triangs.size()>2; i++) 
		sceneCenter.addLine(QLineF( triangs[i].x*zoomFactor+(5-xmin)*zoomFactor, triangs[i].y*zoomFactor+(5-ymin)*zoomFactor, triangs[(i+1)%3].x*zoomFactor+(5-xmin)*zoomFactor, triangs[(i+1)%3].y*zoomFactor+(5-ymin)*zoomFactor ) );

	sceneCenter.setBackgroundBrush(QBrush(img));
}

void QT_DisparityWidget::changeLeftImg() {
	QImage img = qt_util.Mat2QImage3(leftImg);

	img = qt_util.cropPixmap(img, xmin-5+disparity, ymin+-5, xmax+5+disparity, ymax+5);
	if(img.width() == 0 || img.height() == 0) return;
	img = img.scaled(img.width()*zoomFactor, img.height()*zoomFactor);
	

	viewLeft->setFixedSize(img.width()+2, img.height()+2);
	sceneLeft.setSceneRect(0, 0, img.width(), img.height());
	viewLeft->setScene(&sceneLeft);
	sceneLeft.clear();
	for(int i=0; i<3 && triangs.size()>2; i++) 
		sceneLeft.addLine(QLineF( triangs[i].x*zoomFactor+(5-xmin)*zoomFactor, triangs[i].y*zoomFactor+(5-ymin)*zoomFactor, triangs[(i+1)%3].x*zoomFactor+(5-xmin)*zoomFactor, triangs[(i+1)%3].y*zoomFactor+(5-ymin)*zoomFactor ) );

	sceneLeft.setBackgroundBrush(QBrush(img));
}

void QT_DisparityWidget::changeRightImg() {
	QImage img = qt_util.Mat2QImage3(rightImg);

	img = qt_util.cropPixmap(img, xmin-5-disparity, ymin+-5, xmax+5-disparity, ymax+5);
	if(img.width() == 0 || img.height() == 0) return;
	img = img.scaled(img.width()*zoomFactor, img.height()*zoomFactor);
	

	viewRight->setFixedSize(img.width()+2, img.height()+2);
	sceneRight.setSceneRect(0, 0, img.width(), img.height());
	viewRight->setScene(&sceneRight);
	sceneRight.clear();
	for(int i=0; i<3 && triangs.size()>2; i++) 
		sceneRight.addLine(QLineF( triangs[i].x*zoomFactor+(5-xmin)*zoomFactor, triangs[i].y*zoomFactor+(5-ymin)*zoomFactor, triangs[(i+1)%3].x*zoomFactor+(5-xmin)*zoomFactor, triangs[(i+1)%3].y*zoomFactor+(5-ymin)*zoomFactor ) );

	sceneRight.setBackgroundBrush(QBrush(img));
}

void QT_DisparityWidget::createHorizontalGroupBox2() {
	horizontalGroupBox2 = new QGroupBox(tr("Horizontal layout 2"));
    QHBoxLayout *layout = new QHBoxLayout;

	sliderDisp = new QSlider(Qt::Horizontal, this);
	sliderDisp->setPageStep(1);
	sliderDisp->setTickInterval(1);
	sliderDisp->setMaximum(30);	
	layout->addWidget(sliderDisp);

	lcdNumberDisp = new QLCDNumber(3);
	lcdNumberVarDisp = new QLCDNumber(3);
	lcdNumberDisp->setSegmentStyle(QLCDNumber::Outline);
	lcdNumberVarDisp->setSegmentStyle(QLCDNumber::Flat);
	layout->addWidget(lcdNumberDisp);
	layout->addWidget(lcdNumberVarDisp);
    
    horizontalGroupBox2->setLayout(layout);
}

void QT_DisparityWidget::createHorizontalGroupBox()
{
    horizontalGroupBox = new QGroupBox(tr("Horizontal layout"));
    QHBoxLayout *layout = new QHBoxLayout;

	viewCenter = new QGraphicsView;
	viewLeft = new QGraphicsView;
	viewRight = new QGraphicsView;

	layout->addWidget(viewLeft);
	layout->addWidget(viewCenter);
	layout->addWidget(viewRight);	
    
    horizontalGroupBox->setLayout(layout);
}

QSize QT_DisparityWidget::minimumSizeHint() const
{
    return QSize(40, 40);
}

QSize QT_DisparityWidget::sizeHint() const
{
    return QSize(375, 450);
}

void QT_DisparityWidget::setTriangs(vector<point>& triangs) {
	this->triangs = triangs;
	getMaxMin();

	update();
}

void QT_DisparityWidget::setImgs(cv::Mat imgCenter, cv::Mat imgLeft, cv::Mat imgRight) {
	this->centerImg = imgCenter;
	this->leftImg = imgLeft;
	this->rightImg = imgRight;

	isInit = true;

	update();
}

void QT_DisparityWidget::setAlphaVec(vector<float> alphaVec) {
	QVector<double> x, y;
	float maxEl = -99999;
 
	for(int i=0; i<(int)alphaVec.size(); i++) {
		x.push_back(i);
		y.push_back(alphaVec[i]);
		cout << "alphaVec: " << alphaVec[i] << " ";
		if(alphaVec[i]>maxEl)
			maxEl = alphaVec[i];
	}
	
	sort(alphaVec.begin(), alphaVec.end());

	customPlot->graph(0)->setData(x, y);
	// give the axes some labels:
	customPlot->xAxis->setLabel("alpha");
	customPlot->yAxis->setLabel("disparity");
	// set axes ranges, so we see all data:
	customPlot->xAxis->setRange(0, alphaVec.size());

	customPlot->yAxis->setRange(0, maxEl);//alphaVec[alphaVec.size()-1]);
	customPlot->replot();

	cout << "MAX ELEM: " << maxEl << endl;
}

void QT_DisparityWidget::setDisparity(int disp) {
	disparity = disp;

	update();

	emit disparityChanged(disp);
}

void QT_DisparityWidget::setMaxDisparity(int disp) {
	maxDisparity = disp;

	sliderDisp->setMaximum(maxDisparity);
}

void QT_DisparityWidget::paintEvent(QPaintEvent *)
{
	if(isInit == false) return;
	changeLeftImg();
	changeRightImg();
	changeCenterImg();
}
