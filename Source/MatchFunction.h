#ifndef MATCHFUNCTION_H
#define MATCHFUNCTION_H

#define LEFT_SIDE	0
#define RIGHT_SIDE	1

#include "Aux/Raster.h"
#include <sbl/core/Config.h>
#include "DataStructure/region.h"
#include "DataStructure/FastVec.h"
#include "Aux/ImagesDaemon.h"

#include <opencv2/core.hpp>


class MatchFunction {
private:
	Raster* raster;
	sbl::Config* config;
	ImagesDaemon* imgDaemon;

	cv::Mat* image;
	FastVec<region>* regions_vec;
	cv::Mat* centerImg;
	vector<cv::Mat>* sideImgs;
    int dist_vec[MAX_NUM_THREADS][20];


	int maxDisp;
	int numSideCams;
	int numLeftCameras;  // number of cameras in left 
	int maxNumSideCameras;  // max number of cameras in one side. Wisth both this informations it is possible
							// to know the number of cameras in left and right. 'numRightCameras = maxNumSideCameras - numLeftCameras'
	float ThresholdBadCam;

	float calcCost(int it, int deltax, unsigned char side, cv::Mat* img_center, cv::Mat* img_ptr, int threadIdx=0);
	float calcCostRGB(int it, int deltax, unsigned char side, cv::Mat* img_center, cv::Mat* img_ptr, int threadIdx=0);
	float calcCostMSE(int it, int deltax, unsigned char side, cv::Mat* img_center, cv::Mat* img_ptr, int threadIdx=0);
	void calcTotalCost(int it, int deltax);

public:
	MatchFunction();
	~MatchFunction();

	void setConfig(sbl::Config* config, FastVec<region>* regions_vec, ImagesDaemon* imgDaemon);
	void setRaster(Raster* raster);
	float match(int regIt, int delta, float& ratioCost, int threadIdx=0);
};

#endif
