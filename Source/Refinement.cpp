#include "Refinement.h"

Refinement::Refinement() {
	A = new float*[MAX_NUM_THREADS];
	Ainv = new float*[MAX_NUM_THREADS];
	W = new float*[MAX_NUM_THREADS];
	b = new float*[MAX_NUM_THREADS];
	c = new float*[MAX_NUM_THREADS];
	d = new float*[MAX_NUM_THREADS];
 	D = new float*[MAX_NUM_THREADS];
	alfa = new float*[MAX_NUM_THREADS];


	for(int t=0; t<MAX_NUM_THREADS; t++) {
		A[t] = new float[MAX_NUM_TRIANG_NEIGHBORS*MAX_NUM_TRIANG_NEIGHBORS];
		Ainv[t] = new float[MAX_NUM_TRIANG_NEIGHBORS*MAX_NUM_TRIANG_NEIGHBORS];
		W[t] = new float[MAX_NUM_TRIANG_NEIGHBORS*MAX_NUM_TRIANG_NEIGHBORS];
		b[t] = new float[MAX_NUM_TRIANG_NEIGHBORS];
		c[t] = new float[MAX_NUM_TRIANG_NEIGHBORS*MAX_NUM_TRIANG_NEIGHBORS];
		d[t] = new float[MAX_NUM_TRIANG_NEIGHBORS];
 		D[t] = new float[MAX_NUM_TRIANG_NEIGHBORS];
		alfa[t] = new float[MAX_NUM_TRIANG_NEIGHBORS];

		for(int i=0; i<MAX_NUM_TRIANG_NEIGHBORS; i++) {
			for(int j=0; j<MAX_NUM_TRIANG_NEIGHBORS; j++) {
				A[t][(i)*MAX_NUM_TRIANG_NEIGHBORS+j] = 0;
				Ainv[t][(i)*MAX_NUM_TRIANG_NEIGHBORS+j] = 0;
				W[t][(i)*MAX_NUM_TRIANG_NEIGHBORS+j] = 0;
				c[t][(i)*MAX_NUM_TRIANG_NEIGHBORS+j] = 0;
			}
			b[t][i] = 0;
			d[t][i] = 0;
			D[t][i] = 0;
			alfa[t][i] = 1.0;
		}
	}

	mathOp = new MathOp;

	expColor = new float[23*80];
	for(int i=0; i<23*80; i++)
		expColor[i] = exp(float(-i)/80.0f);

	expColorVar = new double[190*80];
	for(int i=0; i<190*80; i++)
		expColorVar[i] = exp(double(-i)/80.0f);
}

Refinement::~Refinement() {

}

void Refinement::setConfig(sbl::Config* config) {
	this->config = config;

    maxDisp = config->readInt( "maxDisp" );
}

void Refinement::setRegions(FastVec<region>* regions_vec) {
	this->regions_vec = regions_vec;
}

void Refinement::setConfig(sbl::Config* config, FastVec<region>* regions_vec, FastVec<point_t>* points_vec, FastVec<edge>* edges_vec, FastVec<triang>* triangs_vec) {
	this->config = config;
	this->regions_vec = regions_vec;
	this->points_vec = points_vec;
	this->edges_vec = edges_vec;
	this->triangs_vec = triangs_vec;
	numSideCameras = config->readInt( "numSideCams" );
    variance = config->readFloat( "smoothnessRefine" , 5.0 );
}

void Refinement::setVariance(float var) {
	variance = var;
}

float Refinement::weightOmega(int r1, int r2, bool simple) {
	float res;

    res = sqrt( ((*regions_vec)[r1].meanLab[0]-(*regions_vec)[r2].meanLab[0])*((*regions_vec)[r1].meanLab[0]-(*regions_vec)[r2].meanLab[0]) +
                ((*regions_vec)[r1].meanLab[1]-(*regions_vec)[r2].meanLab[1])*((*regions_vec)[r1].meanLab[1]-(*regions_vec)[r2].meanLab[1]) +
                ((*regions_vec)[r1].meanLab[2]-(*regions_vec)[r2].meanLab[2])*((*regions_vec)[r1].meanLab[2]-(*regions_vec)[r2].meanLab[2]) );

//    res = sqrt( ((*regions_vec)[r1].meanRGB[0]-(*regions_vec)[r2].meanRGB[0])*((*regions_vec)[r1].meanRGB[0]-(*regions_vec)[r2].meanRGB[0]) +
//                ((*regions_vec)[r1].meanRGB[1]-(*regions_vec)[r2].meanRGB[1])*((*regions_vec)[r1].meanRGB[1]-(*regions_vec)[r2].meanRGB[1]) +
//                ((*regions_vec)[r1].meanRGB[2]-(*regions_vec)[r2].meanRGB[2])*((*regions_vec)[r1].meanRGB[2]-(*regions_vec)[r2].meanRGB[2]) );



	if(simple || variance < 0.000001)
		return exp(-(float)res/(20));//(float)expColor[int(res/20)*80];//exp(-res/(20));
    else {
        //if( res <= 15 )
        //    return 100;
        //else
        //    return 0.00000001;
        return exp(-res/(variance));//(float)expColorVar[int(res/(150*variance))*80];//exp(-res/(150*variance));
    }
}

// Original Matlab code:
// A = diag(sum(w,2))+diag(alfa)-w;
// b = diag(alfa)*D;
// d = inv(A)*b;
void Refinement::refineMesh3D(FastVec<region>* regions_vec, Mesh3D* mesh) {
	int* ptrList;
	this->regions_vec = regions_vec;

	ptrList = points_vec->getPtrList();
    //#pragma omp parallel for
	for(int v = 0; v < points_vec->size(); v++) { // for every point of each triangle
		float sum=0;
		int threadIdx = (int)omp_get_thread_num();
		int vertex = ptrList[v];
		endListTriangs[threadIdx] = endListVertices[threadIdx] = 0;

		mesh->triOp->find_triangs(vertex, list_triangs, endListTriangs, threadIdx);
		mesh->triOp->find_vertices(vertex, list_triangs, endListTriangs, list_vertices, endListVertices, threadIdx);

		if(endListTriangs[threadIdx]<0)
			continue;

		// for triangles without temporal coherence
		for(int i=0; i<=endListTriangs[threadIdx]; i++) { // first position is for triangle 'cont'.
			for(int j=i+1; j<=endListTriangs[threadIdx]; j++)
				W[threadIdx][(i)*MAX_NUM_TRIANG_NEIGHBORS+(j)] = W[threadIdx][(j)*MAX_NUM_TRIANG_NEIGHBORS+i] = (float)numSideCameras*weightOmega(list_triangs[threadIdx][i], list_triangs[threadIdx][j]); ///float(list_triangs.size());
			
            int idx = (*regions_vec)[list_triangs[threadIdx][i]].depth.size()-1;
            D[threadIdx][i] = d[threadIdx][i] = (*regions_vec)[list_triangs[threadIdx][i]].depth[idx]; //(regions_vec[list_triangs[threadIdx][i]].stable ? tempDispMean[points_vec[vertex].y][points_vec[vertex].y] : regions_vec[list_triangs[threadIdx][i]].depth);
			W[threadIdx][(i)*MAX_NUM_TRIANG_NEIGHBORS+i] = 1;
		}
		
		for(int i=0; i<=endListTriangs[threadIdx]; i++) {
			sum = W[threadIdx][(i)*MAX_NUM_TRIANG_NEIGHBORS+0];
			for(int j=1; j<=endListTriangs[threadIdx]; j++)
				sum += W[threadIdx][(i)*MAX_NUM_TRIANG_NEIGHBORS+j];
					
			for(int j=0; j<=endListTriangs[threadIdx]; j++) {
				if(i == j) c[threadIdx][(i)*MAX_NUM_TRIANG_NEIGHBORS+j] = sum;
				else c[threadIdx][(i)*MAX_NUM_TRIANG_NEIGHBORS+j] = 0;
			}
		}

					
		for(int i=0; i<=endListTriangs[threadIdx]; i++) {
			//alfa[threadIdx][i] = ((*regions_vec)[list_triangs[threadIdx][i]].stable ? config->getAlphaStableConst()*(*regions_vec)[list_triangs[threadIdx][i]].probs[(int)(*regions_vec)[list_triangs[threadIdx][i]].depth] : (*regions_vec)[list_triangs[threadIdx][i]].probs[(int)(*regions_vec)[list_triangs[threadIdx][i]].depth]);
            int idx = (int)(*regions_vec)[list_triangs[threadIdx][i]].probs.size()-1;//Agg.size()-1;
            int idxDepth = (int)(*regions_vec)[list_triangs[threadIdx][i]].depth.size()-1;
            int debugDepth = (int)(*regions_vec)[list_triangs[threadIdx][i]].depth[idxDepth];

            if( debugDepth < 0 || debugDepth > maxDisp ) debugDepth = maxDisp/2;
            alfa[threadIdx][i] = (*regions_vec)[list_triangs[threadIdx][i]].probsAgg[idx][debugDepth];
            //alfa[threadIdx][i] = (*regions_vec)[list_triangs[threadIdx][i]].probs[idx][debugDepth];
			if((alfa[threadIdx][i] >= 0.001 && alfa[threadIdx][i] < 50) == false) 
				alfa[threadIdx][i] = 0.001;
		}
			
		for(int i=0; i<=endListTriangs[threadIdx]; i++) {
			for(int j=0; j<=endListTriangs[threadIdx]; j++) {
				if(i == j)
					A[threadIdx][(i)*MAX_NUM_TRIANG_NEIGHBORS+j] = c[threadIdx][(i)*MAX_NUM_TRIANG_NEIGHBORS+j] + alfa[threadIdx][i] - W[threadIdx][(i)*MAX_NUM_TRIANG_NEIGHBORS+j]; // CHANGE 1 With new formula
				else
					A[threadIdx][(i)*MAX_NUM_TRIANG_NEIGHBORS+j] = -W[threadIdx][(i)*MAX_NUM_TRIANG_NEIGHBORS+j];
			}
			b[threadIdx][i] = alfa[threadIdx][i]*D[threadIdx][i]; 
		}

		mathOp->matrix_inverse(A, Ainv, endListTriangs[threadIdx]+1, MAX_NUM_TRIANG_NEIGHBORS, threadIdx);
		mathOp->matrix_mul(Ainv, b, d, endListTriangs[threadIdx]+1, 1, endListTriangs[threadIdx]+1, MAX_NUM_TRIANG_NEIGHBORS, threadIdx);
			
		for(int i=0; i<=endListVertices[threadIdx]; i++)
            (*regions_vec)[list_triangs[threadIdx][i]].zRef[list_vertices[threadIdx][i]] = (*regions_vec)[list_triangs[threadIdx][i]].zClosed[list_vertices[threadIdx][i]] = d[threadIdx][i]; //regions_vec[list_triangs[threadIdx][i]].z[list_vertices[threadIdx][i]];

		
		// for triangles WITH temporal coherence
		for(int i=0; i<=endListTriangs[threadIdx]; i++)  // first position is for triangle 'cont'.
			D[threadIdx][i] = d[threadIdx][i] = (*regions_vec)[list_triangs[threadIdx][i]].zTemp[0];
		
		for(int i=0; i<=endListTriangs[threadIdx]; i++)
			b[threadIdx][i] = alfa[threadIdx][i]*D[threadIdx][i];

		mathOp->matrix_inverse(A, Ainv, endListTriangs[threadIdx]+1, MAX_NUM_TRIANG_NEIGHBORS, threadIdx);
		mathOp->matrix_mul(Ainv, b, d, endListTriangs[threadIdx]+1, 1, endListTriangs[threadIdx]+1, MAX_NUM_TRIANG_NEIGHBORS, threadIdx);
			
		for(int i=0; i<=endListVertices[threadIdx]; i++) {
			//if((*triangs_stats)[list_triangs[threadIdx][i]].stable > config->getTempWindow())
			//	(*regions_vec)[list_triangs[threadIdx][i]].zTempRef[list_vertices[threadIdx][i]] = (*triangs_stats)[list_triangs[threadIdx][i]].meanZ[list_vertices[threadIdx][i]];
			//else
            (*regions_vec)[list_triangs[threadIdx][i]].zTempRef[list_vertices[threadIdx][i]] = d[threadIdx][i];
            int tidx = list_triangs[threadIdx][i];
            int vidx = list_vertices[threadIdx][i];
            int pidx = (*triangs_vec)[tidx].points_vec[vidx];
            if( (*regions_vec)[tidx].x[vidx] != (*points_vec)[pidx].x ) {
                cout << "Refinement::refineMesh3D() -> Problem! Inconsistencies between regions_vec and triangs_vec point indexes!!" << endl;
            }
		}

	}
}




