#include "QT_DrawLabel.h"


QT_DrawLabel::QT_DrawLabel(int dispType, QWidget *parent)
    : QLabel(parent)
{
    isInit = false;
	regions_vec=NULL;
	pts_vec=NULL;
	frame=1;
	switch( dispType ) {
		case 0: dispOption = TRIANG;  break;
		case 1: dispOption = EDGE_PT;  break;
	}
}



QSize QT_DrawLabel::minimumSizeHint() const
{
    return QSize(10, 10);
}


QSize QT_DrawLabel::sizeHint() const
{
    return QSize(40, 40);
}


void QT_DrawLabel::setTriang(vector<point_t>& triang) {
	this->triang = triang;
	
	update();
}

void QT_DrawLabel::setRegionsVec(FastVec<region>* regions_vec) {
	this->regions_vec = regions_vec;
}

void QT_DrawLabel::setPtsVec( FastVec<point_t>* pts_vec ) {
	this->pts_vec = pts_vec;
}

void QT_DrawLabel::setConfigs(sbl::Config* config) {
	this->config = config;
}

void QT_DrawLabel::setImg(QImage& img) {
	this->img = img;
	isInit = true;
	resize(img.width(), img.height());
}

void QT_DrawLabel::mousePressEvent(QMouseEvent *evt) {
	QPoint p = mapFromGlobal(QCursor::pos());
	emit mousePressed(p);
}

void QT_DrawLabel::drawEdgePts( QPaintEvent* e ) {
	char tempStr[512];
	QPixmap imgWidget(img.width(), img.height()); 
	QLabel::paintEvent(e);

	QPen pen(Qt::red, 5, Qt::SolidLine);
	QPen penBlue(Qt::blue, 1, Qt::SolidLine);
	QPainter painterScreen(this);
	QPainter painter(&imgWidget);

	painter.setPen(pen);
    painter.setRenderHint(QPainter::Antialiasing, true);
	QPoint qpoint(0,0);
	painter.drawImage(qpoint,img);

	painterScreen.setPen(pen);
    painterScreen.setRenderHint(QPainter::Antialiasing, true);
	painterScreen.drawImage(qpoint,img);
	painterScreen.setFont(QFont("Arial", 6));


	QPoint point, point2;
	QLine line;
	if(pts_vec==NULL)
		return;
	for(FastVec<point_t>::iterator it=pts_vec->begin(); it!=pts_vec->end(); it++) {
		if( it->idx == it->nonDiscPt )  continue;
		point.setY( it->y );  point.setX( it->x ); 
		point2.setY( (*pts_vec)[ it->nonDiscPt ].y ); point2.setX( (*pts_vec)[ it->nonDiscPt ].x ); 

		line.setP1( point );
		line.setP2( point2 );
		
		painterScreen.setPen( pen );
		painterScreen.drawPoint( point );
		painterScreen.setPen( penBlue );
		painterScreen.drawLine( line );
		painter.setPen( pen );
		painter.drawPoint( point );
		painter.setPen( penBlue );
		painter.drawLine( line );

		//painter.drawPolyline(triangle);
		//painterScreen.drawPolyline(triangle);
	}

	sprintf(tempStr, "edge_pts_%d.png", ++frame);
	QString fileName(tr(tempStr));
	imgWidget.save(fileName);
}

void QT_DrawLabel::drawTriangulation( QPaintEvent* e ) {
	char tempStr[512];
	QPixmap imgWidget(img.width(), img.height()); 
	QLabel::paintEvent(e);

	
	QPen pen(Qt::red, 1, Qt::SolidLine);
	QPainter painterScreen(this);
	QPainter painter(&imgWidget);

	painter.setPen(pen);
    painter.setRenderHint(QPainter::Antialiasing, true);
	QPoint qpoint(0,0);
	painter.drawImage(qpoint,img);

	painterScreen.setPen(pen);
    painterScreen.setRenderHint(QPainter::Antialiasing, true);
	painterScreen.drawImage(qpoint,img);
	painterScreen.setFont(QFont("Arial", 6));


	QPolygon triangle;
	if(regions_vec==NULL)
		return;
	for(FastVec<region>::iterator it=regions_vec->begin(); it!=regions_vec->end(); it++) {
		triangle.clear();
		triangle << QPoint(it->x[0], it->y[0]);
		triangle << QPoint(it->x[1], it->y[1]);
		triangle << QPoint(it->x[2], it->y[2]);
		triangle << QPoint(it->x[0], it->y[0]);
		painter.drawPolyline(triangle);
		painterScreen.drawPolyline(triangle);
	}

	//painterScreen.drawText( QRect( QPoint( 10, 10 ), QPoint( 20,20 ) ), Qt::AlignCenter, QString( "[255]" ) );

	// FIX!!!
	if(regions_vec->size() > 0) {
		sprintf(tempStr, "triangulation_%d.png", ++frame);
		QString fileName(tr(tempStr));
		imgWidget.save(fileName);
	}
}

void QT_DrawLabel::paintEvent(QPaintEvent* e)
{
	if(isInit == false)
		return;


	switch ( dispOption ) 
	{
		case TRIANG: drawTriangulation( e );  break;
		case EDGE_PT: drawEdgePts( e );  break;
		//default: return;
	}
}
