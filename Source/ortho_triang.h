#ifndef ORTHO_TRIANG_H
#define ORTHO_TRIANG_H

#include "Constants.h"

class orthoTriang {
public:
	orthoTriang* ptrFwd;
	orthoTriang* ptrBack;
	int idx;
	int idxPos;

	bool mark[MAX_NUM_THREADS];
	bool used;
	bool stillExists;
	int x[3], y[3];
	float z[3];
	
	orthoTriang();
	~orthoTriang();

    orthoTriang& operator=(const orthoTriang &rhs);
};

#endif
