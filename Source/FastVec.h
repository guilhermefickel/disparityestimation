#ifndef FASTVEC_H
#define FASTVEC_H

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

// This is my vector class. It allocs all the memory in the beggining and
// for the end user it works as contiguous vector. 
// # Insertion is log(N)
// # Access is Constant
// # RM is Constant (independent of the type T, since I simple change some
//   pointers
// The key idea is to not waste time in deallocing memory, which can be
// really slow depending on type T. 
// The second key idea is to do all the remove's in a batch, followed by
// insertions. This way the remove proceadure, which is log(N) will be
// diluted...
template<class T>
class FastVec {
private:
	T* data;
	int* ptrList;
	bool* erased;
	int vecSize;
	int maxVecSize;
	vector<int> holesVec;
	int numHoles;
	int numRemoveBeforeSort;
	T* firstPtr;
	T* lastPtr;
	int lastIdx;

public:
	FastVec() { 
        maxVecSize = 20;
		holesVec.resize(maxVecSize+1);
        try {
            erased = new bool[20];
        }
        catch (std::bad_alloc& ba)
          {
            std::cerr << "bad_alloc caught: " << ba.what() << '\n';
          }
        try {
            data = new T[20];
        }
        catch (std::bad_alloc& ba)
          {
            std::cerr << "bad_alloc caught: " << ba.what() << '\n';
          }
        try {
            ptrList = new int[20];
        }
        catch (std::bad_alloc& ba)
          {
            std::cerr << "bad_alloc caught: " << ba.what() << '\n';
          }
		firstPtr = lastPtr = data; 
		lastPtr->ptrFwd = lastPtr->ptrBack = firstPtr->ptrFwd = firstPtr->ptrBack = data;
		lastIdx = 0;
		vecSize = numHoles = numRemoveBeforeSort = 0; 
	};
	FastVec(int maxSize) { 
		maxVecSize = maxSize; 
		holesVec.resize(maxVecSize+1);
		erased = new bool[maxSize];
		data = new T[maxSize]; 
		ptrList = new int[maxSize];
		firstPtr = lastPtr = data; 
		lastPtr->ptrFwd = lastPtr->ptrBack = firstPtr->ptrFwd = firstPtr->ptrBack = data;
		lastIdx = 0;
		vecSize = numHoles = numRemoveBeforeSort = 0; 
	};
	~FastVec() { };

	 class iterator:public std::iterator<std::random_access_iterator_tag, T>
    {
        public:
            typedef iterator self_type;
            typedef T value_type;
            typedef T& reference;
            typedef T* pointer;
            //typedef std::random_access_iterator_tag iterator_category;
            typedef int difference_type;
			iterator() { }
            iterator(pointer ptr) : ptr_(ptr) { }
			self_type operator++() { self_type i = *this; ptr_ = ptr_->ptrFwd; return i; }
			self_type operator++(int junk) { ptr_ = ptr_->ptrFwd; return *this; }
            reference operator*() { return *ptr_; }
            pointer operator->() { return ptr_; }
            bool operator==(const self_type& rhs) { return ptr_ == rhs.ptr_; }
            bool operator!=(const self_type& rhs) { return ptr_ != rhs.ptr_; }
			bool operator<(const self_type& rhs) { return ptr_ < rhs.ptr_; }
        private:
            pointer ptr_;
    };

	void reserve(int newVecSize) {
        maxVecSize = newVecSize;
		holesVec.resize(maxVecSize+1);
		delete[] erased;
        try {
            erased = new bool[maxVecSize];
        }
        catch (std::bad_alloc& ba)
          {
            std::cerr << "bad_alloc caught: " << ba.what() << '\n';
          }
		delete[] data;
        try {
            data = new T[maxVecSize];
        }
        catch (std::bad_alloc& ba)
          {
            std::cerr << "bad_alloc caught: " << ba.what() << '\n';
          }
		delete[] ptrList;
        try {
            ptrList = new int[maxVecSize];
        }
        catch (std::bad_alloc& ba)
          {
            std::cerr << "bad_alloc caught: " << ba.what() << '\n';
          }
		firstPtr = lastPtr = data; 
		lastPtr->ptrFwd = lastPtr->ptrBack = firstPtr->ptrFwd = firstPtr->ptrBack = data;
		lastIdx = 0;
		vecSize = numHoles = numRemoveBeforeSort = 0; 
	}

	void clear() {
		numHoles = 0;
		vecSize = 0;
		numRemoveBeforeSort = 0;
		firstPtr = lastPtr = data; 
		lastPtr->ptrFwd = lastPtr->ptrBack = firstPtr->ptrFwd = firstPtr->ptrBack = data;
		lastIdx = 0;
	}
	 
	T& operator[](int index)
    {
       return data[index];
    }

    iterator begin()
    {
        return iterator(firstPtr);
    }
 
    iterator end()
    {
        return iterator(lastPtr);
    }

	iterator getIt(int idx) {
		return iterator(&data[idx]);
	}

	void rm(int index)
	{
		//if(data[index].ptrBack == NULL || data[index].ptrFwd == NULL)
		if(erased[index])
			return;

		erased[index] = true;
		if(index != lastIdx) {
			holesVec[numHoles] = index;
			numHoles++;
		}
		numRemoveBeforeSort++;
		ptrList[data[index].idxPos] = ptrList[vecSize-1];
		data[ptrList[vecSize-1]].idxPos = data[index].idxPos;
		vecSize--;

		if(&data[index] == firstPtr) {
			firstPtr = data[index].ptrFwd;
			return;
		}
		else if(&data[index] == (lastPtr-1)) {
			lastPtr = (data[index].ptrBack+1);
			data[index].ptrBack->ptrFwd = lastPtr;
			lastPtr->ptrBack = data[index].ptrBack;
			return;
		}

		data[index].ptrBack->ptrFwd = data[index].ptrFwd;
		data[index].ptrFwd->ptrBack = data[index].ptrBack;
		data[index].ptrFwd = data[index].ptrBack = NULL;
	}

	// copying data can be really slow. Avoid using this! Actually,
	// I deleted my implementation! It is almost identical to insert(T& newData)
	// and I didn't want to keep two pieces of almost identical code to mantain.
	void insert(T& newData) 
	{
		cout << "Waaaaay to slow, not used!" << endl;
		system("PAUSE");
	}

	// returns the iterator in the newly added position. Just fill
	// this with your data and you're good to go!
	iterator itInsert(int &idx) {
		T* newIdx;
		if(numHoles > 0) {
			if(numRemoveBeforeSort>0) {
				//cout << "I removed " << numRemoveBeforeSort << " before I make this sort. Is it good? Is it over 9000!?" << endl;
				sort(holesVec.begin(), holesVec.begin()+numHoles);
				numRemoveBeforeSort=0;
			}
			int newIdxAux = holesVec[numHoles-1]; // created this variable only for debug purposes
			newIdx = &data[newIdxAux];
			newIdx->idx = newIdxAux;
			idx = newIdxAux;
			numHoles--;

			// X X X O O O
			// X X X O O X
			if((newIdx) >= lastPtr) {
				newIdx->ptrBack = lastPtr->ptrBack;
				newIdx->ptrFwd = newIdx+1;
				// fix left neighbor ptrFwd. There is no right neighbor.
				(lastPtr->ptrBack)->ptrFwd = newIdx;
				lastPtr = newIdx+1;
				lastIdx = idx+1;
			} 
			else if(newIdx <= firstPtr) {
				newIdx->ptrFwd = firstPtr;
				newIdx->ptrBack = newIdx;
				// fix right neighbor ptrBack.
				firstPtr->ptrBack = newIdx;
				// update firstPtr.
				firstPtr = newIdx;
			} else 	{ //  X X X O O O X X
				newIdx->ptrBack = (newIdx+1)->ptrBack;
				newIdx->ptrFwd = newIdx+1;
				// fix left neighbor ptrFwd
				((newIdx+1)->ptrBack)->ptrFwd = newIdx;
				// fix right neighbor ptBack
				(newIdx+1)->ptrBack = newIdx;
			}
			ptrList[vecSize] = idx;
			newIdx->idxPos = vecSize;
			vecSize++;
			erased[idx] = false;
		}
		// first: lastPtr->ptrBack = first; lastPtr->ptrFwd = first;
		else {
			newIdx = lastPtr;	
			(lastPtr->ptrBack)->ptrFwd = newIdx;
			newIdx->ptrBack = lastPtr-1;
			newIdx->ptrFwd = newIdx+1;
			newIdx->idx = lastIdx;
			idx = lastIdx;
			lastPtr++;
			lastPtr->ptrBack = newIdx;
			lastPtr->ptrFwd = NULL;
			lastIdx++;
			ptrList[vecSize] = idx;
			newIdx->idxPos = vecSize;
			vecSize++;
			erased[idx] = false;
		}

		return iterator(newIdx);
	}
	
	bool exists(int idx) {
		return erased[idx];
	}

	int* getPtrList() {
		return ptrList;
	}

	int size() {
		return vecSize;
	}

	int maxIdx() {
		return lastIdx;
	}
};

#endif
