#include "qx_basic.h"
#include "qx_tree_filter.h"

qx_tree_filter::qx_tree_filter()
{
}
qx_tree_filter::~qx_tree_filter()
{
	clean();
}
void qx_tree_filter::clean()
{
}
int qx_tree_filter::init(vector<tuple<float, float, float, int, int> > *regions_vec, vector< pair<int,int> >* edges_vec, double sigma_range, double sigma_range_dist)
{
    m_regions_vec = regions_vec;
    m_edges_vec = edges_vec;
    m_nr_region = (int)regions_vec->size();
    m_nr_edge = (int)edges_vec->size();
    m_sigma_range = sigma_range;
    m_mst.init(regions_vec,edges_vec,sigma_range,sigma_range_dist);
        return(0);
}

int qx_tree_filter::build_tree()
{
    m_mst.mst();
	m_mst_parent=m_mst.get_parent();
	m_mst_nr_child=m_mst.get_nr_child();
	m_mst_children=m_mst.get_children();
	m_mst_rank=m_mst.get_rank();
	m_mst_weight=m_mst.get_weight();
	m_node_id=m_mst.get_node_id();

	return(0);
}
template<typename T>void qx_tree_filter::combine_tree(T*image_filtered)
{

}
template<typename T>void qx_tree_filter::init_tree_value(T*image,bool compute_weight)
{

}
int qx_tree_filter::filter(double*cost,double*cost_backup,int nr_plane)
{
	memcpy(cost_backup,cost,sizeof(double)*m_nr_region*nr_plane);
	int*node_id=m_node_id;
	int*node_idt=&(node_id[m_nr_region-1]);
	for(int i=0;i<m_nr_region;i++)
	{
		int id=*node_idt--;
		int id_=id*nr_plane;
		int nr_child=m_mst_nr_child[id];
		if(nr_child>0)
		{
			double*value_sum=&(cost_backup[id_]);
			for(int j=0;j<nr_child;j++)
			{
				int id_child=m_mst_children[id][j];
				int id_child_=id_child*nr_plane;
                double weight= exp(-m_mst_weight[id_child]/m_sigma_range);
				//value_sum+=m_mst_value_sum_aggregated_from_child_to_parent[id_child]*weight;
				double*value_child=&(cost_backup[id_child_]);
				for(int k=0;k<nr_plane;k++)
				{
					value_sum[k]+=(*value_child++)*weight;
				}
			}
		}
	}
	int*node_id0=node_id;
	int tree_parent=*node_id0++;
	int tree_parent_=tree_parent*nr_plane;
	memcpy(&(cost[tree_parent_]),&(cost_backup[tree_parent_]),sizeof(double)*nr_plane);
	for(int i=1;i<m_nr_region;i++)//K_00=f(0,00)[K_0-f(0,00)J_00]+J_00, K_00: new value, J_00: old value, K_0: new value of K_00's parent
	{
		int id=*node_id0++;
		int id_=id*nr_plane;
		int parent=m_mst_parent[id];
		int parent_=parent*nr_plane;

		double*value_parent=&(cost[parent_]);//K_0
		double*value_current=&(cost_backup[id_]);//J_00
		double*value_final=&(cost[id_]);
        double weight=exp(-m_mst_weight[id]/m_sigma_range);//f(0,00)

		for(int k=0;k<nr_plane;k++) 
		{
			double vc=*value_current++;
			*value_final++=weight*((*value_parent++)-weight*vc)+vc;
		}
		//printf("Final: [id-value-weight]: [%d - %3.3f - %3.3f]\n",id,m_mst_[id].value_sum_aggregated_from_parent_to_child,m_mst_[id].weight_sum_aggregated_from_parent_to_child);
	}
	return(0);
}
