/*$Id: qx_nonlocal_cost_aggregation.cpp,v 1.1 2007/02/16 04:11:12 liiton Exp $*/
#include "qx_basic.h"
#include "qx_nonlocal_cost_aggregation.h"
qx_nonlocal_cost_aggregation::qx_nonlocal_cost_aggregation()
{
    m_buf_f=NULL;
	m_buf_d2=NULL;
	m_disparity_regions=NULL;
}
qx_nonlocal_cost_aggregation::~qx_nonlocal_cost_aggregation()
{
    clean();
}
void qx_nonlocal_cost_aggregation::clean()
{
    qx_freef(m_buf_f); m_buf_f=NULL; // is this the correct function!??!? Check this later...
	qx_freed_3(m_buf_d2); m_buf_d2=NULL;

	qx_freeu_1(m_disparity_regions); m_disparity_regions=NULL;
}
int qx_nonlocal_cost_aggregation::init(vector<tuple<float, float, float, int, int> > *regions_vec, vector< pair<int,int> >* edges_vec, int nr_plane, double sigma_range, double sigma_range_dist, double max_color_difference, double max_gradient_difference, double weight_on_color)
{
	clean();

	m_regions_vec = regions_vec;
	m_edges_vec = edges_vec;
    m_nr_region = (int)regions_vec->size(); m_nr_edge = (int)edges_vec->size(); m_nr_plane=nr_plane;
    m_sigma_range=sigma_range, m_sigma_range_dist=sigma_range_dist;
	m_max_color_difference=max_color_difference; m_max_gradient_difference=max_gradient_difference;
	m_weight_on_color=weight_on_color;
	m_weight_on_color_inv=1-m_weight_on_color;

	m_buf_f=qx_allocf(6,m_nr_region);
	m_buf_d2=qx_allocd_3(4,m_nr_region,m_nr_plane);//qx_allocd_4(4,m_h,m_w,m_nr_plane);
	m_cost_vol=m_buf_d2[0];
	m_cost_vol_backup=m_buf_d2[1];
	m_cost_vol_temp=m_buf_d2[2];
	m_cost_vol_right=m_buf_d2[3];
	m_cost_min=m_buf_f[0];
	m_cost_temp=m_buf_f[1];
	m_cost=m_buf_f[2];
	for(int y=0;y<m_nr_region;y++) m_cost_temp[y]=QX_DEF_FLOAT_MAX;

    m_disparity_regions = qx_allocu1(m_nr_region);

	for(int i=0;i<256;i++) m_table[i]=exp(-double(i)/(m_sigma_range*255));
    m_tf.init(regions_vec, edges_vec,m_sigma_range,m_sigma_range_dist);
	return(0);
}
int qx_nonlocal_cost_aggregation::matching_cost( vector< vector<float> >* cost_vec )
{
	// copy the costs!!!
	int reg_idx=0;
	for( vector< vector<float> >::iterator r=cost_vec->begin(); r != cost_vec->end(); r++ ) {
		for( int d=0; d<m_nr_plane; d++ ) {
			m_cost_vol[reg_idx][d] = r->at(d);
		}
		reg_idx++;
	}

	image_copy(m_cost_vol_backup,m_cost_vol,m_nr_region,m_nr_plane);

	m_tf.build_tree();
	return(0);
}
int qx_nonlocal_cost_aggregation::disparity(unsigned char*disparity,vector< vector<float> >& cost_vec,bool use_nonlocal_post_processing)
{

//    for( int r=0; r<m_regions_vec->size(); r++ ) {
//        for (int i=0; i<m_nr_plane; i++ ) {
//            cout << m_cost_vol[r][i] << " ";
//        }
//        cout << endl;
//    }
//    cout << endl;

	image_copy(m_cost_vol,m_cost_vol_backup,m_nr_region,m_nr_plane);

//    for( int r=0; r<m_regions_vec->size(); r++ ) {
//        for (int i=0; i<m_nr_plane; i++ ) {
//            cout << m_cost_vol[r][i] << " ";
//        }
//        cout << endl;
//    }
//    cout << endl;

	m_tf.filter(m_cost_vol[0],m_cost_vol_temp[0],m_nr_plane);
	depth_best_cost(m_disparity_regions,m_cost_vol,m_nr_region,m_nr_plane);
    image_copy(disparity,m_disparity_regions,m_nr_region);

    for( int r=0; r<m_regions_vec->size(); r++ ) {
        float sumCost = 0;
        for (int i=0; i<m_nr_plane; i++ ) {
            sumCost += m_cost_vol[r][i];
        }
        for (int i=0; i<m_nr_plane; i++ ) {
            cost_vec[r][i] = 1.0-m_cost_vol[r][i]/sumCost;
        }
    }
	
	return(0);
}
