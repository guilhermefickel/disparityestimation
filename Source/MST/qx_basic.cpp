#include "qx_basic.h"

void boxcar_sliding_window_x(double *out,double *in,int h,int w,int radius)
{
    double scale = 1.0f / (2*radius+1);    
    for (int y = 0; y < h; y++) {    
        double t;
        // do left edge
        t = in[y*w] * radius;
        for (int x = 0; x < radius+1; x++) {
            t += in[y*w+x];
        }
        out[y*w] = t * scale;
        for(int x = 1; x < radius+1; x++) {
            int c = y*w+x;
            t += in[c+radius];
            t -= in[y*w];
            out[c] = t * scale;
        }        
        // main loop
        for(int x = radius+1; x < w-radius; x++) {
            int c = y*w+x;
            t += in[c+radius];
            t -= in[c-radius-1];
            out[c] = t * scale;
        }
        // do right edge
        for (int x = w-radius; x < w; x++) {
            int c = y*w+x;
            t += in[(y*w)+w-1];
            t -= in[c-radius-1];
            out[c] = t * scale;
        }
        
    }
}
void boxcar_sliding_window_y(double *out,double *in,int h,int w,int radius)
{
    double scale = 1.0f / (2*radius+1);    
    for (int x = 0; x < w; x++) 
	{    
        double t;
        // do left edge
        t = in[x] * radius;
        for (int y = 0; y < radius+1; y++) {
            t += in[y*w+x];
        }
        out[x] = t * scale;
        for(int y = 1; y < radius+1; y++) {
            int c = y*w+x;
            t += in[c+radius*w];
            t -= in[x];
            out[c] = t * scale;
        }        
        // main loop
        for(int y = radius+1; y < h-radius; y++) {
            int c = y*w+x;
            t += in[c+radius*w];
            t -= in[c-(radius*w)-w];
            out[c] = t * scale;
        }
        // do right edge
        for (int y = h-radius; y < h; y++) {
            int c = y*w+x;
            t += in[(h-1)*w+x];
            t -= in[c-(radius*w)-w];
            out[c] = t * scale;
        }        
    }
}
void boxcar_sliding_window(double **out,double **in,double **temp,int h,int w,int radius)
{
    boxcar_sliding_window_x(temp[0],in[0],h,w,radius);
    boxcar_sliding_window_y(out[0],temp[0],h,w,radius);
}

void boxcar_sliding_window_x(float *out,float *in,int h,int w,int radius)
{
    float scale = 1.0f / (2*radius+1);    
    for (int y = 0; y < h; y++) {    
        float t;
        // do left edge
        t = in[y*w] * radius;
        for (int x = 0; x < radius+1; x++) {
            t += in[y*w+x];
        }
        out[y*w] = t * scale;
        for(int x = 1; x < radius+1; x++) {
            int c = y*w+x;
            t += in[c+radius];
            t -= in[y*w];
            out[c] = t * scale;
        }        
        // main loop
        for(int x = radius+1; x < w-radius; x++) {
            int c = y*w+x;
            t += in[c+radius];
            t -= in[c-radius-1];
            out[c] = t * scale;
        }
        // do right edge
        for (int x = w-radius; x < w; x++) {
            int c = y*w+x;
            t += in[(y*w)+w-1];
            t -= in[c-radius-1];
            out[c] = t * scale;
        }
        
    }
}
void boxcar_sliding_window_y(float *out,float *in,int h,int w,int radius)
{
    float scale = 1.0f / (2*radius+1);    
    for (int x = 0; x < w; x++) 
	{    
        float t;
        // do left edge
        t = in[x] * radius;
        for (int y = 0; y < radius+1; y++) {
            t += in[y*w+x];
        }
        out[x] = t * scale;
        for(int y = 1; y < radius+1; y++) {
            int c = y*w+x;
            t += in[c+radius*w];
            t -= in[x];
            out[c] = t * scale;
        }        
        // main loop
        for(int y = radius+1; y < h-radius; y++) {
            int c = y*w+x;
            t += in[c+radius*w];
            t -= in[c-(radius*w)-w];
            out[c] = t * scale;
        }
        // do right edge
        for (int y = h-radius; y < h; y++) {
            int c = y*w+x;
            t += in[(h-1)*w+x];
            t -= in[c-(radius*w)-w];
            out[c] = t * scale;
        }
    }
}
void boxcar_sliding_window(float**out,float**in,float**temp,int h,int w,int radius)
{
    //int min_hw=min(h,w);
    int min_hw=(h<w?h:w);
	if(radius>=min_hw)
	{
		double dsum=0;
		float*in_=in[0];
		for(int y=0;y<h;y++) for(int x=0;x<w;x++) dsum+=*in_++;
		dsum/=(h*w);
		float fsum=(float)dsum;
		float*out_=out[0];
		for(int y=0;y<h;y++) for(int x=0;x<w;x++) *out_++=fsum;
	}
	else if(radius>0)
	{
		boxcar_sliding_window_x(temp[0],in[0],h,w,radius);
		boxcar_sliding_window_y(out[0],temp[0],h,w,radius);
	}
	else memcpy(out[0],in[0],sizeof(float)*h*w);
}
void boxcar_sliding_window_x(unsigned char*out,unsigned char*in,int h,int w,int radius)
{
    float scale = 1.0f / (2*radius+1);    
    for (int y = 0; y < h; y++) {    
        float t;
        // do left edge
        t = in[y*w] * radius;
        for (int x = 0; x < radius+1; x++) {
            t += in[y*w+x];
        }
        out[y*w] = (unsigned char)( t * scale+0.5 );
        for(int x = 1; x < radius+1; x++) {
            int c = y*w+x;
            t += in[c+radius];
            t -= in[y*w];
            out[c] = (unsigned char)(t * scale+0.5);
        }        
        // main loop
        for(int x = radius+1; x < w-radius; x++) {
            int c = y*w+x;
            t += in[c+radius];
            t -= in[c-radius-1];
            out[c] = (unsigned char)(t * scale+0.5);
        }
        // do right edge
        for (int x = w-radius; x < w; x++) {
            int c = y*w+x;
            t += in[(y*w)+w-1];
            t -= in[c-radius-1];
            out[c] = (unsigned char)(t * scale+0.5);
        }
        
    }
}
void boxcar_sliding_window_y(unsigned char*out,unsigned char*in,int h,int w,int radius)
{
    float scale = 1.0f / (2*radius+1);    
    for (int x = 0; x < w; x++) 
	{    
        float t;
        // do left edge
        t = in[x] * radius;
        for (int y = 0; y < radius+1; y++) {
            t += in[y*w+x];
        }
        out[x] = (unsigned char)(t * scale+0.5);
        for(int y = 1; y < radius+1; y++) {
            int c = y*w+x;
            t += in[c+radius*w];
            t -= in[x];
            out[c] = (unsigned char)(t * scale+0.5);
        }        
        // main loop
        for(int y = radius+1; y < h-radius; y++) {
            int c = y*w+x;
            t += in[c+radius*w];
            t -= in[c-(radius*w)-w];
            out[c] = (unsigned char)(t * scale+0.5);
        }
        // do right edge
        for (int y = h-radius; y < h; y++) {
            int c = y*w+x;
            t += in[(h-1)*w+x];
            t -= in[c-(radius*w)-w];
            out[c] = (unsigned char)(t * scale+0.5);
        }
    }
}
void boxcar_sliding_window(unsigned char**out,unsigned char**in,unsigned char**temp,int h,int w,int radius)
{
    //int min_hw=min(h,w);
    int min_hw=(h<w?h:w);
	if(radius<min_hw)
	{
		boxcar_sliding_window_x(temp[0],in[0],h,w,radius);
		boxcar_sliding_window_y(out[0],temp[0],h,w,radius);
	}
	else
	{
		double dsum=0;
		unsigned char*in_=in[0];
		for(int y=0;y<h;y++) for(int x=0;x<w;x++) dsum+=*in_++;
		dsum/=(h*w);
        unsigned char usum=(unsigned char)(dsum+0.5);
		unsigned char*out_=out[0];
		for(int y=0;y<h;y++) for(int x=0;x<w;x++) *out_++=usum;
	}
}


void depth_best_cost(unsigned char*depth,double**evidence,int s,int nr_planes)
{
	for(int y=0;y<s;y++) {int d; vec_min_pos(d,evidence[y],nr_planes); depth[y]=d;}
}
void vec_min_pos(int &min_pos,double *in,int len)
{
	double min_val=in[0];
	min_pos=0;
	for (int i=1;i<len;i++) if(in[i]<min_val)
	{
		min_val=in[i];	
		min_pos= i;
	}
}

int file_open_ascii(char *file_path,int *out,int len)
{
	FILE *file_in; char str[65]; int i;
    file_in=fopen(file_path,"r");
    //fopen_s(&file_in,file_path,"r");
	if(file_in!=NULL)
	{
		fseek(file_in,0,SEEK_SET);	
		for(i=0;i<len;i++ )
		{ 
            fscanf(file_in,"%s",str);
            //fscanf_s(file_in,"%s",str,65);
			out[i]=atoi(str);
		}
		fclose(file_in);
	}
	else
	{
		printf("qx_basic_file: Can not open file: %s\n",file_path);
		getchar();
		exit(-1);
	}
	return(0);
}
