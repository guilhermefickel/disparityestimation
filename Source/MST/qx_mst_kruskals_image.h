/***********************************************************************************
\Author:	Qingxiong Yang
\Function:	Compute minimum spanning tree from an image using Kruskal's algorithm
			(a faster implementation can be obtained using Prim's algorithm).
\Reference:	Qingxiong Yang, A Non-Local Cost Aggregation Method for Stereo Matching,
			IEEE Conference on Computer Vision and Pattern Recognition (CVPR) 2012.
************************************************************************************/
#ifndef QX_MST_KRUSKALS_IMAGE_H
#define QX_MST_KRUSKALS_IMAGE_H
#define QX_DEF_MST_MAX_NR_CHILD				64
#define QX_DEF_MST_KI_MIN_DISTANT			1
#define QX_DEF_MST_KI_4NR_NEIGHBOR			4
#define QX_DEF_MST_KI_8NR_NEIGHBOR			8
#define QX_DEF_MST_KI_PARENT_DEFAULT		-1
#define QX_DEF_MST_KI_SIGMA_RANGE			0.1
#include "qx_basic.h"
#include <iostream>
#include <vector>
#include <tuple>
#include <utility>


using namespace std;

class qx_queue_mst
{
public:
	qx_queue_mst();
	~qx_queue_mst();
	void clean();
	int	init(int len);
	void reinit();
	void push(int x);
	int pull();//return x: the first item in the queue;
	int *queue;
	int	length;
	int	first;
	int	last;
};
class qx_mst_kruskals_image
{
public:
	qx_mst_kruskals_image();
	~qx_mst_kruskals_image();
	void clean();
    int init(vector< tuple<float,float,float,int,int> >* regions_vec, vector< pair<int,int> >* edges_vec, double sigma_range, double sigma_range_dist);
    int mst();
public://output
	void update_table(double sigma_range);	
	int*get_rank(){return(m_rank);}
	int*get_parent(){return(m_parent);}
	int*get_nr_child(){return(m_nr_child);}
	int**get_children(){return(m_children);}
    double*get_weight(){return(m_weight);}
	int*get_node_id(){return(m_node_id_from_parent_to_child);}
private:
    vector< tuple<float,float,float,int,int> >* m_regions_vec;
    vector< pair<int,int> >* m_edges_vec;
    int m_nr_channel,m_nr_neighbor;
	int m_nr_region;
	int m_nr_edge,m_nr_vertices,m_parent_default,m_tree_size;
	int**m_edge,*m_id_edge;//compute edges
    double*m_distance;

    double m_sigma_range, m_sigma_range_dist;

	int*m_parent;//obtain tree edges
	int*m_nr_child,m_max_nr_child,**m_children;
    double*m_weight;
	int**m_connected;
    double**m_connected_distance;
	int*m_nr_connected;

	qx_queue_mst m_queue;//build tree
	int*m_node_id_from_parent_to_child;
	int*m_rank;
private:
	void init_mst();
	int findset(int x);
	void kruskal();
	int build_tree();
	//void reorder_edges(vector< pair< int, QX_PAIR > >&min_spanning_tree);
	//void print(vector< pair< int, QX_PAIR > >&min_spanning_tree);
};
void test_qx_mst_kruskals_image();
#endif
