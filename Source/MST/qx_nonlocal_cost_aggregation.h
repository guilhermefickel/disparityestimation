/******************************************************************************************
\Author:	Qingxiong Yang
\Function:	1. Compute matching cost using image color/intensity and gradient,
			2. Compute disparity map using filtering method presented in qx_tree_filter.h
\Reference:	Qingxiong Yang, A Non-Local Cost Aggregation Method for Stereo Matching,
			IEEE Conference on Computer Vision and Pattern Recognition (CVPR) 2012.
*******************************************************************************************/
#ifndef QX_NONLOCAL_COST_AGGREGATION_H
#define QX_NONLOCAL_COST_AGGREGATION_H
#include "qx_tree_filter.h"
#include <vector>
#include <utility>
#include <tuple>

using namespace std;

class qx_nonlocal_cost_aggregation
{
public:
    qx_nonlocal_cost_aggregation();
    ~qx_nonlocal_cost_aggregation();
    void clean();
    int init(
        vector< tuple<float,float,float,int,int> >* regions_vec, vector< pair<int,int> >* edges_vec,
        int nr_plane,
		double sigma_range=0.1,
        double sigma_range_dist=1.0,
		double max_color_difference=7,
		double max_gradient_difference=2,
		double weight_on_color=0.11
		);
    int matching_cost(vector< vector<float> >* cost_vec);
    int disparity(unsigned char*disparity, vector<vector<float> > &cost_vec, bool use_nonlocal_post_processing=false);
private:
	void compute_filter_weights(unsigned char***texture);
	void filter(float**image_filtered,float**image,bool compute_weight=true);
private:
    qx_tree_filter m_tf;
    vector< tuple<float,float,float,int,int> >* m_regions_vec;
    vector< pair<int,int> >* m_edges_vec;
    int	m_nr_region, m_nr_edge, m_nr_plane; double m_max_color_difference,m_max_gradient_difference,m_weight_on_color,m_weight_on_color_inv;
    unsigned char* m_disparity_regions;
    double m_table[256],m_sigma_range,m_sigma_range_dist;
    float**m_buf_f,*m_cost_min,*m_cost_temp,*m_cost;
	double***m_buf_d2,**m_cost_vol,**m_cost_vol_right,**m_cost_vol_backup,**m_cost_vol_temp;
};
#endif
