#ifndef DISPARITYESTIMATION_H
#define DISPARITYESTIMATION_H

#include <sbl/core/Config.h>
#include "MatchFunction.h"
#include "Aux/ImgOp.h"
#include "Aux/Timer.h"
#include "Mesh.h"
#include "Aux/MathOp.h"
#include "Aux/Raster.h"
#include "Aux/ImagesDaemon.h"
#include "MST/qx_nonlocal_cost_aggregation.h"
#include "MST/qx_basic.h"
#include "ParticleTrack.h"
#include "opencv2/gpu/gpu.hpp"


#include <ctime>
#include <algorithm>
#include <tuple>
#include <omp.h>

using namespace std;

class DisparityEstimation {
private:
	sbl::Config* config;
	Mesh3D* mesh;
	Raster* raster;
	MatchFunction matchFunction;
	ImgOp imOp;
	MathOp mathOp;
	FastVec<region> regions_vec;
	ImagesDaemon* imgDaemon;
	ParticleTrack* particleTrack;
    qx_nonlocal_cost_aggregation m_nlca;

    Timer timerTotalMatch;
    Timer timerMatch;
    Timer timerAgg;
    Timer timerFindTriang;
    Timer timerAlloc;
    Timer timerUpdateProb;
    Timer timerProbPath;
    Timer timerAll;

    bool debugMode;
    bool usingBilateralDisp;
    bool tempBilateralDisp;
    bool usingHMM;
    bool usingKalman;
    bool usingTempCost;
    bool useMST;


	int currTempPos; // current temporal position, necessary to control the temporal window
	cv::Mat dmImg;

	// search algorithm variables. Defined here to avoid several alloc and de-alloc within a single processed frame.
	int** list_neighbors;//[MAX_NUM_THREADS][MAX_NUM_TRIANG_NEIGHBORS];
	int* num_neighbors;//[MAX_NUM_THREADS];

	// infos
	int W, H;

    // Variables used to find the best probability path within pdfs of a single
    // triangle within the time.
    double**** Costs;
    int tempWindowProb;
    int maxDisp;
    double Weights[300];



	void calcInitialEstimative();
    float getAggValue(int currTriang, int delta, int threadIdx=0, bool temp=false);
	void getDispLimits(int it, int& firstDisp, int& lastDisp, int maxDisp, int threadIdx=0);
    void calcTempProbs();
    void findBestProbPath();
    void kalmanDisparity();
    void kalmanDisparityCorners();
    void bilateralDisp();
    void calcEdges( vector< pair<int,int> >& e_vec );
	cv::Mat getTriangulation();

public:
	DisparityEstimation();
	~DisparityEstimation();

	void setConfig(sbl::Config* configs);
	Mesh3D* calculateDisparityMap(int frame, bool doRefinement=true);

	void getDisparityMap(float dispMult, cv::Mat& rasterImg, bool color=false, bool tempCohesion=false);
	FastVec<region>* getRegionsVec();
	FastVec<point_t>* getPtsVec();
	cv::Mat* getEdgesImg();
	ImagesDaemon* getImgDaemon();
	int find_triang_from_pt( int y, int x );
    void getSimRegions( vector<int>& simTriangs, vector<float>& simVal, int currTriang );
    vector< tuple<float,int,int> >* getWeightLinks();
};



#endif
