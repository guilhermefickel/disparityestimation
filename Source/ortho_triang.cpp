#include "ortho_triang.h"

orthoTriang::orthoTriang() {
	for(int i=0; i<MAX_NUM_THREADS; i++)
		mark[i] = false;
	used=false;
	stillExists = false;
}

orthoTriang::~orthoTriang() {

}

orthoTriang& orthoTriang::operator=(const orthoTriang &rhs) {
	// Only do assignment if RHS is a different object from this.
	if (this != &rhs) {
		for(int i=0; i<MAX_NUM_THREADS; i++)
			this->mark[i] = rhs.mark[i];

		this->stillExists = rhs.stillExists;
		this->used = rhs.used;
		
		for(int i=0; i<3; i++) {
			this->x[i] = rhs.x[i];
			this->y[i] = rhs.y[i];
			this->z[i] = rhs.z[i];
		}

	}

	return *this;
}