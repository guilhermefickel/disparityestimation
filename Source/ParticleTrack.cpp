#include "ParticleTrack.h"

ParticleTrack::ParticleTrack() {
    config = NULL;
    imgDaemon = NULL;
    particle_vec.reserve(NUM_MAX_POINTS);
}

ParticleTrack::~ParticleTrack() {

}

void ParticleTrack::setConfig( sbl::Config* config, ImagesDaemon* imgDaemon ) {
    this->config = config;
    this->imgDaemon = imgDaemon;

    H = imgDaemon->getCenterImg()->rows;
    W = imgDaemon->getCenterImg()->cols;

    particlesIdx = new int*[H];
    for(int i=0; i<H; i++) {
        particlesIdx[i] = new int[W];
        for(int j=0; j<W; j++) {
            particlesIdx[i][j] = -1;
        }
    }

    distTransform = new cv::Mat( H, W, CV_32FC1 );

    occMask = cv::Mat::zeros( H, W, CV_8UC1 );
}

void ParticleTrack::init( ) {

}

void ParticleTrack::addParticles( vector<point_t>* pts_vec ) {
    int trash;
    int currentTime = imgDaemon->getCurrentTime();
    cv::Mat* im = imgDaemon->getCenterImg();

    for( int i=0; i<pts_vec->size(); i++ ) {
        FastVec<particle>::iterator it = particle_vec.itInsert( trash );
        it->x.clear();  it->y.clear();  it->R.clear();  it->G.clear();  it->B.clear();

        it->x.push_back( (*pts_vec)[i].x );
        it->y.push_back( (*pts_vec)[i].y );

        particlesIdx[ (*pts_vec)[i].y ][ (*pts_vec)[i].x ] = it->idx;

        it->R.push_back( im->data[im->step * (int)it->y[0] + int(it->x[0])*3 + 2 ] );
        it->G.push_back( im->data[im->step * (int)it->y[0] + int(it->x[0])*3 + 1 ] );
        it->B.push_back( im->data[im->step * (int)it->y[0] + int(it->x[0])*3 + 0 ] );
        it->beginTime = currentTime;
    }
}

vector<int>* ParticleTrack::getPtsToRM( ) {
    return &ptsToRM;
}

// 1 2 3
inline bool isFlowCorrect(cv::Point2f u)
{
    return !cvIsNaN(u.x) && !cvIsNaN(u.y) && fabs(u.x) < 1e9 && fabs(u.y) < 1e9;
}
void ParticleTrack::opticalFlow( cv::Mat frame0, cv::Mat frame1, cv::Mat &flowx, cv::Mat &flowy ) {
    cv::cvtColor(frame0, frame0, CV_BGR2GRAY);
    cv::cvtColor(frame1, frame1, CV_BGR2GRAY);

    cv::cuda::GpuMat d_frame0;//(*lastFrame);
    cv::cuda::GpuMat d_frame1;//(*(configs->getImgCenterPtr()));

    frame0.convertTo(frame0, CV_32FC1, 1.0 / 255.0);
    frame1.convertTo(frame1, CV_32FC1, 1.0 / 255.0);
    d_frame0.upload(frame0);
    d_frame1.upload(frame1);

    cv::cuda::GpuMat d_flowx(frame0.size(), CV_32FC1);
    cv::cuda::GpuMat d_flowy(frame1.size(), CV_32FC1);

    cv::Mat_<cv::Point2f> flow;
    cv::Ptr<cv::cuda::BroxOpticalFlow> brox = cv::cuda::BroxOpticalFlow::create(0.197f, 50.0f, 0.8f, 40, 277, 40);

    brox->calc(d_frame0, d_frame1, flow);

    flowx = cv::Mat::zeros( frame0.rows, frame0.cols, CV_32FC1 );
    flowy = cv::Mat::zeros( frame0.rows, frame0.cols, CV_32FC1 );

    for (int i=0; i<flow.rows; i++) {
        for (int j=0; j<flow.cols; j++) {
            cv::Point2f u = flow(i, j);

            if (!isFlowCorrect(u)) {
                flowx.at<float>(i,j) = flowy.at<float>(i,j) = 0;
            } else {
                flowx.at<float>(i,j) = u.x;
                flowy.at<float>(i,j) = u.y;
            }
        }
    }

}

void ParticleTrack::moveParticle( FastVec<point_t>::iterator it_p, float dx, float dy ) {
    bool inBorder=false;
    int idx = (int)particle_vec[it_p->idx].x.size()-1;
    if( particle_vec[it_p->idx].x[idx] == 0 || particle_vec[it_p->idx].x[idx] == W-1 || particle_vec[it_p->idx].y[idx] == 0 || particle_vec[it_p->idx].y[idx] == H-1 ) {
        inBorder = true;
        dx = dy = 0;
    }

    float fposX = particle_vec[it_p->idx].x[idx]+dx;
    float fposY = particle_vec[it_p->idx].y[idx]+dy;
    int posX = round( fposX );
    int posY = round( fposY );

    // check if it is within the optical flow occlusion. If it is, remove it.
//    if( occMask.data[ (int)round(particle_vec[it_p->idx].y[idx])*occMask.step + (int)round(particle_vec[it_p->idx].x[idx]) ] == 255 &&
//        it_p->dispDisc == false && inBorder == false ) {
//        particle_vec[it_p->idx].x.push_back( particle_vec[it_p->idx].x[idx] );
//        particle_vec[it_p->idx].y.push_back( particle_vec[it_p->idx].y[idx] );
//        ptsToRM.push_back( it_p->idx );
//        return;
//    }



    // out of boundary? Put in the "to remove" vector and return
    if( ( inBorder == false ) && ( fposX < 0 || fposX > W-1 || fposY < 0 || fposY > H-1 ) ) {
        particle_vec[it_p->idx].x.push_back( particle_vec[it_p->idx].x[idx] );
        particle_vec[it_p->idx].y.push_back( particle_vec[it_p->idx].y[idx] );
        ptsToRM.push_back( it_p->idx );
        it_p->mark[0] = true;
        return;
    }

    // 5 6  -> 1
    // is a particle from a disparity discontinuity? Make it stay on the edge!!!
    if( ( it_p->dispDisc || it_p->edge ) && inBorder == false ) {
        int dx, dy;
        getClosestEdge( posX, posY, dx, dy );



        fposX = posX+dx;
        fposY = posY+dy;
        posX = (int)round( fposX );
        posY = (int)round( fposY );

        if( fposX < 0 || fposX > W-1 || fposY < 0 || fposY > H-1) {
            particle_vec[it_p->idx].x.push_back( particle_vec[it_p->idx].x[idx] );
            particle_vec[it_p->idx].y.push_back( particle_vec[it_p->idx].y[idx] );
            ptsToRM.push_back( it_p->idx );
            it_p->mark[0] = true;
            return;
        }
    } else if( inBorder == false ) {

        // otherwise, is the color similar? If not, also remove and return
        float R1 = float( im->data[im->step * posY + posX*3 + 2 ] );
        float G1 = float( im->data[im->step * posY + posX*3 + 1 ] );
        float B1 = float( im->data[im->step * posY + posX*3 + 0 ] );

        float R2 = particle_vec[it_p->idx].R[ particle_vec[it_p->idx].R.size()-1 ];
        float G2 = particle_vec[it_p->idx].G[ particle_vec[it_p->idx].G.size()-1 ];
        float B2 = particle_vec[it_p->idx].B[ particle_vec[it_p->idx].B.size()-1 ];

//        float meanColor[3];
//        meanColor[0] = meanColor[1] = meanColor[2] = 0;
//        int firstFrame = ((int)particle_vec[it_p->idx].R.size()-8 < 0 ? 0 : (int)particle_vec[it_p->idx].R.size()-8);
//        for( int f=firstFrame; f<particle_vec[it_p->idx].R.size(); f++ ) {
//            meanColor[0] += particle_vec[it_p->idx].R[f];
//            meanColor[1] += particle_vec[it_p->idx].G[f];
//            meanColor[2] += particle_vec[it_p->idx].B[f];
//        }
//        meanColor[0] /= particle_vec[it_p->idx].R.size()-firstFrame;
//        meanColor[1] /= particle_vec[it_p->idx].R.size()-firstFrame;
//        meanColor[2] /= particle_vec[it_p->idx].R.size()-firstFrame;

//        R2 = meanColor[0];  G2 = meanColor[1];  B2 = meanColor[2];

        float colorSym = (R1-R2)*(R1-R2) + (G1-G2)*(G1-G2) + (B1-B2)*(B1-B2);


        if( colorSym > 20*20*3 && inBorder==false ) {
            particle_vec[it_p->idx].x.push_back( particle_vec[it_p->idx].x[idx] );
            particle_vec[it_p->idx].y.push_back( particle_vec[it_p->idx].y[idx] );
            ptsToRM.push_back( it_p->idx );
            return;
        }
    }

    particle_vec[it_p->idx].x.push_back( fposX );
    particle_vec[it_p->idx].y.push_back( fposY );
    particle_vec[it_p->idx].R.push_back( im->data[im->step * posY + posX*3 + 2 ] );
    particle_vec[it_p->idx].G.push_back( im->data[im->step * posY + posX*3 + 1 ] );
    particle_vec[it_p->idx].B.push_back( im->data[im->step * posY + posX*3 + 0 ] );

}

void ParticleTrack::getClosestEdge(  int x, int y, int &dx, int &dy  ) {
    int posX = x;
    int posY = y;
    int direction=-1;
    int steps=0;

    dx = dy = 0;

    float l;// = distTransform->ptr<float>(posY)[posX+1];
    float r;// = distTransform->ptr<float>(posY)[posX-1];
    float d;// = distTransform->ptr<float>(posY+1)[posX];
    float u;// = distTransform->ptr<float>(posY-1)[posX];
    float min=100000000;



    while( min > 0.1 && abs(dx)<8 && abs(dy)<8 && steps++<64) {
        l = distTransform->ptr<float>(posY)[posX-1];
        r = distTransform->ptr<float>(posY)[posX+1];
        d = distTransform->ptr<float>(posY+1)[posX];
        u = distTransform->ptr<float>(posY-1)[posX];

        if( l < min ) { min=l; direction=0; }
        else if( r < min ) { min=r; direction=1; }
        else if( d < min ) { min=d; direction=2; }
        else if( u < min ) { min=u; direction=3; }
        else break;

        switch( direction ) {
            case 0: posX--; dx-=1; break;
            case 1: posX++; dx+=1; break;
            case 2: posY++; dy+=1; break;
            case 3: posY--; dy-=1; break;
        }
        //cout << min << " ";
    }

    //cout << "["<<y<<","<<x<<"]->"<<"["<<dy<<","<<dx<<"]   ";

    if( abs(dx)==8 || abs(dy)==8 )
        dx = dy = 0;
}

void ParticleTrack::calcOccMask( cv::Mat frame1, cv::Mat frame2, int frameIndex ) {
    char nameD[1024];
    int erosion_size=6;
    cv::Mat element = cv::getStructuringElement(cv::MORPH_ELLIPSE,
                          cv::Size(2 * erosion_size + 1, 2 * erosion_size + 1),
                          cv::Point(erosion_size, erosion_size) );

    if( occMask.rows <= 0 )
        occMask = cv::Mat::zeros( H, W, CV_8UC1 );


    opticalFlow( frame2, frame1, bflowx, bflowy );

    for (int y = 0; y < H; y++)
          for (int x = 0; x < W; x++) {
              int row = y+int(round(flowy.at<float>(y,x)));
              int col = x+int(round(flowx.at<float>(y,x)));
              //int pastRow = y+int(round(pastBackFlowy.at<float>(y,x)));
              //int pastCol = x+int(round(pastBackFlowx.at<float>(y,x)));
              int pastRow = y+int(round(bflowy.at<float>(y,x)));
              int pastCol = x+int(round(bflowx.at<float>(y,x)));
              if( row<0 || col<0 || row>=H || col>=W ||
                  pastRow<0 || pastCol<0 || pastRow>=H || pastCol>=W ) {
                  occMask.at<unsigned char>(y,x) = 255;
                  continue;
              }
              if( fabs( flowx.at<float>(y,x)+bflowx.at<float>(row,col) ) < 0.25 &&
                  fabs( flowy.at<float>(y,x)+bflowy.at<float>(row,col) ) < 0.25 &&
                  fabs( bflowx.at<float>(y,x)+flowx.at<float>(pastRow,pastCol) ) < 0.25 &&
                  fabs( bflowy.at<float>(y,x)+flowy.at<float>(pastRow,pastCol) ) < 0.25 )
                  //fabs( pastBackFlowx.at<float>(y,x)+pastFlowx.at<float>(pastRow,pastCol) ) < 0.25 &&
                  //fabs( pastBackFlowy.at<float>(y,x)+pastFlowy.at<float>(pastRow,pastCol) ) < 0.25 )
                    occMask.at<unsigned char>(y,x) = 0;
              else
                    occMask.at<unsigned char>(y,x) = 255;
          }

    cv::dilate(occMask, occMask, element);
    sprintf( nameD, "occMask_%d.png", frameIndex );
    cv::imwrite( nameD, occMask);
}


void ParticleTrack::rmCloseParticles( FastVec< point_t >* pts_vec, FastVec< edge >* edges_vec ) {
    vector< int > badPts;


    // REDO THIS! Do 'for every edge e' and just see the distance of those two particles/vertices
    for( FastVec<point_t>::iterator it_p = pts_vec->begin(); it_p != pts_vec->end(); it_p++ ) {
        // if point is already marked to remove, continue
        if( it_p->mark[ 0 ] == true )
            continue;

        int p, x, y, x2, y2;
        badPts.clear();
        x = it_p->x;  y = it_p->y;

        for( int e = 0; e < it_p->numEdges; e++ ) {
            if( (*edges_vec)[ it_p->edges_vec[e] ].points_vec[0] == it_p->idx )
                p = (*edges_vec)[ it_p->edges_vec[e] ].points_vec[1];
            else
                p = (*edges_vec)[ it_p->edges_vec[e] ].points_vec[0];

            // if point is already marked to remove, continue
            if( (*pts_vec)[ p ].mark[ 0 ] == true )
                continue;

            x2 = (*pts_vec)[p].x;
            y2 = (*pts_vec)[p].y;

            if( (x-x2)*(x-x2) + (y-y2)*(y-y2) < 26 ) {
                (*pts_vec)[ p ].mark[0] = true;
                badPts.push_back( p );
                ptsToRM.push_back( p );
            }

        }


    }

}

void ParticleTrack::updateParticles( int frameIndex, FastVec<point_t>* pts_vec, FastVec<edge>* edges_vec, cv::Mat* edgesImg, vector< tuple<float,int,int> >* weightLinks ) {
    cv::Mat frame1, frame2;

    if( frameIndex == 0)
        return;

    this->edgesImg = edgesImg;

    frame1 = imgDaemon->getImg( frameIndex );
    frame2 = imgDaemon->getImg( frameIndex+1 );
    im = imgDaemon->getCenterImg();

    cv::distanceTransform( 255-*edgesImg, *distTransform, CV_DIST_L2, 3 );
    *distTransform = (*distTransform)*255;
    //normalize(*distTransform, *distTransform, 0.0, 255.0, cv::NORM_MINMAX);
    distTransform->convertTo( bw, CV_8UC1 );
    cv::imwrite( "DistTransform.png", bw );

    // read motion
    opticalFlow( frame1, frame2, flowx, flowy );
    calcOccMask( frame1, frame2, frameIndex-1 );

    for( FastVec<point_t>::iterator it_p = pts_vec->begin(); it_p != pts_vec->end(); it_p++ ) {
        int refY, refX;
        refY = (*pts_vec)[it_p->nonDiscPt].y;
        refX = (*pts_vec)[it_p->nonDiscPt].x;
        it_p->mark[0] = false;

        moveParticle( it_p, flowx.at<float>( refY, refX ), flowy.at<float>( refY,refX ) );
    }

    rmCloseParticles( pts_vec, edges_vec );

    // remove particles updating particlesIdx
    int numPtsToRM = ptsToRM.size();
    for( int i=0; i<ptsToRM.size(); i++ ) {
        int idx = particle_vec[ ptsToRM[i] ].x.size() - 1;
        int X = (int)round( particle_vec[ ptsToRM[i] ].x[idx-1] );
        int Y = (int)round( particle_vec[ ptsToRM[i] ].y[idx-1] );


        particlesIdx[ Y ][ X ] = -1;
        particle_vec.rm( ptsToRM[i] );
    }

    for( FastVec<particle>::iterator it = particle_vec.begin(); it != particle_vec.end(); it++ ) {
        int idx = it->x.size()-1;
        if( idx < 0)
            continue;

        // old position of the particle it
        int oX = (int)round( it->x[idx-1]);
        int oY = (int)round( it->y[idx-1]);
        particlesIdx[ oY ][ oX ] = -1;

    }

    for( FastVec<particle>::iterator it = particle_vec.begin(); it != particle_vec.end(); it++ ) {
        int idx = it->x.size()-1;
        int oX = (int)round( it->x[idx-1]);
        int oY = (int)round( it->y[idx-1]);
        if( particlesIdx[ oY ][ oX ] != -1 ) {
            cout << "Should be all -1!!!" << endl;
        }
    }

    // update particlesIdx for new location
    for( FastVec<particle>::iterator it = particle_vec.begin(); it != particle_vec.end(); it++ ) {
        int idx = it->x.size()-1;
        if( idx < 0)
            continue;

        // new position of the particle it
        int X = (int)round( it->x[idx] );
        int Y = (int)round( it->y[idx] );

        // if already exists a particle in this location I have a conflict. Remove this particle
        if( particlesIdx[ Y ][ X ] != -1 && particlesIdx[ Y ][ X ] != it->idx )
            ptsToRM.push_back( it->idx );
        else
            particlesIdx[ Y ][ X ] = it->idx;
    }
    // new particles to remove. They end it up in the same position
    for( int i=numPtsToRM; i<ptsToRM.size(); i++ ) {
        particle_vec.rm( ptsToRM[i] );
    }

    cout << "Frame " << frameIndex << endl;
}

void ParticleTrack::updateVertices(FastVec<point_t>* points_vec, int **ptsIdx) {
    vector<point> ptsChanged;
    vector<point> ptsToErase;
    point p, p2;

    for( FastVec<particle>::iterator it = particle_vec.begin(); it != particle_vec.end(); it++ ) {
        int idx = it->x.size()-1;
        if( idx == -1 )
            continue;

        int Y = (int)round( it->y[idx] );
        int X = (int)round( it->x[idx] );

        p2.y = (int)round( it->y.at(idx-1) );
        p2.x = (int)round( it->x.at(idx-1) );
        ptsToErase.push_back( p2 );
        (*points_vec)[it->idx].oldX = p2.x;
        (*points_vec)[it->idx].oldY = p2.y;
        (*points_vec)[it->idx].x = (int)round(it->x[idx]);
        (*points_vec)[it->idx].y = (int)round(it->y[idx]);
        p.y = round( it->y[idx] );
        p.x = round( it->x[idx] );
        ptsChanged.push_back( p );
    }

    // first set to -1 the old position. Ideally all positions of ptsIdx should be -1
    for( vector<point>::iterator it = ptsToErase.begin(); it != ptsToErase.end(); it++ ) {
        ptsIdx[ (int)it->y ][ (int)it->x ] = -1;
    }

    for( int i=0; i<H; i++ )
        for( int j=0; j<W; j++ )
            ptsIdx[ i ][ j ] = -1;

    for( FastVec<particle>::iterator it = particle_vec.begin(); it != particle_vec.end(); it++ ) {
        int idx = it->x.size()-1;
        int Y = (int)round(it->y[idx]);
        int X = (int)round(it->x[idx]);
        if( ptsIdx[ Y ][ X ] != -1 )
            cout <<"Already have a vertex in here..." << endl;
        else
            ptsIdx[ Y ][ X ] = it->idx;
    }


    //cout << "partivle_vec->" <<particle_vec.size() << "   points_vec->"<<points_vec->size() << endl;
    if( abs( particle_vec.size() - points_vec->size() ) > 0 ) {
        cout << "points_vec.size() != particle_vec.size() !!!!!" << endl;
        cout.flush();
    }

    //cout.flush();

    //checkPtsIdx();
}

int ParticleTrack::numParticles() {
    return (int)particle_vec.size();
}

void ParticleTrack::checkPtsIdx() {
    int count=0;
    for( int i=0; i<H; i++ )
        for( int j=0; j<W; j++ ) {
            if( particlesIdx[i][j] != -1 ) {
                int idx = particle_vec[ particlesIdx[i][j] ].x.size() - 1;
                int X = (int)round( particle_vec[ particlesIdx[i][j] ].x[idx] );
                int Y = (int)round( particle_vec[ particlesIdx[i][j] ].y[idx] );
                if( Y != i || X != j ) {
                    cout << "Problem with ptsIDX!!!";
                }
                count++;
            }
        }

    if( count != (int)particle_vec.size() ) {
        cout << "Diff number of ptsIdx and points_vec!!!"  << endl;
        for( FastVec<particle>::iterator it = particle_vec.begin(); it != particle_vec.end(); it++ ) {
            int idx = it->x.size() - 1;
            int Y = (int)round( it->y[idx] );
            int X = (int)round( it->x[idx] );
            if( particlesIdx[Y][X] != it->idx ) {
                cout << "Problem with particle it->idx in ....";
            }
        }
    }
}

int** ParticleTrack::getParticlesIdx() {
    return particlesIdx;
}

cv::Mat* ParticleTrack::getOccMask() {
    return &occMask;
}
