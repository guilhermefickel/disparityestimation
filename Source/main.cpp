
#include <QApplication>
#include "QT/QT_MainWidget.h"
#include <iostream>
#include <sbl/core/Config.h>
#include <sbl/core/String.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    if(argc != 2) {
        std::cout << "Usage Error: must provide de path and file name to the config file." << std::endl;
        exit(1);
    }
    QT_MainWidget* qt_mainWidget;  // main QT widget
    sbl::Config config;			   // config file

    config.allowDefault( true );
    config.load( sbl::String( argv[1] ) );

    qt_mainWidget = new QT_MainWidget();
    qt_mainWidget->setConfigs(&config);
    qt_mainWidget->show();


    return a.exec();
}
