#include "ScaleMap.h"


ScaleMap::ScaleMap() {
    minDistFromEdge = 9*9;
	edgePts = 0;
    edgePtRadius = 7;
    currFrame = 1;

	cannyR = cv::Mat::zeros(1, 1, CV_8UC1);
	cannyG = cv::Mat::zeros(1, 1, CV_8UC1);
	cannyB = cv::Mat::zeros(1, 1, CV_8UC1);
	pointsMap = cv::Mat::zeros(1, 1, CV_8UC1);
	temporalRun = false;

    directions[0].y = -1;  directions[0].x = 0;
    directions[1].y = -1;  directions[1].x = 1;
    directions[2].y = 0;  directions[2].x = 1;
    directions[3].y = +1;  directions[3].x = 1;
    directions[4].y = +1;  directions[4].x = 0;
    directions[5].y = +1;  directions[5].x = -1;
    directions[6].y = 0;  directions[6].x = -1;
    directions[7].y = -1;  directions[7].x = -1;

}

ScaleMap::~ScaleMap() {

}

void ScaleMap::config(int minDistFromEdge) {
	this->minDistFromEdge = minDistFromEdge;
}

vector<point_t>* ScaleMap::firstRun( cv::Mat* img ) {
	this->img = img;
	numAddedPts = 0;

	// alloc and init scale map variables
	pointsMap = cv::Mat::zeros(img->rows, img->cols, CV_8UC1);
	partMask = cv::Mat::zeros(img->rows, img->cols, CV_8UC1);
	scaleMap = cv::Mat::zeros(img->rows, img->cols, CV_8UC1);
	cannyR = cv::Mat::zeros(img->rows, img->cols, CV_8UC1);
	cannyG = cv::Mat::zeros(img->rows, img->cols, CV_8UC1);
	cannyB = cv::Mat::zeros(img->rows, img->cols, CV_8UC1);

    H = img->rows;
    W = img->cols;

	colorCanny();
	createScaleMap();
	findPts();

	return &pvec;
}

vector<point_t>* ScaleMap::update( cv::Mat* img, FastVec<point_t>* pts_vec ) {
	this->img = img;
	numAddedPts = 0;

	pointsMap = cv::Mat::zeros(img->rows, img->cols, CV_8UC1);
	partMask = cv::Mat::zeros(img->rows, img->cols, CV_8UC1);
	scaleMap = cv::Mat::zeros(img->rows, img->cols, CV_8UC1);

	colorCanny();
	createScaleMap();
    updatePts( pts_vec );

	return &pvec;
}

void ScaleMap::createScaleMap() {
	vector<cv::Mat> planes;
	vector<cv::Mat> planesScale[6];
    int maxScale = 5;
    float deltaS = 15;

	cv::Size szero(0,0);
	img->copyTo(scaleImgs[0]);


	#pragma omp parallel for
	for(int i=1; i<=5; i++) {
		cv::Mat localAux;
        cv::GaussianBlur(scaleImgs[0], scaleImgs[i], szero, pow(1.9,i), pow(1.9,i) );
        //resize(localAux, scaleImgs[i], scaleImgs[i].size(), 1.0/pow(1.9,i), 1.0/pow(1.9,i));

        char imName[1024];
        sprintf(imName,"scaleImgs_%d.png",i);
        cv::imwrite(imName,scaleImgs[i]);
    }


	#pragma omp parallel for
	for(int i=0; i<=maxScale; i++)
		cv::split(scaleImgs[i], planesScale[i]);

	#pragma omp parallel for
	for(int i=0; i<img->rows; i++) {
		unsigned char* ptr1 = planesScale[0][0].ptr<unsigned char>(i);
		unsigned char* ptr2 = planesScale[0][1].ptr<unsigned char>(i);
		unsigned char* ptr3 = planesScale[0][2].ptr<unsigned char>(i);
		for(int j=0; j<img->cols; j++) {  // for each row and col
			uchar maxLvl = 0;
			for(int scaleLvl = 1; scaleLvl <= maxScale; scaleLvl++) { // and for each level of scale images
                int yIndex = i;
                int xIndex = j;

				uchar* ptr1Scale = planesScale[scaleLvl][0].ptr<uchar>(yIndex);
				uchar* ptr2Scale = planesScale[scaleLvl][1].ptr<uchar>(yIndex);
				uchar* ptr3Scale = planesScale[scaleLvl][2].ptr<uchar>(yIndex);

				if(diffScalePts(i, j, ptr1[j], ptr2[j], ptr3[j], ptr1Scale[xIndex], ptr2Scale[xIndex], ptr3Scale[xIndex]) < deltaS)
					maxLvl++;
			}
            unsigned char* ptrSM = scaleMap.ptr<uchar>(i);
			ptrSM[j] = maxLvl;//*255/6;
		}
	}
	cv::GaussianBlur(scaleMap, scaleMap, szero, 2, 2);

    cv::imwrite( "scalemap.png", scaleMap*(255/5) );
}

void ScaleMap::findPts() {
	// Insert vertices along the boundaries
	for(int i=0; i<img->rows; i+=10)
		for(int j=0; j<img->cols; j+=img->cols-1) {
            insertVertex(i,j,7,true);
		}
    insertVertex(img->rows-1,0, 7,true);
    insertVertex(img->rows-1,img->cols-1, 7,true);

	for(int i=0; i<img->rows; i+=img->rows-1)
		for(int j=10; j<img->cols-10; j+=10) {
            insertVertex(i,j,7,true);
		}

	borderPts = pvec.size();

	int frameBorder = 3;
    // add new particles within the edges
    addEdgePts();
    imgEdges *= 255;

	edgePts = pvec.size();
    // add new particles using the scale map
	for (int y = frameBorder; y < img->rows - frameBorder; y++) {
		for (int x = frameBorder; x < img->cols - frameBorder; x++) {
            if (partMask.data[ y*partMask.step + x ] == 0 && pointsMap.data[ y*partMask.step + x ] == 0 ) {
				int sep = distFromScale( y, x );
				point_t p;
				p.x = x;  p.y = y;
                p.edge = false;
                p.radius = sqrt(float(sep));
				drawCircleFilled( x, y, sqrt(float(sep)), 255 );
				pvec.push_back( p );
			}
		}
	}
}

bool ScaleMap::findEdgePt( Pt &p ) {
    int frameBorder = 3;
    for (int y = frameBorder; y < img->rows - frameBorder; y++) {
        for (int x = frameBorder; x < img->cols - frameBorder; x++) {
            if (imgEdges.data[ y*imgEdges.step + x ] == 255 && partMask.data[ y*partMask.step + x ] == 0 ) {
                p.x = x;  p.y = y;
                return true;
            }
        }
    }

    return false;
}

void ScaleMap::markEdgePt( Pt p ) {
    imgEdges.data[ imgEdges.step*p.y + p.x ] = 1;
}

void ScaleMap::findNextPts( Pt p, vector<Pt>& nextPts ) {
    Pt paux;
    int lastInd=8;
    // for all directions
    for( int i=0; i<lastInd; i++ ) {
        int y = p.y + directions[i].y;
        int x = p.x + directions[i].x;
        // if this direction goes to a valid position
        if( y>0 && y<H-1 && x>0 && x<W-1 ) {
            // is it an edge?
            if( imgEdges.data[ y*imgEdges.step + x ] == 255 ) {
                paux.x = x;  paux.y = y;
                nextPts.push_back( paux );
                markEdgePt( paux );
                if( i==0 ) {
                    lastInd--;
                    paux.y = p.y + directions[7].y;
                    paux.x = p.x + directions[7].x;
                    markEdgePt( paux );
                }
                i++;
                paux.y = p.y + directions[i].y;
                paux.x = p.x + directions[i].x;
                markEdgePt( paux );
            }
        }

    }
}

void ScaleMap::startPath( Pt p ) {
    vector<Pt> nextPts;
    vector<Pt> bufferPts; // points soft-added, i.e. it is yet to be confirmed if they will be added
    int totalLen = 1; // total length of the edge
    int lastPtLen = 1; // total length from the last inserted pt

    markEdgePt( p );
    findNextPts( p, nextPts );

    while( nextPts.size() > 0 ) {
        // start new paths
        if( nextPts.size() > 1 ) {
            addBufferPts( bufferPts );
            //insertVertex( p.y, p.x, edgePtRadius, true );
            for( vector<Pt>::iterator it = nextPts.begin(); it != nextPts.end(); it++ )
                startPath( *it );
            // and end this path
            return;
        }
        p = nextPts[0];
        markEdgePt( p );
        if( lastPtLen > edgePtRadius && partMask.data[ p.y*partMask.step + p.x ] == 0) {
            //insertVertex( p.y, p.x, edgePtRadius, true );
            bufferPts.push_back( p );
            lastPtLen = 0;
        }
        if( bufferPts.size() >= 200 )// it is getting to big. Is it a bug or just a long edge?
            addBufferPts( bufferPts );

        lastPtLen++;
        totalLen++;
        nextPts.clear();
        markEdgePt( p );

        findNextPts( p, nextPts );
    }
    addBufferPts( bufferPts );
}

// add pts in buffer. Try to ad the minimum amount of them.
void ScaleMap::addBufferPts( vector<Pt>& bufferPts ) {
    if( bufferPts.size() == 0 ) return;
    int first, last;
    first = last = 0;

    for( int i = 0; i < bufferPts.size(); i++ ) {
        last = i;
        if( last-first < 2 ) continue;

        // if the line formed by the first and last point do not represent well the edge OR
        // the distance between first and last is to big (don't want a triangle to big)
        if( maxError( bufferPts, first, last ) > 0.5 || (last-first)>4 ) {
            insertVertex( bufferPts[first].y, bufferPts[first].x, (last-1-first)*edgePtRadius, true );
            first = last;
        }
    }

    if(first != last) {
        insertVertex( bufferPts[first].y, bufferPts[first].x, (last-first)*edgePtRadius+1, true );
        insertVertex( bufferPts[last].y, bufferPts[last].x, edgePtRadius+1, true );
    } else {
        insertVertex( bufferPts[first].y, bufferPts[first].x, edgePtRadius+1, true );
    }

    bufferPts.clear();
}

float ScaleMap::maxError( vector<Pt>& bufferPts, int first, int last ) {
    float maxErr = -1;
    for( int i=first+1; i<last; i++ ) {
        point ap;
        point n;
        point final;
        float dotAPN;
        float err;
        float sizeN;

        ap.x = bufferPts[first].x-bufferPts[i].x;
        ap.y = bufferPts[first].y-bufferPts[i].y;
        n.x = bufferPts[last].x-bufferPts[first].x;
        n.y = bufferPts[last].y-bufferPts[first].y;
        sizeN = sqrt( n.x*n.x + n.y*n.y );
        n.x /= sizeN;
        n.y /= sizeN;

        dotAPN = ap.x*n.x + ap.y*n.y;
        final.x = ap.x - n.x*dotAPN;
        final.y = ap.y - n.y*dotAPN;

        err = sqrt( final.x*final.x + final.y*final.y );
        if( err > maxErr ) maxErr = err;
    }

    return maxErr;
}

void ScaleMap::addEdgePts( bool addPt ) {

    int frameBorder = 3;
    // add new particles
    for (int y = frameBorder; y < img->rows - frameBorder; y++) {
        for (int x = frameBorder; x < img->cols - frameBorder; x++) {
            if (imgEdges.data[ y*imgEdges.step + x ] == 255 && partMask.data[ y*partMask.step + x ] == 0 ) {
                if( pointsMap.data[ pointsMap.step*y + x ] == 255 ) {
                    cout << "There is already a point in here!!!" << endl;
                    continue;
                }
                int sep = edgePtRadius;
                point_t p;
                p.x = x;  p.y = y;
                p.edge = true;
                drawCircleFilled( x, y, sep, 255 );
                pointsMap.data[ pointsMap.step*y + x ] = 255;
                if( addPt ) pvec.push_back( p );
            }
        }
    }

//    Pt p;
//    vector<Pt> nextPts;

//    // if I can't find an edge point, return
//    while( findEdgePt(p) != false ) {
//        // mark this pt as visited!
//        markEdgePt( p );
//        //insertVertex( p.y, p.x, edgePtRadius, true );

//        // find next pts
//        findNextPts( p, nextPts );

//        // and go to them
//        for( vector<Pt>::iterator it = nextPts.begin(); it != nextPts.end(); it++ )
//            startPath( *it ); // number of pts

//        nextPts.clear();
//    }

}

void ScaleMap::updatePts( FastVec<point_t>* pts_vec ) {
	int frameBorder = 3;
	pvec.clear();
	vector<point_t> bufferPts;

    // add previous edge particles
    for( FastVec<point_t>::iterator it_p = pts_vec->begin(); it_p != pts_vec->end(); it_p++ ) {
		point_t p;  int sep;
		p.x = it_p->x;  p.y = it_p->y;
		p.oldX = it_p->oldX;  p.oldY = it_p->oldY;
		p.edge = true;
        if( it_p->edge ) {
            sep = edgePtRadius;
            //sep = it_p->radius;
			drawCircleFilled( p.x, p.y, sep, 255 );
            pointsMap.data[ p.y*pointsMap.step + p.x ] = 255;
        }
	}



    // add previous particles from scale map but with a small scale
    for( FastVec<point_t>::iterator it_p = pts_vec->begin(); it_p != pts_vec->end(); it_p++ ) {
        point_t p;  int sep;
        p.x = it_p->x;  p.y = it_p->y;
        p.oldX = it_p->oldX;  p.oldY = it_p->oldY;
        int x = p.x;  int y = p.y;

        if( it_p->edge == false ) {
            if( pointsMap.data[ p.y*pointsMap.step + p.x ] == 255 ) {
                cout << "ScaleMap::updatePts -> already exists a point in here!!"<<endl;
            }
            sep = 6*6;//distFromScale( y, x );
            drawCircleFilled( p.x, p.y, sqrt( float( sep ) ), 255 );
            pointsMap.data[ p.y*pointsMap.step + p.x ] = 255;
        }
    }


    char imName[1024];
    sprintf( imName, "scaleMapBeforeNewEdgePts.png" );
    cv::imwrite( imName, partMask );
    sprintf( imName, "ptsMask.png" );
    cv::imwrite( imName, pointsMap );

    // add new particles within edges
    addEdgePts();
    imgEdges *= 255;


    // add previous particles from scale map but with correct radius
    for( FastVec<point_t>::iterator it_p = pts_vec->begin(); it_p != pts_vec->end(); it_p++ ) {
        point_t p;  int sep;
        p.x = it_p->x;  p.y = it_p->y;
        p.oldX = it_p->oldX;  p.oldY = it_p->oldY;
        int x = p.x;  int y = p.y;

        if( it_p->edge == false ) {
            sep = distFromScale( y, x );
            drawCircleFilled( p.x, p.y, sqrt( float( sep ) ), 255 );
            it_p->radius = sqrt( float( sep ) );
        }
    }


    sprintf( imName, "scaleMapFill.png" );
    cv::imwrite( imName, partMask );

    // add new particles using scale map
    for (int y = frameBorder; y < img->rows - frameBorder; y++) {
        for (int x = frameBorder; x < img->cols - frameBorder; x++) {
            if (partMask.data[ y*partMask.step + x ] == 0) {
                int sep = distFromScale( y, x );
                point_t p;
                p.x = x;  p.y = y;
                p.edge = false;
                p.radius = sqrt(float(sep));
                drawCircleFilled( x, y, sqrt(float(sep)), 255 );
                pointsMap.data[ p.y*pointsMap.step + p.x ] = 255;
                pvec.push_back( p );

            }
        }
    }

    //cout << "Adding " << pvec.size() << " points!!!" << endl;
}

void ScaleMap::colorCanny() {
  //int lowThreshold = 40;
    cv::Mat grayImg;
    int lowThreshold = 20; //12
    vector<cv::Mat> imagePlanes;

    cv::split( *img, imagePlanes );


    img->convertTo( grayImg, CV_8UC1 );

  /// Reduce noise with a gaussian blur
  // cv::GaussianBlur( imagePlanes[0], cannyR, cv::Size(5,5) , 2.6 );
  // cv::GaussianBlur( imagePlanes[1], cannyG, cv::Size(5,5) , 2.6 );
  // cv::GaussianBlur( imagePlanes[2], cannyB, cv::Size(5,5) , 2.6 );

  cv::GaussianBlur( grayImg, grayImg, cv::Size(5,5) , 2.6 );

  /// Canny detector
  //Canny( cannyR, cannyR, lowThreshold, lowThreshold*3, 3, false );
  //Canny( cannyG, cannyG, lowThreshold, lowThreshold*3, 3, false );
 // Canny( cannyB, cannyB, lowThreshold, lowThreshold*3, 3, false );

  Canny( grayImg, imgEdges, lowThreshold, lowThreshold*3, 3, false );

   //imgEdges = cannyR+cannyG+cannyB; // + equals the OR operator in this case

  int morph_size = 1;
  //cv::Mat element = cv::getStructuringElement( cv::MORPH_ELLIPSE, cv::Size( 2*morph_size + 1, 2*morph_size+1 ), cv::Point( morph_size, morph_size ) );

    /// Apply the specified morphology operation
  // cv::morphologyEx( imgEdges, imgEdges, cv::MORPH_CLOSE, element );


  char edgeImgName[1024];
  sprintf(edgeImgName, "edges_%d.png", currFrame++);
  //cv::imread(edgeImgName);

  cv::imwrite(edgeImgName, imgEdges);

  //imshow("Edges", imgEdges);
 // cv::waitKey();
}


/*****************************************************************************************
/*****************************************************************************************
/*************************         Auxiliar Functions                 ********************
/****************************************************************************************/
/// draws a filled circle in the image
void ScaleMap::drawCircleFilled(int x, int y, int radius, int v ) {
    // check bounds
    if( radius < 0 ) {
        cout << "Why is radius negative?" << endl;
        radius *= -1;
    }
    int xMin = x - radius, xMax = x + radius;
    int yMin = y - radius, yMax = y + radius;
    int width = img->cols, height = img->rows;
    if (xMin < 0) xMin = 0;
    if (xMin >= width) xMin = width - 1;
    if (xMax >= width) xMax = width - 1;
    if (yMin < 0) yMin = 0;
    if (yMin >= height) yMin = height - 1;
    if (yMax >= height) yMax = height - 1;
    int rsq = radius * radius;

    // draw the circle
    for (int yc = yMin; yc <= yMax; yc++) {
        for (int xc = xMin; xc <= xMax; xc++) {
            int xd = xc - x;
            int yd = yc - y;
            if (xd * xd + yd * yd < rsq) {
                //img.data( xc, yc ) = v;
				partMask.data[ yc*partMask.step + xc ] = (uchar)v;
            }
        }
    }
}

void ScaleMap::insertVertex( int y, int x, int radius, bool isEdge ) {
	point_t p;

    if( pointsMap.data[ pointsMap.step*y + x ] != 0 )
        return;

	p.x = x;  p.y = y;
	p.edge = isEdge;
    p.radius = radius;
    if( radius < 1 ) {
        cout << "Small radius!?" << endl;
    }
    drawCircleFilled( x, y, radius, 255 );
	pvec.push_back( p );
    pointsMap.data[ p.y*pointsMap.step + p.x ] = 255;
}

inline float ScaleMap::diffScalePts(int i, int j, uchar val1, uchar val2, uchar val3, uchar val1Scale, uchar val2Scale, uchar val3Scale) {
	return sqrt(float(pow(float(val1)-float(val1Scale),2) + pow(float(val2)-float(val2Scale),2) + pow(float(val3)-float(val3Scale),2)));
}

float ScaleMap::distFromScale(int y, int x) {
    //return pow(1.5f,((float)scaleMap.ptr<uchar>(y)[x]+6)*2.0f);
    return pow(1.5f,((float)scaleMap.ptr<uchar>(y)[x]+6)*2.0f);
}

vector<point_t>* ScaleMap::getPts(int& edgePts, int& borderPts) {
	edgePts = this->edgePts;
	borderPts = this->borderPts;

	return &pvec;
}

cv::Mat* ScaleMap::getEdgesImg() {
	return &imgEdges;
}

void ScaleMap::runEdgeDetector(cv::Mat* img) {
	this->img = img;
	colorCanny();
}
