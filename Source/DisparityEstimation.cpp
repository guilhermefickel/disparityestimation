#include "DisparityEstimation.h"

DisparityEstimation::DisparityEstimation() {
    mesh = new Mesh3D;
    raster = new Raster;
    matchFunction.setRaster(raster);
    imgDaemon = new ImagesDaemon;
    particleTrack = new ParticleTrack;

    currTempPos = 0;
    timerMatch.setName( "Matching cost" );
    timerMatch.init();
    timerTotalMatch.setName( "Total Matching cost" );
    timerTotalMatch.init();
    timerAgg.setName( "Aggregation" );
    timerAgg.init();
    timerFindTriang.setName( "FindTriangs" );
    timerFindTriang.init();
    timerAlloc.setName( "Alloc" );
    timerAlloc.init();
    timerUpdateProb.setName( "Update Probabilities" );
    timerUpdateProb.init();
    timerProbPath.setName( "Prob Path" );
    timerProbPath.init();
    timerAll.setName( "All" );
    timerAll.init();

}

DisparityEstimation::~DisparityEstimation() {

}

void DisparityEstimation::setConfig(sbl::Config* config) {
    //int MAX_NUM_TRIANGS = config->readInt( "MaxNumberTriangs" );
    debugMode = config->readBool( "debug", false );
    this->config = config;
    imgDaemon->setConfig( config );
    particleTrack->setConfig( config, imgDaemon );
    maxDisp = config->readInt( "maxDisp" );
    tempWindowProb = config->readInt( "tempWindowProb", 900 );
    usingBilateralDisp = config->readBool( "bilateralDisp", 1 );
  //  usingBilateralDisp = false;
    cout << "Using bilateral disp: " << usingBilateralDisp << endl;
    tempBilateralDisp = config->readBool( "tempBilateralDisp", 1 );
    // tempBilateralDisp = true;
    cout << "Using temp bilateral disp: " << tempBilateralDisp << endl;
    usingHMM = config->readBool( "HMM", 1 );
    usingKalman = config->readBool( "Kalman", 1 );
    usingTempCost = config->readBool( "tempCost", 0 );
    useMST = config->readBool( "useMST", 1 );


    H = imgDaemon->getCenterImg()->rows;
    W = imgDaemon->getCenterImg()->cols;

    regions_vec.reserve(MAX_NUM_TRIANGS);
    omp_set_num_threads(MAX_NUM_THREADS);

    list_neighbors = new int*[MAX_NUM_THREADS];
    num_neighbors = new int[MAX_NUM_TRIANGS];
    for(int i=0; i<MAX_NUM_THREADS; i++)
        list_neighbors[i] = new int[MAX_NUM_TRIANGS];

    Costs = new double***[MAX_NUM_THREADS];
    for(int i=0; i<MAX_NUM_THREADS; i++) {
        Costs[i] = new double**[MAX_NUM_TRIANGS];
        for( int t=0; t<MAX_NUM_TRIANGS; t++ ) {
            Costs[i][t] = new double*[2];
            Costs[i][t][0] = new double[maxDisp];
            Costs[i][t][1] = new double[maxDisp];
        }
    }

    double tempSmoothSigma = config->readDouble( "tempSmoothSigma", 1.55 );
    for( int i=0; i<300; i++ ) {
        Weights[i] = exp(-i*i/tempSmoothSigma );
    }


    dmImg = cv::Mat::zeros(H, W, CV_8UC1);
    mesh->setConfig(config, raster, &regions_vec, imgDaemon, particleTrack);
}

Mesh3D* DisparityEstimation::calculateDisparityMap(int frame, bool doRefinement) {
    cv::Mat dm;
    char imName[1024];
    // Now starting...

    timerAll.start();

    if( frame != config->readInt( "initialFrame" ) ) {
        imgDaemon->nextFrame( );
        particleTrack->updateParticles( imgDaemon->getCurrentTime(), mesh->getPtsVec(), mesh->getEdgesVec(), mesh->getEdgesImg(), mesh->getWeightLinks() );
        mesh->run( );  // create the triangular regions
    }
    else {
        mesh->run(  );  // create the triangular regions
        particleTrack->updateParticles( imgDaemon->getCurrentTime(), mesh->getPtsVec(), mesh->getEdgesVec(), mesh->getEdgesImg(), mesh->getWeightLinks() );
    }

    calcInitialEstimative(); // calculate the initial disparity estimation, save informations on regions_vec
    if( usingBilateralDisp ) bilateralDisp();
    //getDisparityMap( 1.0, dm );
    //sprintf(imName,"dmDispFilter_%d.png",currTempPos);
    //cv::imwrite(imName,dm);
    if(doRefinement)
        mesh->refine();

    timerAll.printTime();

    getDisparityMap( 1.0, dm );
    sprintf(imName,"dmRefined_%d.png",currTempPos);
    cv::imwrite(imName,dm);

    if( usingKalman ) kalmanDisparityCorners();
    getDisparityMap( 1.0, dm );
    sprintf(imName,"dmKalman_%d.png",currTempPos);
    cv::imwrite(imName,dm);

    // fix this!!! Triang size keeps growing!!!!
    mesh->saveMesh3D( regions_vec );
    mesh->calcWeightLinks( regions_vec, particleTrack->getOccMask() );
    getDisparityMap( 1.0, dm );

    sprintf(imName,"dm_final_%d.png",currTempPos);
    cv::imwrite(imName,dm);

    if( debugMode ) {
        char linkName[1024];
        sprintf( linkName, "links_%d.png", imgDaemon->getCurrentFrame() );
        cv::imwrite( linkName, imOp.linksImg( &dm, mesh->getWeightLinks(), mesh->getPtsVec() )  );
    }

    currTempPos++;
    return mesh;
}

void DisparityEstimation::calcInitialEstimative() {
    int* ptrList;
    int maxDisp = config->readInt( "maxDisp" );
    vector<float> probs[MAX_NUM_THREADS];
    vector<float> ratioCost[MAX_NUM_THREADS];


    // configure the match function!!! Important!!!!
    matchFunction.setConfig(config, &regions_vec, imgDaemon);
    matchFunction.setRaster(raster);
    raster->setRegions(&regions_vec);

    ptrList = regions_vec.getPtrList();

    timerTotalMatch.cont();
#pragma omp parallel for
    for(int it_r=0; it_r<regions_vec.size(); it_r++) { // for all regions
        float max_cost, sum_costs;
        int bestx=0;
        int firstDisp, lastDisp;
        int threadIdx = (int)omp_get_thread_num();
        int it = ptrList[it_r];

        max_cost = -999999;
        // set the raster 'function' for later use, on the matching process
        raster->setTriangle(it,threadIdx);
        // getDispLimits(it, firstDisp, lastDisp, maxDisp, threadIdx);
        firstDisp = 0;
        lastDisp = maxDisp-1;


        // SLOW!!!!
        probs[threadIdx].clear();
        ratioCost[threadIdx].clear();
        for(int delta=0; delta <= lastDisp; delta++) {
            probs[threadIdx].push_back( 0.0 );
            ratioCost[threadIdx].push_back( 0.0 );
        }

        timerMatch.cont();
        for(int delta = firstDisp ; delta < lastDisp; delta++) {  // for each disparity value
             probs[threadIdx][delta] = matchFunction.match(it, delta, ratioCost[threadIdx][delta], threadIdx);
         }

        if( probs[threadIdx][lastDisp-1] < 0 )
            for(int delta = firstDisp ; delta < lastDisp; delta++)  // for each disparity value
                 probs[threadIdx][delta] = 0.0;

        timerMatch.pause();

        //regions_vec[it].sum_costs=sum_costs;
        sum_costs=0;
        for(int i=firstDisp; i<lastDisp; i++) {
            sum_costs += probs[threadIdx][i];
            if(probs[threadIdx][i] > max_cost) {
                max_cost = probs[threadIdx][i];
                bestx = i;
            }
        }
        //bestx = 40;
        regions_vec[it].z[0] = regions_vec[it].z[1] = regions_vec[it].z[2] = (float)bestx;
        regions_vec[it].zClosed[0] = regions_vec[it].zClosed[1] = regions_vec[it].zClosed[2] = (float)bestx;
        regions_vec[it].zRef[0] = regions_vec[it].zRef[1] = regions_vec[it].zRef[2] = (int)bestx;
        regions_vec[it].zTemp[0] = regions_vec[it].zTemp[1] = regions_vec[it].zTemp[2] = (float)bestx;
        regions_vec[it].zTempRef[0] = regions_vec[it].zTempRef[1] = regions_vec[it].zTempRef[2] = (float)bestx;
        regions_vec[it].depth.push_back( (float)bestx );

        if (sum_costs > 0) {
            for(int delta = firstDisp; delta < lastDisp; delta++)  // for each disparity value
                probs[threadIdx][delta] /= sum_costs;  // cost/sum_cost = prob (pdf)
        }

        // SO SLOWWWWWWWWWWWWWWWWWWW!!!
        regions_vec[it].probs.push_back( probs[threadIdx] );
        regions_vec[it].ratioCost.push_back( ratioCost[threadIdx] );
        regions_vec[it].sum_costs.push_back( sum_costs );
    }
    timerTotalMatch.pause();
    timerMatch.printTime();
    timerTotalMatch.printTime();


    if (useMST) {
        vector< tuple<float,float,float,int,int> > r_vec;
        vector< pair<int,int> > e_vec;
        vector< vector<float> > cost_vec;
        unsigned char* disparity = new unsigned char[regions_vec.size()];
        FastVec<triang>* triangs_vec = mesh->getTriangsVec();

        for(int it_r=0; it_r<regions_vec.size(); it_r++) { // for all regions
            int it = ptrList[it_r];
            tuple<float,float,float,int,int> t;
            get<0>(t) = regions_vec[it].meanRGB[0];
            get<1>(t) = regions_vec[it].meanRGB[1];
            get<2>(t) = regions_vec[it].meanRGB[2];
            get<3>(t) = (regions_vec[it].y[0]+regions_vec[it].y[1]+regions_vec[it].y[2])/3;
            get<4>(t) = (regions_vec[it].x[0]+regions_vec[it].x[1]+regions_vec[it].x[2])/3;
            r_vec.push_back(t);
            cost_vec.push_back(regions_vec[it].probs[(int)regions_vec[it].probs.size()-1]);
            int count = 0;
            for( vector<float>::iterator it_c = cost_vec[cost_vec.size()-1].begin(); it_c != cost_vec[cost_vec.size()-1].end(); it_c++ ) {
                *it_c = 1.0f-*it_c;
                //if( count == (int)regions_vec[it].depth[regions_vec[it].depth.size()-1] )
                //    *it_c = 0.001;
                count++;
            }
            (*triangs_vec)[it].mark[0] = false;
            regions_vec[it].inverseIdx = it_r;
        }
        calcEdges( e_vec );
        // HOW DO THE EGES!?!??

        m_nlca.init(&r_vec,&e_vec,maxDisp,150.10,50.0);//initialization
        m_nlca.matching_cost( &cost_vec );//compute matching cost
        m_nlca.disparity(disparity,cost_vec);//compute disparity

        for(int it_r=0; it_r<regions_vec.size(); it_r++) { // for all regions
            int it = ptrList[it_r];
            regions_vec[it].z[0] = regions_vec[it].z[1] = regions_vec[it].z[2] = (int)disparity[it_r];
            regions_vec[it].zClosed[0] = regions_vec[it].zClosed[1] = regions_vec[it].zClosed[2] = (int)disparity[it_r];
            regions_vec[it].zRef[0] = regions_vec[it].zRef[1] = regions_vec[it].zRef[2] = (int)disparity[it_r];
            regions_vec[it].zTemp[0] = regions_vec[it].zTemp[1] = regions_vec[it].zTemp[2] = (int)disparity[it_r];
            regions_vec[it].zTempRef[0] = regions_vec[it].zTempRef[1] = regions_vec[it].zTempRef[2] = (int)disparity[it_r];
            regions_vec[it].depth[(int)regions_vec[it].depth.size()-1] = (int)disparity[it_r];
            regions_vec[it].probsAgg.push_back( cost_vec[it_r] );
        }
    } else {
        vector<float> probsAgg[MAX_NUM_THREADS];
        timerAgg.cont();
        // Agregation Step (testing with more then 1 iteration...)
        for(int aggIter = 0; aggIter < 1; aggIter++ ) {
            #pragma omp parallel for
            for(int it_r=0; it_r<regions_vec.size(); it_r++) {
                float sum_costs_agg;
                int bestx=0;
                int firstDisp, lastDisp;
                float max_cost = -99999;
                int threadIdx = (int)omp_get_thread_num();
                int it = ptrList[it_r];

                probsAgg[threadIdx].clear();

                //timerFindTriang.cont();
                num_neighbors[threadIdx] = mesh->triOp->find_triang_neighbors(it, list_neighbors, 64, threadIdx);
                //timerFindTriang.pause();

                raster->setTriangle(it, threadIdx);
                // getDispLimits(it, firstDisp, lastDisp, maxDisp, threadIdx);
                firstDisp = 0;
                lastDisp = maxDisp-1;

                for(int delta = firstDisp ; delta <= lastDisp; delta++) {  // for each disparity value
                    probsAgg[threadIdx].push_back( getAggValue(it, delta, threadIdx) );
                }

                //            timerUpdateProb.cont();
                sum_costs_agg = 0;
                for(int delta = firstDisp ; delta <= lastDisp; delta++) {
                    sum_costs_agg += probsAgg[threadIdx][delta];
                    if(probsAgg[threadIdx][delta] > max_cost) {
                        max_cost = probsAgg[threadIdx][delta];
                        bestx = delta;
                    }
                }
                //            timerUpdateProb.pause();

                int idx = regions_vec[it].depth.size()-1;
                regions_vec[it].depth[idx] = (float)bestx;
                regions_vec[it].z[0] = regions_vec[it].z[1] = regions_vec[it].z[2] = (float)bestx;
                regions_vec[it].zTemp[0] = regions_vec[it].zTemp[1] = regions_vec[it].zTemp[2] = (float)bestx;
                regions_vec[it].zRef[0] = regions_vec[it].zRef[1] = regions_vec[it].zRef[2] = (float)bestx;
                regions_vec[it].zTempRef[0] = regions_vec[it].zTempRef[1] = regions_vec[it].zTempRef[2] = (float)bestx;

                //            timerAlloc.cont();
                regions_vec[it].probsAgg.push_back( probsAgg[threadIdx] );
                regions_vec[it].sum_costs_agg.push_back( sum_costs_agg );
                //            timerAlloc.pause();
            }
        }
        // Copying aggregated value back to original positions
        #pragma omp parallel for
        for(int it_r=0; it_r<regions_vec.size(); it_r++) {
         int it = ptrList[it_r];
         int idx = regions_vec[it].probs.size()-1;
         for(int i=0; i<maxDisp; i++) {
             regions_vec[it].probsAgg[idx][i] = regions_vec[it].probsAgg[idx][i]/regions_vec[it].sum_costs_agg[idx];
         }
         //regions_vec[it].sum_costs[idx] = regions_vec[it].sum_costs_agg[idx];
        }
    }

    cv::Mat dm;
    char imName[1024];
    //getDisparityMap( 1.0, dm );
    //sprintf(imName,"dmAgg_%d.png",currTempPos);
    //cv::imwrite(imName,dm);



//    if( usingTempCost ) {
//        calcTempProbs();

//        for(int aggIter = 0; aggIter < 1; aggIter++ ) {
//            #pragma omp parallel for
//            for(int it_r=0; it_r<regions_vec.size(); it_r++) {
//                float sum_costs_agg;
//                int bestx=0;
//                int firstDisp, lastDisp;
//                float sum_costs = 0;
//                float max_cost = -99999;
//                int threadIdx = (int)omp_get_thread_num();
//                int it = ptrList[it_r];

//                probsAgg[threadIdx].clear();

//                num_neighbors[threadIdx] = mesh->triOp->find_triang_neighbors(it, list_neighbors, 64, threadIdx);

//                raster->setTriangle(it, threadIdx);
//                getDispLimits(it, firstDisp, lastDisp, maxDisp, threadIdx);
//                for(int delta = firstDisp ; delta <= lastDisp; delta++)   // for each disparity value
//                    probsAgg[threadIdx].push_back( getAggValue(it, delta, threadIdx, true) );


//                sum_costs_agg = 0;
//                for(int delta = firstDisp ; delta <= lastDisp; delta++) {
//                    sum_costs_agg += probsAgg[threadIdx][delta];
//                    if(probsAgg[threadIdx][delta] > max_cost) {
//                        max_cost = probsAgg[threadIdx][delta];
//                        bestx = delta;
//                    }
//                }

//                for(int delta = firstDisp ; delta <= lastDisp; delta++) {
//                    regions_vec[it].probsTime[regions_vec[it].probsTime.size()-1][delta] = probsAgg[threadIdx][delta]/sum_costs_agg;
//                }

//                int idx = regions_vec[it].depth.size()-1;

//                regions_vec[it].depth[idx] = (float)bestx;
//                regions_vec[it].z[0] = regions_vec[it].z[1] = regions_vec[it].z[2] = (float)bestx;
//                regions_vec[it].zTemp[0] = regions_vec[it].zTemp[1] = regions_vec[it].zTemp[2] = (float)bestx;
//                regions_vec[it].zRef[0] = regions_vec[it].zRef[1] = regions_vec[it].zRef[2] = (float)bestx;
//                regions_vec[it].zTempRef[0] = regions_vec[it].zTempRef[1] = regions_vec[it].zTempRef[2] = (float)bestx;

//                regions_vec[it].sum_costs_time.push_back( sum_costs_agg );
//            }
//        }
//        // Copying aggregated value back to original positions
//        #pragma omp parallel for
//        for(int it_r=0; it_r<regions_vec.size(); it_r++) {
//            int it = ptrList[it_r];
//            int idx = regions_vec[it].probs.size()-1;
//            for(int i=0; i<maxDisp; i++) {
//                regions_vec[it].probsTime[idx][i] = regions_vec[it].probsTime[idx][i]/regions_vec[it].sum_costs_time[idx];
//            }
//            //regions_vec[it].sum_costs[idx] = regions_vec[it].sum_costs_agg[idx];
//        }
//    }


    if( usingHMM ) {
        timerProbPath.cont();
        findBestProbPath();
        //kalmanDisparityCorners();
        timerProbPath.pause();
        timerProbPath.printTime();
    }

    //getDisparityMap( 1.0, dm );
    //sprintf(imName,"dmHMM_%d.png",currTempPos);
    //cv::imwrite(imName,dm);


//    kalmanDisparity();

//    for( FastVec<region>::iterator it = regions_vec.begin(); it != regions_vec.end(); it++ ) {
//        float mean=0;
//        int last = it->depth.size();
//        int first = last-15;
//        if( first<0 ) first = 0;

//        for(int i=first; i<last; i++)
//            mean += it->depth[i];

//        mean /= last-first;
//        it->depth[last-1] = mean;
//        it->z[0] = it->z[1] = it->z[2] = mean;
//        it->zRef[0] = it->zRef[1] = it->zRef[2] = mean;
//        it->zTemp[0] = it->zTemp[1] = it->zTemp[2] = mean;
//        it->zTempRef[0] = it->zTempRef[1] = it->zTempRef[2] = mean;
//    }

}

void DisparityEstimation::calcEdges( vector< pair<int,int> >& e_vec ) {
    int t, o, v;
    FastVec<triang>* triangs_vec = mesh->getTriangsVec();
    FastVec<edge>* edges_vec = mesh->getEdgesVec();
    FastVec<triang>::iterator it_t = triangs_vec->begin();
    queue<int> Q;
    pair<int,int> ePair = make_pair( 0, 0 );

    v = it_t->idx;

//3      enqueue v onto Q
    Q.push( v ); // endQ points to the next empty space in Q

//4      mark v LATER!!!
//    (*triangs_vec)[v].mark[0] = true;

//5      while Q is not empty:
    while( (int)Q.size() > 0 ) {
//6          t ← Q.dequeue()
        t = Q.front();
        Q.pop();
        // If I've already visit this triangle then all of their edges have been added!
        if( (*triangs_vec)[t].mark[0] == true )
            continue;
         (*triangs_vec)[t].mark[0] = true;

//9          for all edges e the triangle t
        for(int e=0; e<3; e++) {
            int eIdx = (*triangs_vec)[t].edges_vec[e];
            if( (*edges_vec)[eIdx].numTriangs < 2 ) // no neighbor triangle!
                continue;
//10             o ← neighbor triangle
            o = ( (*edges_vec)[eIdx].triangs_vec[0] != t ? (*edges_vec)[eIdx].triangs_vec[0] : (*edges_vec)[eIdx].triangs_vec[1] );
//11             if o is not marked:
            if( (*triangs_vec)[o].mark[0] == false ) {
//12                  mark o LATER!! ONLY AFTER POP!!
//13                  enqueue o onto Q
                //(*triangs_vec)[o].mark[0] = true;
                Q.push( o );
                // add this edge!!
                get<0>(ePair) = regions_vec[t].inverseIdx;
                get<1>(ePair) = regions_vec[o].inverseIdx;
                e_vec.push_back( ePair );
            }
        }
    }
}

float DisparityEstimation::getAggValue(int currTriang, int delta, int threadIdx, bool temp) {
    float weightSum = 0;
    float aggValue = 0;
    float res;

    //return regions_vec[currTriang].probs[regions_vec[currTriang].probs.size()-1][delta];

    for(int n=0; n<num_neighbors[threadIdx]; n++)  {
        int idx = regions_vec[list_neighbors[threadIdx][n]].probs.size()-1;

        if (regions_vec[list_neighbors[threadIdx][n]].probs[idx][delta] <= 0 )
            continue;
        float size_mult = (float)regions_vec[list_neighbors[threadIdx][n]].numPts;
        size_mult = exp(-(100.0f*100.0f)/(size_mult*size_mult));//expPreCalc[int(100/size_mult)*80];// exp(-100.0f/size_mult);  // WARNING! Set this as variables from config->getXXX() !!!!
        float dist_mult = 1;//mesh->distBetweenTriangs( currTriang, list_neighbors[threadIdx][n]);
        dist_mult = exp(-(dist_mult*dist_mult)/(20.0f*20.0f));//expPreCalc[int(dist_mult/30)*80];//exp(-dist_mult/30.0f);
        int currR = list_neighbors[threadIdx][n];

        res = sqrt( (regions_vec[currTriang].meanLab[0]-regions_vec[currR].meanLab[0]) * (regions_vec[currTriang].meanLab[0]-regions_vec[currR].meanLab[0]) +
                  (regions_vec[currTriang].meanLab[1]-regions_vec[currR].meanLab[1]) * (regions_vec[currTriang].meanLab[1]-regions_vec[currR].meanLab[1]) +
                  (regions_vec[currTriang].meanLab[2]-regions_vec[currR].meanLab[2]) * (regions_vec[currTriang].meanLab[2]-regions_vec[currR].meanLab[2])  );
//        res = sqrt( (regions_vec[currTriang].meanRGB[0]-regions_vec[currR].meanRGB[0]) * (regions_vec[currTriang].meanRGB[0]-regions_vec[currR].meanRGB[0]) +
//                    (regions_vec[currTriang].meanRGB[1]-regions_vec[currR].meanRGB[1]) * (regions_vec[currTriang].meanRGB[1]-regions_vec[currR].meanRGB[1]) +
//                    (regions_vec[currTriang].meanRGB[2]-regions_vec[currR].meanRGB[2]) * (regions_vec[currTriang].meanRGB[2]-regions_vec[currR].meanRGB[2])  );


        float colorWeight = exp(-(float)res/(5)) *  (float)regions_vec[list_neighbors[threadIdx][n]].numPts;// * size_mult * dist_mult;

        //float colorWeight = 0.5;//refine->weightOmega(currTriang, list_neighbors[threadIdx][n], true) * size_mult * dist_mult;
        weightSum += colorWeight;
        if( temp )  {
            //for( int i=0; i<num_neighbors[threadIdx]; i++)
            //    cout << threadIdx << "->" << list_neighbors[threadIdx][i] << endl;
            //cout.flush();
            aggValue += ( (regions_vec[list_neighbors[threadIdx][n]].probsTime[idx][delta]) * colorWeight);///(float(list_neighbors.size()+1));


        }
        if (regions_vec[list_neighbors[threadIdx][n]].probs[idx][delta] != regions_vec[list_neighbors[threadIdx][n]].probs[idx][delta]) std::cout << "Fuck prob"  << std::endl;
        else aggValue += ( (regions_vec[list_neighbors[threadIdx][n]].probs[idx][delta]) * colorWeight);///(float(list_neighbors.size()+1));
    }
    int idx = regions_vec[currTriang].probs.size()-1;
    float size_mult =  (float)regions_vec[currTriang].numPts;//exp(-(100.0f*100.0f)/ ( (float)regions_vec[currTriang].numPts*(float)regions_vec[currTriang].numPts ) );//regions_vec[currTriang].points_vec.size()/100.0f;
    if( temp ) {
        if (regions_vec[currTriang].probsTime[idx][delta] > 0)
            aggValue += size_mult * (regions_vec[currTriang].probsTime[idx][delta]);///(float(num_neighbors[threadIdx]+1));
    } else if ((regions_vec[currTriang].probs[idx][delta]) > 0)
        aggValue += size_mult * (regions_vec[currTriang].probs[idx][delta]);///(float(num_neighbors[threadIdx]+1));

    return aggValue;
}

void DisparityEstimation::getDisparityMap(float dispMult, cv::Mat& rasterImg, bool color, bool tempCohesion) {
    int* ptrList;
    raster->setRegions(&regions_vec);

    if( rasterImg.rows != H || rasterImg.cols != W )
        rasterImg = cv::Mat::zeros(H, W, CV_8UC1);

    ptrList = regions_vec.getPtrList();
    #pragma omp parallel for
    for(int it_r=0; it_r<regions_vec.size(); it_r++) { // for each region
        int left, right;
        float a, b, c, d;
        int threadIdx = (int)omp_get_thread_num();
        int i=ptrList[it_r];

        raster->setTriangle(i,threadIdx);

        if(tempCohesion) {
            a = ((float)regions_vec[i].y[1]-(float)regions_vec[i].y[0])*(regions_vec[i].zTempRef[2]-regions_vec[i].zTempRef[0]) -
                ((float)regions_vec[i].y[2]-(float)regions_vec[i].y[0])*(regions_vec[i].zTempRef[1]-regions_vec[i].zTempRef[0]);

            b = (regions_vec[i].zTempRef[1]-regions_vec[i].zTempRef[0])*((float)regions_vec[i].x[2]-(float)regions_vec[i].x[0])-
            (regions_vec[i].zTempRef[2]-regions_vec[i].zTempRef[0])*((float)regions_vec[i].x[1]-(float)regions_vec[i].x[0]);
        } else {
            a = ((float)regions_vec[i].y[1]-(float)regions_vec[i].y[0])*(regions_vec[i].zClosed[2]-regions_vec[i].zClosed[0]) -
                ((float)regions_vec[i].y[2]-(float)regions_vec[i].y[0])*(regions_vec[i].zClosed[1]-regions_vec[i].zClosed[0]);
                //((float)regions_vec[i].y[1]-(float)regions_vec[i].y[0])*(regions_vec[i].zRef[2]-regions_vec[i].zRef[0]) -
                //((float)regions_vec[i].y[2]-(float)regions_vec[i].y[0])*(regions_vec[i].zRef[1]-regions_vec[i].zRef[0]);

            b = (regions_vec[i].zClosed[1]-regions_vec[i].zClosed[0])*((float)regions_vec[i].x[2]-(float)regions_vec[i].x[0])-
                        (regions_vec[i].zClosed[2]-regions_vec[i].zClosed[0])*((float)regions_vec[i].x[1]-(float)regions_vec[i].x[0]);
            //b = (regions_vec[i].zRef[1]-regions_vec[i].zRef[0])*((float)regions_vec[i].x[2]-(float)regions_vec[i].x[0])-
            //(regions_vec[i].zRef[2]-regions_vec[i].zRef[0])*((float)regions_vec[i].x[1]-(float)regions_vec[i].x[0]);
        }

        c = float(((float)regions_vec[i].x[1]-(float)regions_vec[i].x[0])*((float)regions_vec[i].y[2]-(float)regions_vec[i].y[0])-
            ((float)regions_vec[i].x[2]-(float)regions_vec[i].x[0])*((float)regions_vec[i].y[1]-(float)regions_vec[i].y[0]));

        if(tempCohesion)	d = -(a*(float)regions_vec[i].x[0] + b*(float)regions_vec[i].y[0] + c*regions_vec[i].zTempRef[0]);
        else				d = -(a*(float)regions_vec[i].x[0] + b*(float)regions_vec[i].y[0] + c*regions_vec[i].zClosed[0]);//c*regions_vec[i].zRef[0]);

        for(int row=regions_vec[i].y[0]; row<=regions_vec[i].y[2]; row++) {
            raster->getHorizLimits(row, left, right,threadIdx);
            for(int col=left; col<=right; col++) {
                rasterImg.ptr<unsigned char>(row)[col] = uchar(dispMult*((-a*(float)col - b*(float)row - d)/c));
            }
        }
    }
}

void DisparityEstimation::getDispLimits(int it, int& firstDisp, int& lastDisp, int maxDisp, int threadIdx) {
    firstDisp = 0;
    lastDisp = maxDisp-1;

    return;
}

void DisparityEstimation::getSimRegions( vector<int>& simTriangs, vector<float>& simVal, int currTriang ) {
    mesh->getSimRegions( simTriangs, simVal, currTriang );
}

vector< tuple<float,int,int> >* DisparityEstimation::getWeightLinks() {
    return mesh->getWeightLinks();
}

void DisparityEstimation::findBestProbPath() {
    int* ptrList;
    ptrList = regions_vec.getPtrList();

    // for all triangles/regions
    //#pragma omp parallel for
    for(int it_r=0; it_r<regions_vec.size(); it_r++) { // for each region
        int threadIdx = 0;//(int)omp_get_thread_num();
        FastVec<region>::iterator it = regions_vec.getIt( ptrList[it_r] );
        // no temporal coherence possible if I only have 1 frame!!!
        //if( it->probs.size() < 2 ) {
        //    //it->probsTime.push_back( it->probs[(int)it->probs.size()-1] );
        //    continue;
        //}

        int firstProb = 0;//(int)it->probs.size()-tempWindowProb;
        if( firstProb < 0 ) firstProb = 0;
        int lastProb = (int)it->probs.size();
        int T = lastProb-firstProb;

        // init Costs from the first frame in the temporal window. Afterwards I will
        // update all the following Costs using the preceding ones. In the first iteration
        // I will need those ones.

        // if it is frist frame, init Costs
        if( T<2 ) {
            for( int d=0; d<maxDisp; d++)
                Costs[threadIdx][it->idx][0][d] = Costs[threadIdx][it->idx][1][d] = (double)it->probsAgg.at(firstProb).at(d)*100;
        }
        else {
            for( int t=T-1; t<T; t++ ) {
                for( int d=0; d<maxDisp; d++ ) {
                    double maxCost=-1;
                    for( int i=0; i<maxDisp; i++ ) {
                        double cost = (double)it->probsAgg.at(t+firstProb).at(d)*10 * Costs[threadIdx][it->idx][0][i] * Weights[abs(d-i)];
                        if( cost > maxCost ) {
                            maxCost = cost;
                            Costs[threadIdx][it->idx][1][d] = maxCost;//Costs[threadIdx][it->idx][0][d];//maxCost;
                            //Path[threadIdx][t][d] = i;
                        }
                    }
                }
            }
        }

        int initD = -1;
        double maxCost = -1;
        int maxIdx=-1;


        for( int d=0; d<maxDisp; d++ ) {
            if( Costs[threadIdx][it->idx][1][d] > maxCost ) {
                maxCost = Costs[threadIdx][it->idx][1][d];
                initD = d;
            }
        }

        if( initD < 0 ) {
            cout << "Problem! Negative disparity!?!?" << endl;
        }
        for( int d=0; d<maxDisp; d++ ) {
            Costs[threadIdx][it->idx][0][d] = Costs[threadIdx][it->idx][1][d]/maxCost;
            if( Costs[threadIdx][it->idx][0][d] < 10e-7 )
                Costs[threadIdx][it->idx][0][d] = 10e-7;
        }

        maxIdx = initD;

//        if( initD < 0 ) {
//            cout << "Terrible problem: initD < 0" << endl;
//            exit(1);
//        }

//        for( int t=0; t<T-1; t++ ) {
//            maxIdx = Path[threadIdx][t][initD];
//            initD = Path[threadIdx][t][initD];
//        }
//        if( maxIdx < 0 ) {
//            cout << "Terrible problem: maxIdx < 0" << endl;
//            exit(1);
//        }



        // This is known in pt-BR as gambiarra. Someday I will change this... perhaps...
        // but it DOES work!!!
        it->depth[ it->depth.size()-1 ] = maxIdx;
        it->z[0] = it->z[1] = it->z[2] = maxIdx;
        it->zTemp[0] = it->zTemp[1] = it->zTemp[2] = maxIdx;
        it->zClosed[0] = it->zClosed[1] = it->zClosed[2] = maxIdx;
        it->zRef[0] = it->zRef[1] = it->zRef[2] = maxIdx;
        it->zTempRef[0] = it->zTempRef[1] = it->zTempRef[2] = maxIdx;
    }

}
void DisparityEstimation::calcTempProbs() {
    float maxVal[1024];
    vector<float> probsVec;
    vector<float>::iterator itMax;

    for( FastVec<region>::iterator it = regions_vec.begin(); it != regions_vec.end(); it++ ) {
        probsVec.clear();
        for( int i=0; i<it->probs.size(); i++ ) {
            itMax = (std::max_element( it->probsAgg[i].begin(), it->probsAgg[i].end() ));
            maxVal[i] = *itMax;
            if( maxVal[i] < 0.00001 ) {
                cout << "Problema!!!! Maxval muito pequeno!!!" << endl;
                exit(1);
            }
        }
        for( int d = 0; d < it->probs[0].size(); d++ ) {
            float tmpVal = 1;
            int firstP = (int)it->probs.size()-1-8;
            if( firstP<0 ) firstP = 0;
            for( int i=firstP; i<it->probs.size(); i++ )
                tmpVal *= it->probsAgg[i][d]/maxVal[i];
            probsVec.push_back( tmpVal );
        }
        probsVec[ probsVec.size()-1 ] = 0.000001;
        float tmpMax = -1;
        float tmpSum = 0;
        int maxIdx;
        for( int i=0; i<probsVec.size(); i++) {
            tmpSum += probsVec[i];
            if( probsVec[i] > tmpMax ) {
                tmpMax = probsVec[i];
                maxIdx = i;
            }
        }

        for( int i=0; i<probsVec.size(); i++)
            probsVec[i] /= tmpSum;
        it->probsTime.push_back( probsVec );
        it->depth[ it->depth.size()-1 ] = maxIdx;
        it->z[0] = it->z[1] = it->z[2] = maxIdx;
        it->zTemp[0] = it->zTemp[1] = it->zTemp[2] = maxIdx;
        it->zRef[0] = it->zRef[1] = it->zRef[2] = maxIdx;
        it->zTempRef[0] = it->zTempRef[1] = it->zTempRef[2] = maxIdx;
    }
}

void DisparityEstimation::kalmanDisparity() {
    double H=1;
    double A=1;

    int* ptrList = regions_vec.getPtrList();

#pragma omp parallel for
    for(int it_r=0; it_r<regions_vec.size(); it_r++) { // for all regions
        int itIdx = ptrList[it_r];
        int idx = (int)regions_vec[itIdx].meanL.size()-1;
        int idxDepth = (int)regions_vec[itIdx].depth.size()-1;

        double R=10;
        double Q=20000;

        FastVec<region>::iterator it = regions_vec.getIt(itIdx);
        if( idx>0 ) {
            float colorSim = (it->meanL[idx]-it->meanL[idx-1]) * (it->meanL[idx]-it->meanL[idx-1]);
            colorSim += (it->meana[idx]-it->meana[idx-1]) * (it->meana[idx]-it->meana[idx-1]);
            colorSim += (it->meanb[idx]-it->meanb[idx-1]) * (it->meanb[idx]-it->meanb[idx-1]);
            colorSim += 1;
            Q = exp(-(50)/colorSim);
            R = exp( -it->probs.at(idx).at(it->depth[idx])/0.01 )*10;
            //if(Q>20000) Q = 20000;
            //if(Q<0.5) Q = 0.01;

            float z = it->depth[idxDepth];
            if(it->kalmanX<0) {
                it->kalmanX = (1.0/H) * z;
                it->kalmanP = (1.0/H) * R * (1.0/H);
            }
            it->kalmanX = A*it->kalmanX; // + s.B*s.u ; there is no control, so is zero
            it->kalmanP = A*it->kalmanP*A + Q;
            // Kalman gain factor
            double K = it->kalmanP * H * 1.0f/(H * it->kalmanP * H + R);
            // Correction based on observation
            it->kalmanX = it->kalmanX + K*(z-H*it->kalmanX);
            it->kalmanP = it->kalmanP - K*H*it->kalmanP;

            it->depth[idxDepth] = it->kalmanX;
            it->z[0] = it->z[1] = it->z[2] = it->kalmanX;
            it->zRef[0] = it->zRef[1] = it->zRef[2] = it->kalmanX;
            it->zTemp[0] = it->zTemp[1] = it->zTemp[2] = it->kalmanX;
            it->zTempRef[0] = it->zTempRef[1] = it->zTempRef[2] = it->kalmanX;
        }
    }

}

void DisparityEstimation::kalmanDisparityCorners() {
    double H=1;
    double A=1;

    int* ptrList = regions_vec.getPtrList();

//#pragma omp parallel for
    for(int it_r=0; it_r<regions_vec.size(); it_r++) { // for all regions
        int itIdx = ptrList[it_r];
        int idx = (int)regions_vec[itIdx].meanL.size()-1;
        int idxDepth = (int)regions_vec[itIdx].depth.size()-1;

        double R=10;
        double Q=20000;

        FastVec<region>::iterator it = regions_vec.getIt(itIdx);
        if( idx<=0 )  continue;
        for( int i=0; i<3; i++ ) {
            float colorSim = (it->meanL[idx]-it->meanL[idx-1]) * (it->meanL[idx]-it->meanL[idx-1]);
            colorSim += (it->meana[idx]-it->meana[idx-1]) * (it->meana[idx]-it->meana[idx-1]);
            colorSim += (it->meanb[idx]-it->meanb[idx-1]) * (it->meanb[idx]-it->meanb[idx-1]);
            colorSim += 1;
            Q = exp(-(100)/colorSim);
            R = exp( -it->probs.at(idx).at(it->depth[idx])/0.1 );
            //if(Q>20000) Q = 20000;
            //if(Q<0.5) Q = 0.01;

            float z = it->zRef[i] ;
            if(it->zKalmanX[i]<0) {
                it->zKalmanX[i] = (1.0/H) * z;
                it->zKalmanP[i] = (1.0/H) * R * (1.0/H);
            }

            it->zKalmanX[i] = A*it->zKalmanX[i]; // + s.B*s.u ; there is no control, so is zero
            it->zKalmanP[i] = A*it->zKalmanP[i]*A + Q;
            // Kalman gain factor
            double K = it->zKalmanP[i] * H * 1.0f/(H * it->zKalmanP[i] * H + R);
            // Correction based on observation
            it->zKalmanX[i] = it->zKalmanX[i] + K*(z-H*it->zKalmanX[i]);
            it->zKalmanP[i] = it->zKalmanP[i] - K*H*it->zKalmanP[i];

            it->zClosed[i] = it->zKalmanX[i];
            it->zRef[i] = it->zKalmanX[i];
            it->zTemp[i] = it->zKalmanX[i];
            it->zTempRef[i] = it->zKalmanX[i];
        }
    }


}

void DisparityEstimation::bilateralDisp() {
    vector<int> simTriangs;
    vector<float> simVal;
    float disp;
    float W;
    float dispIdx;
    float tempSigma = 5.0;
    float distSigma = 25.0;

    cout << "Using temp bilateral disp: " << tempBilateralDisp << endl;



    for( FastVec< region >::iterator it = regions_vec.begin(); it != regions_vec.end(); it++ ) {
        simTriangs.clear();
        simVal.clear();
        disp = 0;
        W = 0;
        getSimRegions( simTriangs, simVal, (int)it->idx );

        int count = 0;
        for( vector<int>::iterator it_t = simTriangs.begin(); it_t != simTriangs.end(); it_t++ ) {
            double sizeW = 1;//exp( -500/ (regions_vec[*it_t].initArea*regions_vec[*it_t].initArea) );
            double distTriangs = mathOp.distBetweenTriangs( mesh->getPtsVec(), (*mesh->getTriangsVec())[*it_t], (*mesh->getTriangsVec())[it->idx] );
            double distW = exp( -distTriangs/distSigma );
            double distTime;
            if( tempBilateralDisp ) distTime = exp( -tempSigma/( (*mesh->getTriangsVec())[*it_t].lastFrame - (*mesh->getTriangsVec())[*it_t].firstFrame + 1 )  );
            else distTime = 1;
            disp += regions_vec[*it_t].depth[ (int)regions_vec[*it_t].depth.size()-1 ] * simVal[ count ] * sizeW * distW * distTime ;
            W += simVal[ count++ ] * sizeW * distW * distTime;
        }
        dispIdx = (int)it->depth.size()-1;
        double sizeW = 1;//exp( -500/ (it->initArea*it->initArea) );
        double distTime;
        if( tempBilateralDisp ) distTime = exp( -tempSigma/( it->age+1 )  );
        else distTime = 1;
        disp += it->depth[ dispIdx ] * sizeW * distTime;
        disp /= W+1.0f*sizeW*distTime;

        if( disp < 0 ) disp = 0;
        if( disp >= maxDisp-1 ) disp = maxDisp-2;
        if( isnan(disp) ) disp = it->depth.at( dispIdx );

        //disp = 25;//it->depth[ dispIdx ];
        it->depth.at( dispIdx ) = disp;
        it->z[0] = it->z[1] = it->z[2] = disp;
        it->zRef[0] = it->zRef[1] = it->zRef[2] = disp;
        it->zClosed[0] = it->zClosed[1] = it->zClosed[2] = disp;
        //cout << dispIdx << " " << disp << " --- ";
    }
}

FastVec<region>* DisparityEstimation::getRegionsVec() {
	return &regions_vec;
}

FastVec<point_t>* DisparityEstimation::getPtsVec( ) {
	return mesh->getPtsVec();
}

cv::Mat* DisparityEstimation::getEdgesImg() {
	return mesh->getEdgesImg();
}

ImagesDaemon* DisparityEstimation::getImgDaemon() {
	return imgDaemon;
}

int DisparityEstimation::find_triang_from_pt( int y, int x ) {
	return mesh->find_triang_from_pt( y, x );
}

cv::Mat DisparityEstimation::getTriangulation() {
    //String triangulationName;
    //triangulationName = sbl::sprintF("triang_%d.png", imgDaemon->getCurrentFrame() );
    //cv::imwrite( triangulationName.c_str(), imOp.triangulationImg( imgDaemon->getCenterImg(), mesh->ptsForTriangulation() ) );

    //return imOp.triangulationImg( imgDaemon->getCenterImg(), mesh->ptsForTriangulation() );

    //cv::imshow( "Triangulation", imOp.triangulationImg( imgDaemon->getCenterImg(), mesh->ptsForTriangulation() ) );
	//String triangulationName;

	//cv::waitKey(1);
}
