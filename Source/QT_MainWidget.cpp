#include "QT_MainWidget.h"

QT_MainWidget::QT_MainWidget(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	dispEst = new DisparityEstimation;
	depthMapImg = cv::Mat::zeros(1,1,CV_8UC1);

	this->imgDaemon = dispEst->getImgDaemon();


	currentFrame = 0;

	qt_dispWidget = new QT_DisparityWidget;
	qt_dispWidget->show();

	qt_drawLabel = new QT_DrawLabel( 0 );
	qt_drawLabel->show();

	qt_edgePts = new QT_DrawLabel( 1 );
	qt_edgePts->show();

	//dmLabel = new QT_DrawLabel;
	//dmLabel->show();

    QString windowName("Main Window (Empty)");
	setWindowTitle(windowName);

	QString windowLabelName("Triangulation");
	qt_drawLabel->setWindowTitle(windowLabelName);

	QString windowEdgeName("Edge Points");
	qt_edgePts->setWindowTitle(windowEdgeName);

	//QString windowDmLabelName("Disparity Map");
	//dmLabel->setWindowTitle(windowDmLabelName);
	//
	QString windowDispName("Stereo Debug");
	qt_dispWidget->setWindowTitle(windowDispName);


	connect(qt_drawLabel, SIGNAL(mousePressed(QPoint)), this, SLOT(triangChanged(QPoint)));
	//connect(dmLabel, SIGNAL(mousePressed(QPoint)), this, SLOT(triangChanged(QPoint)));
}

QT_MainWidget::~QT_MainWidget()
{

}

void QT_MainWidget::mousePressEvent(QMouseEvent *evt) {
	cout << "pressed mouse!!" << endl;
	//for(int i=configs->getInitFrame(); i<configs->getInitFrame()+configs->getNumFrames(); i++)
	updateDMState();
}

void QT_MainWidget::updateDMState() {
    int lastFrame = config->readInt( "firstFrame" ) + config->readInt( "numFrames" );
    cv::Mat centerImg;
    vector<cv::Mat> sideImgs;

    while( currentFrame < lastFrame) {
        dispEst->calculateDisparityMap(currentFrame,true);
        currentFrame++;

        dispEst->getDisparityMap(config->readInt( "dmMult", 1 ),depthMapImg,false);
        cv::imwrite("DM.png",depthMapImg);

		
        regions_vec = dispEst->getRegionsVec();

        qt_drawLabel->setConfigs(config);
        qt_drawLabel->setRegionsVec(regions_vec);
        qt_drawLabel->setImg( qt_util.Mat2QImage3(*imgDaemon->getCenterImg()) );
        //qt_drawLabel->resize(imgDaemon->getCenterImg()->cols, imgDaemon->getCenterImg()->rows);

        qt_edgePts->setConfigs(config);
        qt_edgePts->setPtsVec(dispEst->getPtsVec());
        qt_edgePts->setImg(qt_util.Mat2QImage1(*dispEst->getEdgesImg()));
        qt_edgePts->resize(imgDaemon->getCenterImg()->cols, imgDaemon->getCenterImg()->rows);


        dispEst->getDisparityMap(config->readInt( "dmMult", 1 ),depthMapImg,false);
        //dmLabel->setImg(qt_util.Mat2QImage1(depthMapImg));
        //dmLabel->setConfigs(configs);
        cv::imwrite("DM.png",depthMapImg);


        centerImg = *imgDaemon->getCenterImg();//*configs->getImgCenterPtr();
        sideImgs = *imgDaemon->getSideImgs();//configs->getSideImgs();
        qt_dispWidget->setImgs(centerImg, sideImgs[0], sideImgs[imgDaemon->getNumCameras()-1]);
        int maxDisp = config->readInt( "maxDisp" );
        qt_dispWidget->setMaxDisparity(maxDisp);

        qt_drawLabel->repaint();
        qt_edgePts->repaint();
        qt_dispWidget->repaint();
    }


    ////char tempStr[512];
    ////vector<vector<int>> joinRegionsVec;
    ////QPoint p = mapFromGlobal(QCursor::pos());
    ////
    ////dispEst->calculateDisparityMap(currentFrame,true);
    ////
    ////triOp = dispEst->getTriOp();
    ////regions_vec = dispEst->getRegionsVec();

    //qt_drawLabel->setRegionsVec(regions_vec);
    //qt_drawLabel->setImg(qt_util.Mat2QImage3(*imgDaemon->getCenterImg()));
    //qt_drawLabel->resize(imgDaemon->getCenterImg()->cols, imgDaemon->getCenterImg()->rows);

    ////dispEst->getDisparityMap(configs->getDmMult(), depthMapImg, false, true);
    ////sprintf_s(tempStr,"dispEst_%d.png",configs->getCurrentFrame());
    ////cv::imwrite(tempStr, depthMapImg );

    ////dmLabel->setImg(qt_util.Mat2QImage1(depthMapImg));
    ////joinRegionsLabel->setImg(qt_util.Mat2QImage3(*(configs->getImgCenterPtr())));

    ////dispEst->getJointRegions(joinRegionsVec, 0);
    ////joinRegionsLabel->setTriang(joinRegionsVec);
    ////joinRegionsLabel->setRegionsVec(regions_vec);
    ////
    ////
    //cv::Mat centerImg = *imgDaemon->getCenterImg();
    //vector<cv::Mat> sideImgs = *imgDaemon->getSideImgs();//configs->getSideImgs();
    ////qt_dispWidget->setImgs(centerImg, sideImgs[0], sideImgs[imgDaemon->getNumCameras()-1]);
    //int maxDisp = config->readInt( "maxDisp" );
    ////qt_dispWidget->setMaxDisparity( maxDisp );

    ////dmLabel->repaint();
    //qt_drawLabel->repaint();
    ////joinRegionsLabel->repaint();
    ////qt_dispWidget->repaint();

    //currentFrame++;
}

void QT_MainWidget::triangChanged(QPoint qp) {
	vector<point> points;
	point paux;
    vector< vector<int> > joinRegionsVec;

	cout << "MOUSE PRESSED!!" << endl;

//	int currTriang = dispEst->find_triang_from_pt(qp.y(), qp.x());
	
//	for(int i=0; i<3; i++) {
//		paux.x = (float)(*regions_vec)[currTriang].x[i];
//		paux.y = (float)(*regions_vec)[currTriang].y[i];
//		paux.z = 0;
//		points.push_back(paux);
//	}

//	cout << "vertex: " << paux.y << ";" << paux.x << endl;

//	//dispEst->getJointRegions(joinRegionsVec, currTriang);
//	//joinRegionsLabel->setTriang(joinRegionsVec);

//	qt_dispWidget->setTriangs(points);
//	qt_dispWidget->setDisparity((int)(*regions_vec)[currTriang].depth);
//	qt_dispWidget->setAlphaVec((*regions_vec)[currTriang].probs);
	
//	qt_dispWidget->resize(0,0);
}

void QT_MainWidget::setConfigs(sbl::Config* config) {
	cv::Mat centerImg;
	vector<cv::Mat> sideImgs;
    vector< vector<int> > joinRegionsVec;
	bool refineMesh;

	// FIRST: Setting and Running the Disparity Estimation Algorithm
	currentFrame = config->readInt( "initialFrame" , 0 );
	refineMesh = config->readBool( "refineMesh", true );
	dispEst->setConfig(config);
	
	dispEst->calculateDisparityMap(currentFrame,true);
	currentFrame++;
	
	// Now I can do the rest....
	this->config = config;
	//triOp = dispEst->getTriOp();
	regions_vec = dispEst->getRegionsVec();

	qt_drawLabel->setConfigs(config);
	qt_drawLabel->setRegionsVec(regions_vec);
    //qt_drawLabel->setImg(qt_util.Mat2QImage3(*imgDaemon->getCenterImg()));
	qt_drawLabel->resize(imgDaemon->getCenterImg()->cols, imgDaemon->getCenterImg()->rows);

	qt_edgePts->setConfigs(config);
	qt_edgePts->setPtsVec(dispEst->getPtsVec());
    //qt_edgePts->setImg(qt_util.Mat2QImage1(*dispEst->getEdgesImg()));
	qt_edgePts->resize(imgDaemon->getCenterImg()->cols, imgDaemon->getCenterImg()->rows);

	dispEst->getDisparityMap(config->readInt( "dmMult", 1 ),depthMapImg,false);
	//dmLabel->setImg(qt_util.Mat2QImage1(depthMapImg));
	//dmLabel->setConfigs(configs);
	cv::imwrite("DM.png",depthMapImg);


	centerImg = *imgDaemon->getCenterImg();//*configs->getImgCenterPtr();
	sideImgs = *imgDaemon->getSideImgs();//configs->getSideImgs();
	qt_dispWidget->setImgs(centerImg, sideImgs[0], sideImgs[imgDaemon->getNumCameras()-1]);
	int maxDisp = config->readInt( "maxDisp" );
	qt_dispWidget->setMaxDisparity(maxDisp);
}

QSize QT_MainWidget::minimumSizeHint() const
{
    return QSize(375/2, 450/2);
}


QSize QT_MainWidget::sizeHint() const
{
    return QSize(375/2, 450/2);
}


void QT_MainWidget::calcDispMap() {
	//dispEst->calculateDisparityMap(currentFrame,true);
}

void QT_MainWidget::run() {
	
}
