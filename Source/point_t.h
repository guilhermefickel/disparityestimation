#ifndef POINT_T_H
#define POINT_T_H

#include <cstdlib>
#include "Constants.h"

class point_t {
public:
	point_t* ptrFwd;
	point_t* ptrBack;
	int idx;
	int idxPos;
	int nonDiscPt; // if this is a point within a disparity discontinuity, what is the closest pt
				   // that is not in a disparity discontinuity.

	int particleIdx;

	int x, y;
	int oldX, oldY; // x and y from previous frame
	float z[2]; // z coordinate after refinement
	int edges_vec[MAX_EDGES_TO_POINT];
	int triangs_vec[MAX_TRIANGS_TO_POINT];
	int numEdges;  // used to control edges_vec list
	int numTriangs;  // used to control triangs_vec list
	// if 'isNew' os set to false this will count as an inexisting point, but
	// without the need to free or set all the memory. Same for
	// edge and triang.
	bool isNew;
	bool used;
	bool mark[MAX_NUM_THREADS];
	bool dispDisc;
	bool edge;
	int iter;

	point_t();
	~point_t();
	
    point_t& operator=(const point_t &rhs);
					
};

#endif
