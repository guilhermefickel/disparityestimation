#ifndef MESH_H
#define MESH_H

#include <sbl/core/Config.h>
#include "ScaleMap.h"
#include "Aux/TriangleOperations.h"
#include "Aux/MathOp.h"
#include "Aux/Raster.h"
#include "DataStructure/region.h"
#include "DataStructure/point_t.h"
#include "DataStructure/edge.h"
#include "DataStructure/triang.h"
#include "DataStructure/ortho_triang.h"
#include "DataStructure/FastVec.h"
#include "Aux/Constants.h"
#include "Aux/ImagesDaemon.h"
#include "Refinement.h"
#include "ParticleTrack.h"
#include "DelaunayInterface.h"


#include <tuple>
#include <fstream>
#include <ctime>


class Refinement;

class Mesh3D {
private:
	FastVec<point_t> points_vec;
	FastVec<edge> edges_vec;
	FastVec<triang> triangs_vec;
	FastVec<orthoTriang> orthoTriangs_vec;
	FastVec<region>* regions_vec;
    vector<int>* ptsToRM;
	ImagesDaemon* imgDaemon;
	ParticleTrack* particleTrack;
	bool firstRun;

	vector< tuple<float,int,int> > weightLinks; // need initialization
	
	sbl::Config* config;
	ScaleMap* scaleMap;
	MathOp mathOp;
	Raster* raster;
	Refinement* refinement;


	DelaunayInterface* delobject;
	Delaunay* dt;

    bool debugMode;

	// quick access for the index of points and triangles. Unecessary? Perhaps...
	int** ptsIdx;
	int** triangsIdx;
	
	int H, W; // height, width
	int maxDisp;
	void calcRegionRGB(cv::Mat* img, int it, float meanRGB[3], int& numPts);
    float symBtwTriangs(int t1, int t2 );
	void setWeightLink( int eidx, float val );
    void updateVertices(  );
    void updateTriangs( vector<point_t>* pts);
    void updateDataStructure( vector<point_t>* pvec = NULL );
    void findConstrainedTriangles();
    void findBadPts();
    bool isDeformed( FastVec<region>::iterator it_r );
    bool badTriangle( FastVec<region>::iterator it_r );


    // DEBUG
    void checkPtsIdx();

public:
	Mesh3D();
	~Mesh3D();

	TriangleOperations* triOp;

	// WARNING!!! Must set configs BEFORE use this class!!!!!!!!!
	void setConfig( sbl::Config* config, Raster* raster, FastVec<region>* regions_vec, ImagesDaemon* imgDaemon, ParticleTrack* particleTrack );
    void run( );
    void triangulate( );
	void refine( );
	void saveMesh3D( FastVec<region> &regions_vec );
    void calcWeightLinks( FastVec<region> &regions_vec, cv::Mat* occMask );

	void createRegionsVec( FastVec<region> &regions_vec );
	float distBetweenTriangs( int t1, int t2 );
	TriangleOperations* getTriOp( );
	vector<point> ptsForTriangulation( );
    void getSimRegions( vector<int>& simTriangs, vector<float>& simVal, int currTriang ) ;

	// Access information of the mesh to plot on the screen.
	FastVec<point_t>* getPtsVec();
	int getNumPts();
	FastVec<edge>* getEdgesVec();
	int getNumEdges();
	FastVec<triang>* getTriangsVec();
	int getNumTriangs();
	cv::Mat* getEdgesImg();
	vector< tuple<float,int,int> >* getWeightLinks();
	int find_triang_from_pt( int y, int x );

};


#endif
