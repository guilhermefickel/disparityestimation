#ifndef QT_DrawLabel_H
#define QT_DrawLabel_H

#include <QWidget>
#include <QGridLayout>
//#include <QtGui>
#include <QPen>
#include <QPainter>
#include <QLabel>
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <sbl/core/Config.h>
#include "FastVec.h"
#include "region.h"
#include "point_t.h"


using namespace std;

enum dispMode { TRIANG, EDGE_PT };

//! [0]
class QT_DrawLabel : public QLabel
{
    Q_OBJECT

public:
    QT_DrawLabel(int dispType, QWidget *parent = 0);

    QSize minimumSizeHint() const;
    QSize sizeHint() const;
	void setRegionsVec(FastVec<region>* regions_vec);
	void setPtsVec( FastVec<point_t>* pts_vec );
	void setConfigs(sbl::Config* config);
	void setImg(QImage& img);
	void setTriang(vector<point_t>& triang);

signals:
	void mousePressed(QPoint qp);

public slots:
  

protected:
    void paintEvent(QPaintEvent *event);

private:
	sbl::Config* config;
	FastVec<region>* regions_vec;
	FastVec<point_t>* pts_vec;
	QImage img;
	vector<point_t> triang;
	bool isInit;
	int frame;
	dispMode dispOption;

	void mousePressEvent(QMouseEvent *evt);
	void drawTriangulation( QPaintEvent* e );
	void drawEdgePts( QPaintEvent* e );
};
//! [0]

#endif
