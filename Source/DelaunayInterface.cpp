#include "DelaunayInterface.h"

DelaunayInterface::DelaunayInterface() {
    dt = new Delaunay();
}

DelaunayInterface::~DelaunayInterface() {

}


void DelaunayInterface::config( FastVec<point_t>* pts_vec, FastVec<edge>* edges_vec, FastVec<triang>* triangs_vec ) {
	this->pts_vec = pts_vec;
	this->edges_vec = edges_vec;
	this->triangs_vec = triangs_vec;
}

Delaunay* DelaunayInterface::updateTriang( FastVec<point_t>* pts_vec, vector<point_t>* pts, vector<int>* ptsToRm ) {
	Delaunay::Vertex_handle v;

    delete dt;
    dt = new Delaunay();

    // add old pts
    for( FastVec<point_t>::iterator it = pts_vec->begin(); it != pts_vec->end(); it++ ) {
        dt->insert( Point(it->x,it->y) );
    }

    for( FastVec<edge>::iterator it = edges_vec->begin(); it != edges_vec->end(); it++ ) {
        if( it->numTriangs!=2 || it->constrained==false ) continue;
        Point p1( (*pts_vec)[it->points_vec[0]].x, (*pts_vec)[it->points_vec[0]].y );
        Point p2( (*pts_vec)[it->points_vec[1]].x, (*pts_vec)[it->points_vec[1]].y );
        dt->insert_constraint( nearest_vertex( p1 ), nearest_vertex( p2 ) );
    }

    // and finally add new pts
    for( vector<point_t>::iterator it = pts->begin(); it != pts->end(); it++ ) {
        dt->insert( Point(it->x, it->y) );
	}

    //cout << "DT after Inserting: " << dt->number_of_vertices() << endl;
    //cout << "Pts vec: " << pts_vec->size() << endl;

    return dt;
}

Delaunay* DelaunayInterface::updateTriang( vector<point_t>* pts ) {
	Delaunay::Vertex_handle v;
	
	// and finally add new pts
	for( int i=0; i<pts->size(); i++ ) {
        dt->insert( Point((*pts)[ i ].x, (*pts)[ i ].y) );
	}

    return dt;
}

Delaunay::Vertex_handle DelaunayInterface::nearest_vertex( Point p ) {
    Delaunay::Vertex_handle v;
    Delaunay::Face_handle f;
    float minDist = 999999999999999;
    int idx = -1;

    f = dt->locate( p, f);

    if( f->is_valid() == false )
        return v;

    for( int i=0; i<3; i++ ) {
        v = f->vertex( f->ccw(i) );
        if( dt->is_infinite( v ) ) continue;
        if( (v->point().x()-p.x())*(v->point().x()-p.x()) + (v->point().y()-p.y())*(v->point().y()-p.y()) < minDist ) {
            minDist = (v->point().x()-p.x())*(v->point().x()-p.x()) + (v->point().y()-p.y())*(v->point().y()-p.y());
            idx = i;
        }
    }

    if( idx == -1 ) {
        std::cout << "Problem in finding nearest_vertex( Point p ) !!!!" << std::endl;
        return v;
    }

    v = f->vertex( f->ccw(idx) );

    return v;
}
