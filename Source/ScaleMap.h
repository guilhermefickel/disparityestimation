#ifndef SCALEMAP_H
#define SCALEMAP_H

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <vector>

#include "DataStructure/point_t.h"
#include "DataStructure/point.h"
#include "DataStructure/FastVec.h"
#include <iostream>
#include <ctime>

using namespace std;

typedef struct Pt {
    int y,x;
    bool edge;
}Pt;

class ScaleMap {
private:
	bool temporalRun; // only change some points/triangles
	int numAddedPts; // number of points added in temporal coherence.
	int edgePts;
	int borderPts;

	cv::Mat* img;
	cv::Mat imgEdges;
	cv::Mat pointsMap;
	cv::Mat partMask;

	cv::Mat scaleMap;
	cv::Mat scaleImgs[6];
	// That epic sax guy! cannyG
	cv::Mat cannyR, cannyG, cannyB;
    int H,W;

	vector<point_t> pvec;
	int minDistFromEdge;
    int edgePtRadius;

    Pt directions[8];
    int currFrame;

	void colorCanny();
	void findPts();
    void updatePts( FastVec<point_t>* pts_vec );
	void drawCircleFilled(int x, int y, int radius, int v );
	void createScaleMap();
	float diffScalePts(int i, int j, uchar val1, uchar val2, uchar val3, uchar val1Scale, uchar val2Scale, uchar val3Scale);
	float distFromScale(int y, int x);
	void insertVertex( int y, int x, int radius, bool isEdge = false );

    // add edge pts functions
    void addEdgePts( bool addPt = true );
    bool findEdgePt( Pt &p );
    void markEdgePt( Pt p );
    void findNextPts( Pt p, vector<Pt>& nextPts );
    void startPath( Pt p );
    void addBufferPts( vector<Pt>& bufferPts );
    float maxError( vector<Pt>& bufferPts, int first, int last );

public:
	ScaleMap();
	~ScaleMap();

	void config(int minDistFromEdge);
	vector<point_t>* firstRun(cv::Mat* img);
    vector<point_t>* update( cv::Mat* img, FastVec<point_t>* pts_vec );
	vector<point_t>* getPts(int& edgePts, int& borderPts);
	cv::Mat* getEdgesImg();
	void runEdgeDetector(cv::Mat* img);
};

#endif
