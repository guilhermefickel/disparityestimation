#ifndef PARTICLETRACK_H
#define PARTICLETRACK_H


#include <sbl/core/Config.h>
#include <opencv2/core.hpp>
#include <opencv2/cudaoptflow.hpp>
#include <opencv2/core/utility.hpp>

#include <opencv2/highgui.hpp>
#include "Aux/ImagesDaemon.h"
#include "DataStructure/particle.h"

#include <iostream>
#include <tuple>

#include "DataStructure/FastVec.h"
#include "DataStructure/point_t.h"
#include "DataStructure/point.h"
#include "DataStructure/edge.h"
#include <math.h>


using namespace std;



class ParticleTrack {
private:
    sbl::Config spConf;
    sbl::Config* config;
    FastVec<particle> particle_vec;
    vector<int> ptsToRM;
    int** particlesIdx;
    cv::Mat* im;
    cv::Mat* edgesImg;
    cv::Mat* distTransform;
    cv::Mat occMask;
    cv::Mat flowx, flowy;
    cv::Mat bflowx, bflowy;
    cv::Mat bw;

    int H, W;

    ImagesDaemon* imgDaemon;

    void opticalFlow( cv::Mat frame0, cv::Mat frame1, cv::Mat &flowx, cv::Mat &flowy );
    void moveParticle( FastVec<point_t>::iterator it_p, float dx, float dy );
    void getClosestEdge( int x, int y, int &dx, int &dy );
    void calcOccMask( cv::Mat frame1, cv::Mat frame2, int frameIndex );
    void calcOccMask( int frameIndex );
    void rmCloseParticles( FastVec< point_t >* pts_vec, FastVec< edge >* edges_vec );

    void checkPtsIdx() ;

public:
    ParticleTrack();
    ~ParticleTrack();

    void setConfig( sbl::Config* config, ImagesDaemon* imgDaemon );
    void init( );
    void addParticles( vector<point_t>* pts_vec );
    vector<int> *getPtsToRM();
    void updateParticles( int frameIndex, FastVec<point_t>* pts_vec, FastVec<edge>* edges_vec, cv::Mat* edgesImg, vector< tuple<float,int,int> >* weightLinks );
    void updateVertices( FastVec<point_t>* points_vec, int** ptsIdx );
    int numParticles();
    int** getParticlesIdx();
    cv::Mat* getOccMask();

};



#endif
