#ifndef REGION_H
#define REGION_H

#include <vector>

class region {
public:
	region* ptrFwd;
	region* ptrBack;
	int idx;
	int idxPos;

	int x[3];
	int y[3];
	float z[3];
	float zRef[3]; // z after refinement
	float zClosed[3]; // z after closing the mesh
	float zTemp[3]; // 
	float zTempRef[3]; // 
	int ptIdx[3];
	int numPts; // number os pts, i.e. its area

	float meanRGB[3]; // mean value for each channel, used in refinement step
	bool used; // was deleted?
	bool stable; // is temporally stable?

	float depth;
	std::vector<float> probs;
	std::vector<float> probsAgg;
	float sum_costs;
	float sum_costs_agg;
	std::vector<float> ratioCost;  // confidence in the match, expressed by the ratio(gain) of the pixels values

	region();
	~region();

    region& operator=(const region &rhs);
} ;

#endif
