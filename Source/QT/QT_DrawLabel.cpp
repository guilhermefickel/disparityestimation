#include "QT_DrawLabel.h"


QT_DrawLabel::QT_DrawLabel(int dispType, QWidget *parent)
    : QLabel(parent)
{
    isInit = false;
	regions_vec=NULL;
	pts_vec=NULL;
    weightLinks = NULL;
    frame=1;
	switch( dispType ) {
		case 0: dispOption = TRIANG;  break;
		case 1: dispOption = EDGE_PT;  break;
        case 2: dispOption = TRIANGS_AGE;  break;
        case 3: dispOption = TRIANG_SIM; break;
    }
}



QSize QT_DrawLabel::minimumSizeHint() const
{
    return QSize(10, 10);
}


QSize QT_DrawLabel::sizeHint() const
{
    return QSize(40, 40);
}


void QT_DrawLabel::setTriang(vector<point_t>& triang) {
	this->triang = triang;

	update();
}

void QT_DrawLabel::setTriangSim( vector<int> simTriangs, vector<float> simVal ) {
    this->simTriangs = simTriangs;
    this->simVal = simVal;

    isInit = true;

    update();
}

void QT_DrawLabel::setWeightLinks( vector< tuple<float,int,int> >* weightLinks ) {
    this->weightLinks = weightLinks;
}

void QT_DrawLabel::setRegionsVec(FastVec<region>* regions_vec) {
	this->regions_vec = regions_vec;
}

void QT_DrawLabel::setPtsVec( FastVec<point_t>* pts_vec ) {
	this->pts_vec = pts_vec;
}

void QT_DrawLabel::setConfigs(sbl::Config* config) {
	this->config = config;
}

void QT_DrawLabel::setImg(QImage img) {
	this->img = img;
	isInit = true;
	resize(img.width(), img.height());
}

void QT_DrawLabel::mousePressEvent(QMouseEvent *evt) {
	QPoint p = mapFromGlobal(QCursor::pos());
	emit mousePressed(p);
}

void QT_DrawLabel::drawEdgePts( QPaintEvent* e ) {
	char tempStr[512];
	QPixmap imgWidget(img.width(), img.height());
	QLabel::paintEvent(e);

	QPen pen(Qt::red, 5, Qt::SolidLine);
	QPen penBlue(Qt::blue, 1, Qt::SolidLine);
	QPainter painterScreen(this);
	QPainter painter(&imgWidget);

	painter.setPen(pen);
    painter.setRenderHint(QPainter::Antialiasing, true);
	QPoint qpoint(0,0);
	painter.drawImage(qpoint,img);

	painterScreen.setPen(pen);
    painterScreen.setRenderHint(QPainter::Antialiasing, true);
	painterScreen.drawImage(qpoint,img);
	painterScreen.setFont(QFont("Arial", 6));


	QPoint point, point2;
	QLine line;
    if(pts_vec==NULL)
        return;

	for(FastVec<point_t>::iterator it=pts_vec->begin(); it!=pts_vec->end(); it++) {
        //if( it->idx == it->nonDiscPt )  continue;
        //if( it->dispDisc == false )
        //if( it->bad == false )
        //    continue;
		point.setY( it->y );  point.setX( it->x );
		point2.setY( (*pts_vec)[ it->nonDiscPt ].y ); point2.setX( (*pts_vec)[ it->nonDiscPt ].x );

		line.setP1( point );
		line.setP2( point2 );

		painterScreen.setPen( pen );
        if( it->edge ) painterScreen.drawPoint( point );
        if( it->dispDisc ) {
            painterScreen.setPen( penBlue );
            painterScreen.drawLine( line );
        }
		painter.setPen( pen );
        if( it->edge ) painter.drawPoint( point );
        if( it->dispDisc ) {
            painter.setPen( penBlue );
            painter.drawLine( line );
        }
		//painter.drawPolyline(triangle);
		//painterScreen.drawPolyline(triangle);
	}

//    if ( weightLinks != NULL ) {
//        cout << "About to draw " << weightLinks->size() << " lines..." << endl;
//        for( vector< tuple<float,int,int> >::iterator it = weightLinks->begin(); it != weightLinks->end(); it++ ) {
//            if( get<0>(*it) < 1 )
//                continue;

//            point.setY( (*pts_vec)[ get<1>(*it) ].y );   point.setX( (*pts_vec)[ get<1>(*it) ].x );
//            point2.setY( (*pts_vec)[ get<2>(*it) ].y );  point2.setX( (*pts_vec)[ get<2>(*it) ].x );

//            line.setP1( point );
//            line.setP2( point2 );

//            painterScreen.setPen( penBlue );
//            painterScreen.drawLine( line );
//            painter.setPen( penBlue );
//            painter.drawLine( line );

//        }
//    }

    cout << "###################Updating edge_pts!!" << endl;

	sprintf(tempStr, "edge_pts_%d.png", ++frame);
	QString fileName(tr(tempStr));
	imgWidget.save(fileName);
}

void QT_DrawLabel::drawTriangulation( QPaintEvent* e ) {
	char tempStr[512];
	QPixmap imgWidget(img.width(), img.height());
	QLabel::paintEvent(e);


	QPen pen(Qt::red, 1, Qt::SolidLine);
	QPainter painterScreen(this);
	QPainter painter(&imgWidget);

	painter.setPen(pen);
    painter.setRenderHint(QPainter::Antialiasing, true);
	QPoint qpoint(0,0);
	painter.drawImage(qpoint,img);

	painterScreen.setPen(pen);
    painterScreen.setRenderHint(QPainter::Antialiasing, true);
	painterScreen.drawImage(qpoint,img);
	painterScreen.setFont(QFont("Arial", 6));


	QPolygon triangle;
	if(regions_vec==NULL)
		return;
	for(FastVec<region>::iterator it=regions_vec->begin(); it!=regions_vec->end(); it++) {
		triangle.clear();
		triangle << QPoint(it->x[0], it->y[0]);
		triangle << QPoint(it->x[1], it->y[1]);
		triangle << QPoint(it->x[2], it->y[2]);
		triangle << QPoint(it->x[0], it->y[0]);
		painter.drawPolyline(triangle);
		painterScreen.drawPolyline(triangle);
	}

	//painterScreen.drawText( QRect( QPoint( 10, 10 ), QPoint( 20,20 ) ), Qt::AlignCenter, QString( "[255]" ) );

	// FIX!!!
	if(regions_vec->size() > 0) {
		sprintf(tempStr, "triangulation_%d.png", ++frame);
		QString fileName(tr(tempStr));
		imgWidget.save(fileName);
	}
}

void QT_DrawLabel::drawTriangsAge( QPaintEvent* e ) {
    char tempStr[512];
    QPixmap imgWidget(img.width(), img.height());
    QLabel::paintEvent(e);


    QPen pen(Qt::red, 1, Qt::SolidLine);
    QPainter painterScreen(this);
    QPainter painter(&imgWidget);
    QBrush brush( QColor(100,0,0) );

    painter.setPen(pen);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setBrush( brush );
    QPoint qpoint(0,0);
    //painter.drawImage(qpoint,img);

    painterScreen.setPen(pen);
    painterScreen.setRenderHint(QPainter::Antialiasing, true);
    //painterScreen.drawImage(qpoint,img);
    painterScreen.setFont(QFont("Arial", 6));
    painterScreen.setBrush( brush );


    QPointF triangle[3];
    if(regions_vec==NULL) {
        cout << "Regions vec == NULL -> drawAge" << endl;
        return;
    }
    for(FastVec<region>::iterator it=regions_vec->begin(); it!=regions_vec->end(); it++) {
        triangle[0].setX( it->x[0] );
        triangle[0].setY( it->y[0] );
        triangle[1].setX( it->x[1] );
        triangle[1].setY( it->y[1] );
        triangle[2].setX( it->x[2] );
        triangle[2].setY( it->y[2] );
        //triangle << QPoint(it->x[0], it->y[0]);
        //triangle << QPoint(it->x[1], it->y[1]);
        //triangle << QPoint(it->x[2], it->y[2]);
        //triangle << QPoint(it->x[0], it->y[0]);
        if( it->age*15 > 255  )
            brush.setColor( QColor( 0, 0, 255 ) );
        else
            brush.setColor( QColor( 0, 0, it->age*15 ) );

        painter.setBrush( brush );
        painterScreen.setBrush( brush );
        painter.drawConvexPolygon(triangle,3);
        painterScreen.drawConvexPolygon(triangle,3);
    }

    //painterScreen.drawText( QRect( QPoint( 10, 10 ), QPoint( 20,20 ) ), Qt::AlignCenter, QString( "[255]" ) );

    // FIX!!!
    //if(regions_vec->size() > 0) {
        sprintf(tempStr, "triangs_age_%d.png", ++frame);
        QString fileName(tr(tempStr));
        imgWidget.save(fileName);
    //}
}

void QT_DrawLabel::drawTriangSim( QPaintEvent* e ) {
    char tempStr[512];
    QPixmap imgWidget(img.width(), img.height());
    QLabel::paintEvent(e);


    QPen pen(Qt::red, 1, Qt::SolidLine);
    QPainter painterScreen(this);
    QBrush brush( QColor(100,0,0) );

    QPoint qpoint(0,0);
    //painter.drawImage(qpoint,img);

    painterScreen.setPen(pen);
    painterScreen.setRenderHint(QPainter::Antialiasing, true);
    //painterScreen.drawImage(qpoint,img);
    painterScreen.setFont(QFont("Arial", 6));
    painterScreen.setBrush( brush );


    QPointF triangle[3];
    if(regions_vec==NULL)
        return;

    int count=0;
    for( vector<int>::iterator it_sim = simTriangs.begin(); it_sim != simTriangs.end(); it_sim++ ) {
        FastVec<region>::iterator it = regions_vec->getIt( *it_sim );
        triangle[0].setX( it->x[0] );
        triangle[0].setY( it->y[0] );
        triangle[1].setX( it->x[1] );
        triangle[1].setY( it->y[1] );
        triangle[2].setX( it->x[2] );
        triangle[2].setY( it->y[2] );

        brush.setColor( QColor( 0, simVal[count]*255, 0 ) );//simVal[count]*255, simVal[count]*255 ) );

        //cout << "Sim: " << simVal[count] << endl;

        painterScreen.setBrush( brush );
        painterScreen.drawConvexPolygon(triangle,3);

        count++;
    }

//    for(FastVec<region>::iterator it=regions_vec->begin(); it!=regions_vec->end(); it++) {
//        triangle[0].setX( it->x[0] );
//        triangle[0].setY( it->y[0] );
//        triangle[1].setX( it->x[1] );
//        triangle[1].setY( it->y[1] );
//        triangle[2].setX( it->x[2] );
//        triangle[2].setY( it->y[2] );
//        if( it->age*15 > 255  )
//            brush.setColor( QColor( 0, 0, 255 ) );
//        else
//            brush.setColor( QColor( 0, 0, it->age*15 ) );

//        painter.setBrush( brush );
//        painterScreen.setBrush( brush );
//        painter.drawConvexPolygon(triangle,3);
//        painterScreen.drawConvexPolygon(triangle,3);
//    }

    //painterScreen.drawText( QRect( QPoint( 10, 10 ), QPoint( 20,20 ) ), Qt::AlignCenter, QString( "[255]" ) );

    // FIX!!!
//    if(regions_vec->size() > 0) {
//        sprintf(tempStr, "triangs_sim_%d.png", ++frame);
//        QString fileName(tr(tempStr));
//        imgWidget.save(fileName);
//    }
}


void QT_DrawLabel::paintEvent(QPaintEvent* e)
{
    if(isInit == false)
		return;

	switch ( dispOption )
	{
		case TRIANG: drawTriangulation( e );  break;
		case EDGE_PT: drawEdgePts( e );  break;
        case TRIANGS_AGE: drawTriangsAge( e ); break;
        case TRIANG_SIM: drawTriangSim( e ); break;
		//default: return;
	}
}
