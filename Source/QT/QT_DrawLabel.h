#ifndef QT_DrawLabel_H
#define QT_DrawLabel_H

#include <QWidget>
#include <QGridLayout>
//#include <QtGui>
#include <QPen>
#include <QPainter>
#include <QLabel>
#include <QBrush>
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <sbl/core/Config.h>
#include "../DataStructure/FastVec.h"
#include "../DataStructure/region.h"
#include "../DataStructure/point_t.h"


using namespace std;

enum dispMode { TRIANG, EDGE_PT, TRIANGS_AGE, TRIANG_SIM };

//! [0]
class QT_DrawLabel : public QLabel
{
    Q_OBJECT

public:
    QT_DrawLabel(int dispType, QWidget *parent = 0);

    QSize minimumSizeHint() const;
    QSize sizeHint() const;
	void setRegionsVec(FastVec<region>* regions_vec);
	void setPtsVec( FastVec<point_t>* pts_vec );
	void setConfigs(sbl::Config* config);
    void setImg(QImage img);
	void setTriang(vector<point_t>& triang);
    void setTriangSim( vector<int> simTriangs, vector<float> simVal );
    void setWeightLinks( vector< tuple<float,int,int> >* weightLinks );


signals:
	void mousePressed(QPoint qp);

public slots:
  

protected:
    void paintEvent(QPaintEvent *event);

private:
	sbl::Config* config;
	FastVec<region>* regions_vec;
	FastVec<point_t>* pts_vec;
    vector< tuple<float,int,int> >* weightLinks; // need initialization
	QImage img;
	vector<point_t> triang;
    vector<int> simTriangs;
    vector<float> simVal;
	bool isInit;
	int frame;
    dispMode dispOption;

	void mousePressEvent(QMouseEvent *evt);
	void drawTriangulation( QPaintEvent* e );
	void drawEdgePts( QPaintEvent* e );
    void drawTriangsAge( QPaintEvent* e );
    void drawTriangSim( QPaintEvent* e );
};
//! [0]

#endif
