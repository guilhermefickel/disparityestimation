#ifndef QT_DisparityWidget_H
#define QT_DisparityWidget_H

#include <QWidget>
#include <QGridLayout>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QLCDNumber>
#include "QT_DrawLabel.h"
#include <QGroupBox>
#include <QPushButton>
#include <QSlider>
#include <iostream>
#include <vector>
//#include "DataType.h"
#include "../DataStructure/point.h"
#include "../Aux/QT_Util.h"
#include <opencv2/highgui.hpp>
#include "../Aux/qcustomplot.h"

using namespace std;

//! [0]
class QT_DisparityWidget : public QWidget
{
    Q_OBJECT

public:
    QT_DisparityWidget(QWidget *parent = 0);

    QSize minimumSizeHint() const;
    QSize sizeHint() const;
	void setTriangs(vector<point>& triangs);
	void setImgs(cv::Mat imgCenter, cv::Mat imgLeft, cv::Mat imgRight);
    void setAlphaVec(vector<float> alphaVec, vector<float> nonAggVec);
    void setProbsVec( vector< vector<float> >* probsVec, vector< vector<float> >* nonAggProbsVec );
	void setDisparity(int disp);
	void setMaxDisparity(int disp);

public slots:
	void dispChanged(int disp);
    void probFrameChanged( int probFrame );

signals:
     void disparityChanged(int newValue);

protected:
    void paintEvent(QPaintEvent *event);

private:
	QT_Util qt_util;

	QGraphicsView* viewCenter;
	QGraphicsView* viewLeft;
	QGraphicsView* viewRight;
	QGraphicsScene sceneCenter;
	QGraphicsScene sceneLeft;
	QGraphicsScene sceneRight;
	QGroupBox *horizontalGroupBox;
	QGroupBox *horizontalGroupBox2;
	QPushButton *buttons[3];
	QPixmap* pixmapCenter;
	QPixmap* pixmapLeft;
	QPixmap* pixmapRight;
	QSlider* sliderDisp;
    QSlider* sliderProbFrame;
	QLCDNumber* lcdNumberDisp;
	QLCDNumber* lcdNumberVarDisp;
    QLCDNumber* lcdNumberProbFrame;
	QCustomPlot* customPlot;
	int xmin, xmax, ymin, ymax;
	int maxDisparity;
	float zoomFactor;
	vector<float> alphaVec;
    vector< vector<float> >* probsVec;
    vector< vector<float> >* nonAggProbsVec;
    int probFrame;

	bool isInit; // is init? Is safe to draw?

	vector<point> triangs;
	int disparity;
	cv::Mat centerImg, leftImg, rightImg;

	void createHorizontalGroupBox();
	void createHorizontalGroupBox2();
	void changeCenterImg();
	void changeLeftImg();
	void changeRightImg();
	void getMaxMin();
};
//! [0]

#endif
