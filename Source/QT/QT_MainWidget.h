#ifndef QT_MAINWIDGET_H
#define QT_MAINWIDGET_H

#include <QtGui/QMainWindow>
#include <QVBoxLayout>
#include <QPushButton>
#include <sbl/core/Config.h>
#include <opencv2/opencv.hpp>
#include "../DisparityEstimation.h"
#include "../Aux/ImagesDaemon.h"
#include "../Aux/QT_Util.h"
#include "../Aux/MathOp.h"
#include "QT_DisparityWidget.h"
#include "QT_DrawLabel.h"


class QT_MainWidget : public QMainWindow
{
	Q_OBJECT
	QVBoxLayout* boxLayout;
	QPushButton* buttonFwd;


	sbl::Config* config;
	ImagesDaemon* imgDaemon;
    MathOp mathOp;
	DisparityEstimation* dispEst;
	QT_DisparityWidget* qt_dispWidget;
	QT_DrawLabel* qt_drawLabel;
	QT_DrawLabel* qt_edgePts;
    QT_DrawLabel* qt_triangsAge;
    QT_DrawLabel* qt_triangSim;
	QT_Util qt_util;

    bool batchMode;

	cv::Mat depthMapImg;
	FastVec<region>* regions_vec;
	int currentFrame;

public slots:
	void triangChanged(QPoint qp);

signals:
	void mousePressed(QPoint qp);

public:
	QT_MainWidget(QWidget *parent = 0, Qt::WFlags flags = 0);
	~QT_MainWidget();

	void run();
	void setConfigs(sbl::Config* configs);
	void updateDMState();
	
	QSize minimumSizeHint() const;
    QSize sizeHint() const;

private:
	void calcDispMap();
	void mousePressEvent(QMouseEvent *evt);

};

#endif
