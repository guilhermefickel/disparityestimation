#include "QT_MainWidget.h"

QT_MainWidget::QT_MainWidget(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	dispEst = new DisparityEstimation;
	depthMapImg = cv::Mat::zeros(1,1,CV_8UC1);

	this->imgDaemon = dispEst->getImgDaemon();


	currentFrame = 0;

	qt_dispWidget = new QT_DisparityWidget;
	qt_dispWidget->show();

	qt_drawLabel = new QT_DrawLabel( 0 );
	qt_drawLabel->show();

	qt_edgePts = new QT_DrawLabel( 1 );
	qt_edgePts->show();

    qt_triangsAge = new QT_DrawLabel( 2 );
    qt_triangsAge->show();

    qt_triangSim = new QT_DrawLabel( 3 );
    qt_triangSim->show();


	//dmLabel = new QT_DrawLabel;
	//dmLabel->show();

    QString windowName("Main Window (Empty)");
	setWindowTitle(windowName);

	QString windowLabelName("Triangulation");
	qt_drawLabel->setWindowTitle(windowLabelName);

	QString windowEdgeName("Edge Points");
	qt_edgePts->setWindowTitle(windowEdgeName);

    QString windowTriangName("Triangs Age");
    qt_triangsAge->setWindowTitle(windowTriangName);

    QString windowSimName("Triangs Sim");
    qt_triangSim->setWindowTitle(windowSimName);

	//QString windowDmLabelName("Disparity Map");
	//dmLabel->setWindowTitle(windowDmLabelName);
	//
	QString windowDispName("Stereo Debug");
	qt_dispWidget->setWindowTitle(windowDispName);


	connect(qt_drawLabel, SIGNAL(mousePressed(QPoint)), this, SLOT(triangChanged(QPoint)));
	//connect(dmLabel, SIGNAL(mousePressed(QPoint)), this, SLOT(triangChanged(QPoint)));
}

QT_MainWidget::~QT_MainWidget()
{

}

void QT_MainWidget::mousePressEvent(QMouseEvent *evt) {
	cout << "pressed mouse!!" << endl;
	//for(int i=configs->getInitFrame(); i<configs->getInitFrame()+configs->getNumFrames(); i++)
	updateDMState();
}

void QT_MainWidget::updateDMState() {
    int lastFrame = config->readInt( "firstFrame" ) + config->readInt( "numFrames" );
    cv::Mat centerImg;
    vector<cv::Mat> sideImgs;
    char imName[1024];

    cout << currentFrame << " ;;; " << lastFrame << endl;

    do {
        dispEst->calculateDisparityMap(currentFrame,true);
        currentFrame++;

        dispEst->getDisparityMap(config->readFloat( "dmMult", 1 ),depthMapImg,false);
        cv::imwrite("DM.png",depthMapImg);

		
        regions_vec = dispEst->getRegionsVec();

        qt_drawLabel->setConfigs(config);
        qt_drawLabel->setRegionsVec(regions_vec);
        qt_drawLabel->setImg( qt_util.Mat2QImage3(*imgDaemon->getCenterImg()) );
        //qt_drawLabel->resize(imgDaemon->getCenterImg()->cols, imgDaemon->getCenterImg()->rows);

        qt_edgePts->setConfigs(config);
        qt_edgePts->setPtsVec(dispEst->getPtsVec());
        qt_edgePts->setWeightLinks( dispEst->getWeightLinks() );
        qt_edgePts->setImg(qt_util.Mat2QImage1(*dispEst->getEdgesImg()));
        qt_edgePts->resize(imgDaemon->getCenterImg()->cols, imgDaemon->getCenterImg()->rows);

        qt_triangsAge->setConfigs(config);
        qt_triangsAge->setRegionsVec(regions_vec);
        qt_triangsAge->setImg( qt_util.Mat2QImage3(*imgDaemon->getCenterImg()) );
        qt_triangsAge->resize(imgDaemon->getCenterImg()->cols, imgDaemon->getCenterImg()->rows);

        qt_triangSim->setConfigs(config);
        qt_triangSim->setRegionsVec(regions_vec);
        qt_triangSim->resize(imgDaemon->getCenterImg()->cols, imgDaemon->getCenterImg()->rows);

        dispEst->getDisparityMap(config->readFloat( "dmMult", 1 ),depthMapImg,false);
        //dmLabel->setImg(qt_util.Mat2QImage1(depthMapImg));
        //dmLabel->setConfigs(configs);
        sprintf(imName,"DM_%d.png",imgDaemon->getCurrentTime());
        cv::Mat cm_img0;
        double minVal, maxVal;
       // Apply the colormap:
        //cv::minMaxLoc( depthMapImg, &minVal, &maxVal );
        //depthMapImg = depthMapImg-minVal;
        //depthMapImg = depthMapImg * (255/maxVal);
        cv::applyColorMap(depthMapImg, cm_img0, cv::COLORMAP_RAINBOW);
        cv::imwrite(imName,cm_img0);


        centerImg = *imgDaemon->getCenterImg();//*configs->getImgCenterPtr();
        sideImgs = *imgDaemon->getSideImgs();//configs->getSideImgs();
        qt_dispWidget->setImgs(centerImg, sideImgs[0], sideImgs[imgDaemon->getNumCameras()-1]);
        int maxDisp = config->readInt( "maxDisp" );
        qt_dispWidget->setMaxDisparity(maxDisp);

        qt_drawLabel->repaint();
        qt_edgePts->repaint();
        qt_dispWidget->repaint();
        qt_triangsAge->repaint();
        qt_triangSim->repaint();
        //qt_triangsAge->update();

        cout << "Frame: " << currentFrame << " ";

    } while( currentFrame < lastFrame && batchMode );
    if( batchMode ) exit(0);
}

void QT_MainWidget::triangChanged(QPoint qp) {
	vector<point> points;
	point paux;
    vector< vector<int> > joinRegionsVec;
    vector<int> simTriangs;
    vector<float> simVal;

	cout << "MOUSE PRESSED!!" << endl;

    int currTriang = dispEst->find_triang_from_pt(qp.y(), qp.x());
	
    for(int i=0; i<3; i++) {
        paux.x = (float)(*regions_vec)[currTriang].x[i];
        paux.y = (float)(*regions_vec)[currTriang].y[i];
        cout << "["<<paux.y<<","<<paux.x<<"] ";
        paux.z = 0;
        points.push_back(paux);
    }

    cout << "vertex: " << paux.y << ";" << paux.x << endl;
    cout << "Area: " << mathOp.area( (*regions_vec).getIt( currTriang ) ) << ";" << mathOp.perimeter( (*regions_vec).getIt( currTriang ) ) << endl;

    //dispEst->getJointRegions(joinRegionsVec, currTriang);
    //joinRegionsLabel->setTriang(joinRegionsVec);
    dispEst->getSimRegions( simTriangs, simVal, currTriang);
    qt_dispWidget->setTriangs(points);
    qt_triangSim->setTriangSim( simTriangs, simVal );

    int idx = (*regions_vec)[currTriang].depth.size()-1;
    qt_dispWidget->setTriangs(points);
    qt_dispWidget->setDisparity((int)(*regions_vec)[currTriang].depth[idx]);
    qt_dispWidget->setProbsVec( &(*regions_vec)[currTriang].probs, &(*regions_vec)[currTriang].probs );
    //qt_dispWidget->setProbsVec( &(*regions_vec)[currTriang].probsTime, &(*regions_vec)[currTriang].probsAgg );

	
    qt_dispWidget->resize(0,0);
    //qt_triangSim->resize(600,600);
}

void QT_MainWidget::setConfigs(sbl::Config* config) {
	cv::Mat centerImg;
	vector<cv::Mat> sideImgs;
    vector< vector<int> > joinRegionsVec;
	bool refineMesh;

	// FIRST: Setting and Running the Disparity Estimation Algorithm
	currentFrame = config->readInt( "initialFrame" , 0 );
	refineMesh = config->readBool( "refineMesh", true );
    batchMode = config->readBool( "batchMode", false );
	dispEst->setConfig(config);


    dispEst->calculateDisparityMap(currentFrame,true);
    currentFrame++;
	
    // Now I can do the rest....
    this->config = config;
    //triOp = dispEst->getTriOp();
    regions_vec = dispEst->getRegionsVec();

    qt_drawLabel->setConfigs(config);
    qt_drawLabel->setRegionsVec(regions_vec);
    qt_drawLabel->setImg(qt_util.Mat2QImage3(*imgDaemon->getCenterImg()));
    qt_drawLabel->resize(imgDaemon->getCenterImg()->cols, imgDaemon->getCenterImg()->rows);

    qt_edgePts->setConfigs(config);
    qt_edgePts->setPtsVec(dispEst->getPtsVec());
    qt_edgePts->setImg(qt_util.Mat2QImage1(*dispEst->getEdgesImg()));
    qt_edgePts->setWeightLinks( dispEst->getWeightLinks() );
    qt_edgePts->resize(imgDaemon->getCenterImg()->cols, imgDaemon->getCenterImg()->rows);

    dispEst->getDisparityMap(config->readFloat( "dmMult", 1 ),depthMapImg,false);
    //dmLabel->setImg(qt_util.Mat2QImage1(depthMapImg));
    //dmLabel->setConfigs(configs);
    cv::Mat cm_img0;
   // Apply the colormap:
    cv::applyColorMap(depthMapImg, cm_img0, cv::COLORMAP_RAINBOW);
    cv::imwrite("DM.png",depthMapImg);

    qt_triangSim->setConfigs(config);
    qt_triangSim->setRegionsVec(regions_vec);

    qt_triangsAge->setConfigs(config);
    qt_triangsAge->setRegionsVec(regions_vec);
    //qt_drawLabel->setImg(qt_util.Mat2QImage3(*imgDaemon->getCenterImg()));
    qt_triangsAge->resize(imgDaemon->getCenterImg()->cols, imgDaemon->getCenterImg()->rows);


    centerImg = *imgDaemon->getCenterImg();//*configs->getImgCenterPtr();
    sideImgs = *imgDaemon->getSideImgs();//configs->getSideImgs();
    qt_dispWidget->setImgs(centerImg, sideImgs[0], sideImgs[imgDaemon->getNumCameras()-1]);
    int maxDisp = config->readInt( "maxDisp" );
    qt_dispWidget->setMaxDisparity(maxDisp);

    qt_drawLabel->repaint();
    qt_edgePts->repaint();
    qt_dispWidget->repaint();
    qt_triangsAge->repaint();
    qt_triangsAge->update();

    if( batchMode ) updateDMState();
}

QSize QT_MainWidget::minimumSizeHint() const
{
    return QSize(375/2, 450/2);
}


QSize QT_MainWidget::sizeHint() const
{
    return QSize(375/2, 450/2);
}


void QT_MainWidget::calcDispMap() {
	//dispEst->calculateDisparityMap(currentFrame,true);
}

void QT_MainWidget::run() {
	
}
