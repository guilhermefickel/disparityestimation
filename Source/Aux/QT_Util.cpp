#include "QT_Util.h"

QT_Util::QT_Util() {

}

QT_Util::~QT_Util() {

}

cv::Mat QT_Util::QImage32Mat(QImage &src) {
	if(src.width()==0)
		return cv::Mat::zeros(10,10,CV_8UC3);
	cv::Mat dest(src.width(), src.height(), CV_8UC3);
	std::vector<cv::Mat> planes;

	cv::split(dest, planes);
	for (int y = 0; y < src.height(); ++y) {
        QRgb *destrow = (QRgb*)src.scanLine(y);
        for (int x = 0; x < src.width(); ++x) {
            //destrow[x] = qRgba(ptr[x*3+2], ptr[x*3+1], ptr[x*3+0], 255);
			QColor color(destrow[x]);
			planes[2].ptr<unsigned char>(y)[x] = (unsigned char)color.red();
			planes[1].ptr<unsigned char>(y)[x] = (unsigned char)color.green();
			planes[0].ptr<unsigned char>(y)[x] = (unsigned char)color.blue();
		}
    }

	cv::merge(planes, dest);
    //cv::imshow("DEBUG", dest);
	cv::waitKey();
	
	return dest;
}

QImage QT_Util::Mat2QImage3(cv::Mat &src) {
        QImage dest(src.cols, src.rows, QImage::Format_ARGB32);
        for (int y = 0; y < src.rows; ++y) {
                unsigned char* ptr = src.ptr<unsigned char>(y);
                QRgb *destrow = (QRgb*)dest.scanLine(y);
                for (int x = 0; x < src.cols; ++x)
                    destrow[x] = qRgba(ptr[x*3+2], ptr[x*3+1], ptr[x*3+0], 255);
        }
        return (dest);
}

QImage QT_Util::Mat2QImage1(cv::Mat &src) {
        QImage dest(src.cols, src.rows, QImage::Format_ARGB32);
        for (int y = 0; y < src.rows; ++y) {
                unsigned char* ptr = src.ptr<unsigned char>(y);
                QRgb *destrow = (QRgb*)dest.scanLine(y);
                for (int x = 0; x < src.cols; ++x)
                    destrow[x] = qRgba(ptr[x], ptr[x], ptr[x], 255);
        }
        return (dest);
}


// BUG!!! Have to check the limits of the image!!!
QImage QT_Util::cropPixmap(QImage& img, int topleftX, int topleftY, int botrightX, int botrightY) {
	if(topleftX < 0) topleftX = 0;
	if(topleftY < 0) topleftY = 0;
	if(botrightX >= img.width()) botrightX = img.width()-1;
	if(botrightY >= img.height()) botrightY = img.height()-1;

	QImage caramba(botrightX-topleftX, botrightY-topleftY, QImage::Format_ARGB32);
	for(int i=topleftY; i<botrightY && i>=0 && i<img.height(); i++) {
		QRgb *destrow = (QRgb*)caramba.scanLine(i-topleftY);
		QRgb *sourcerow = (QRgb*)img.scanLine(i);
		for(int j=topleftX; j<botrightX && j>=0 && j<img.width(); j++) {
			destrow[j-topleftX] = sourcerow[j];
		}
	}

	return caramba;
}
