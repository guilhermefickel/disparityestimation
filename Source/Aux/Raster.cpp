#include "Raster.h"

Raster::Raster() {
	regions_vec = NULL;

	deltaY = new int[MAX_NUM_THREADS];
	ContourX = new int**[MAX_NUM_THREADS];
	for(int i=0; i<MAX_NUM_THREADS; i++) {
        ContourX[i] = new int*[2048];
        for(int j=0; j<2048; j++)
			ContourX[i][j] = new int[2];
	}

    minINT = -64000;
    maxINT = 64000;
}

Raster::~Raster() {

}

void Raster::setRegions(FastVec<region>* regions_vec) {
	this->regions_vec = regions_vec;
}

void Raster::scanLine(int threadPos, int x1, int y1, int x2, int y2)
{
	int sx, sy, dx1, dy1, dx2, dy2, x, y, m, n, k, cnt;
	
	dx1=0;

	sx = x2 - x1;
	sy = y2 - y1;

	if (sx > 0) dx1 = 1;
	else if (sx < 0) dx1 = -1;
	else dy1 = 0;

	if (sy > 0) dy1 = 1;
	else if (sy < 0) dy1 = -1;
	else dy1 = 0;

	m = abs(sx);
	n = abs(sy);
	dx2 = dx1;
	dy2 = 0;

	if (m < n)
	{
		m = abs(sy);
		n = abs(sx);
		dx2 = 0;
		dy2 = dy1;
	}

	x = x1; y = y1;
	cnt = m + 1;
	k = n / 2;

	while (cnt--)
	{    
		if (x < ContourX[threadPos][y][0]) ContourX[threadPos][y][0] = x;
		if (x > ContourX[threadPos][y][1]) ContourX[threadPos][y][1] = x;
    

		k += n;
		if (k < m)
		{
			x += dx2;
			y += dy2;
		}
		else
		{
			k -= m;
			x += dx1;
			y += dy1;
		}
	}
}

void Raster::setTriangle(int it, int threadPos) {
	if(regions_vec == NULL) {
		std::cout << "Trying to use Raster without initializing it correctly with regions_vec pointer." << std::endl;
		exit(1);
	}

	FastVec<region>::iterator it_r;
	it_r = regions_vec->getIt(it);
	
	// BUG!!!! top MIGHT not be y[0]!!!
	deltaY[threadPos] = it_r->y[0];
	top = it_r->y[0];
	bot = it_r->y[2];


	for (int y = 0; y <= it_r->y[2]-it_r->y[0]; y++)
	{
        ContourX[threadPos][y][0] = maxINT; // min X
        ContourX[threadPos][y][1] = minINT; // max X
	}

	// using this deltaY to only use the begining part of CountourX
	scanLine(threadPos, it_r->x[0], it_r->y[0]-deltaY[threadPos], it_r->x[1], it_r->y[1]-deltaY[threadPos]);
	scanLine(threadPos, it_r->x[1], it_r->y[1]-deltaY[threadPos], it_r->x[2], it_r->y[2]-deltaY[threadPos]);
	scanLine(threadPos, it_r->x[2], it_r->y[2]-deltaY[threadPos], it_r->x[0], it_r->y[0]-deltaY[threadPos]);
}

void Raster::setTriangle(int y1, int x1, int y2, int x2, int y3, int x3, int threadPos) {
	deltaY[threadPos] = y1;
	top = y1;
	bot = y3;

	for (int y = 0; y <= y3-y1; y++)
	{
        ContourX[threadPos][y][0] = maxINT; // min X
        ContourX[threadPos][y][1] = minINT; // max X
	}

	// using this deltaY to only use the begining part of CountourX
	scanLine(threadPos, x1, y1-deltaY[threadPos], x2, y2-deltaY[threadPos]);
	scanLine(threadPos, x2, y2-deltaY[threadPos], x3, y3-deltaY[threadPos]);
	scanLine(threadPos, x3, y3-deltaY[threadPos], x1, y1-deltaY[threadPos]);
}

void Raster::getHorizLimits(int row, int& left, int& right, int threadPos) {
	left = ContourX[threadPos][row-deltaY[threadPos]][0];
	right = ContourX[threadPos][row-deltaY[threadPos]][1];
}

void Raster::getVertLimits(int &top, int &bot) {
	top = this->top;
	bot = this->bot;
}
