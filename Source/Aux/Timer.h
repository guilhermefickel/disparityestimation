
#ifndef TIMER_H_
#define TIMER_H_

#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <sys/time.h>

using namespace std;

class Timer {
private:
    char name[1024];
    timeval startTime;
    timeval lastTime;

public:

    void setName( const char* name );
    void start();
    void pause();
    void init();
    void cont();
    double stop();
    double totalTime();
    void printTime(double duration);
    void printTime();
};

#endif /* TIMER_H_ */
