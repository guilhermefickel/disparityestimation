#include "ImgOp.h"

ImgOp::ImgOp() {
	img = NULL;
}

ImgOp::~ImgOp() {

}

void ImgOp::config(Raster* raster) {
	this->raster = raster;
}


cv::Mat ImgOp::safeOpenImage(const char name[512], int cropSize, bool blur) {
	cv::Mat aux = cv::imread(name);
	if(aux.empty() ) {
		std::cout << "Problem opening image |" << name << "|" << std::endl;
		system("PAUSE");
	}
	cv::Rect myROI(cropSize, cropSize, aux.cols-2*cropSize, aux.rows-2*cropSize);
	aux = aux(myROI);
    //cout << "Img read:|"<<name<<"|"<<endl;
	//cvtColor(aux, aux, CV_BGR2Lab);
    if( blur ) cv::GaussianBlur(aux, aux, cv::Size(5,5) , 1.6 );
	
	return aux;
}

cv::Mat ImgOp::safeOpenImageLab(const char name[512], int cropSize, bool blur) {
    cv::Mat aux = cv::imread(name);
    if(aux.empty() ) {
        std::cout << "Problem opening image |" << name << "|" << std::endl;
        system("PAUSE");
    }
    cv::Rect myROI(cropSize, cropSize, aux.cols-2*cropSize, aux.rows-2*cropSize);
    aux = aux(myROI);
    if( blur ) cv::GaussianBlur(aux, aux, cv::Size(5,5) , 1.6 );

    cvtColor(aux, aux, CV_BGR2Lab);

    return aux;
}


void ImgOp::calcRegionRGB(cv::Mat* img, cv::Mat* imgLab, int it, int y1, int x1, int y2, int x2, int y3, int x3, float meanRGB[3], float meanLab[3], int& numPts, int** triangsIdx) {
	int left, right, top, bot;

	numPts = 0;
	meanRGB[0] = meanRGB[1] = meanRGB[2] = 0;
    meanLab[0] = meanLab[1] = meanLab[2] = 0;

	// get vals to do an bresenham...
	raster->setTriangle(y1, x1, y2, x2, y3, x3);
	raster->getVertLimits(top, bot);
	for(int i=top; i<=bot; i++) {
		raster->getHorizLimits(i, left, right);
		for(int j=left; j<=right; j++) {
            meanRGB[0] += img->data[img->step * i + j*3 + 0];
            meanRGB[1] += img->data[img->step * i + j*3 + 1];
            meanRGB[2] += img->data[img->step * i + j*3 + 2];

            meanLab[0] += imgLab->data[imgLab->step * i + j*3 + 0];
            meanLab[1] += imgLab->data[imgLab->step * i + j*3 + 1];
            meanLab[2] += imgLab->data[imgLab->step * i + j*3 + 2];

			numPts++;
			triangsIdx[i][j] = it;
		}
	}

    meanRGB[0] = float(meanRGB[0])/numPts;
	meanRGB[1] = float(meanRGB[1])/numPts;
	meanRGB[2] = float(meanRGB[2])/numPts;

    meanLab[0] = float(meanLab[0])/numPts;
    meanLab[1] = float(meanLab[1])/numPts;
    meanLab[2] = float(meanLab[2])/numPts;
}


std::vector<point_t> ImgOp::disparityComparsion(cv::Mat& dispMapL, cv::Mat& dispMapR, float mult, float mult2) {
	unsigned char* ptrDML;
	unsigned char* ptrDMR;
	std::vector<point_t> pts;
	point_t p;

	if(mult2 = -1)
		mult2 = mult;
	
	for(int i=0; i<dispMapL.rows; i++) {
		ptrDML = dispMapL.ptr<unsigned char>(i);
		ptrDMR = dispMapR.ptr<unsigned char>(i);
		for(int j=0; j<dispMapL.cols; j++) {
			if(abs(float(ptrDML[j])/float(mult)-float(ptrDMR[j])/float(mult2)) >= 1.1) {
				p.x = j;  p.y = i;  p.z[0] = p.z[1] = 0;
				pts.push_back(p);
			}
		}
	}

	return pts;
}


std::vector<point_t> ImgOp::crossCheck(cv::Mat& dispMapL, cv::Mat& dispMapR, float mult, float mult2) {
	unsigned char* ptrDML;
	unsigned char* ptrDMR;
	std::vector<point_t> pts;
	point_t p;

	if(mult2 = -1)
		mult2 = mult;
	
	for(int i=0; i<dispMapL.rows; i++) {
		ptrDML = dispMapL.ptr<unsigned char>(i);
		ptrDMR = dispMapR.ptr<unsigned char>(i);
		for(int j=0; j<dispMapL.cols; j++) {
			if(j-int(ptrDML[j]/mult) >=0){
				if(abs(ptrDML[j]/mult-ptrDMR[j-int(ptrDML[j]/mult)]/mult2) >= 1.1) {
					p.x = j;  p.y = i;  p.z[0] = 0; p.z[1] = 0;
					pts.push_back(p);
				}
			}
		}
	}

	if(pts.size() == 0)
		cout << "Problem in crossCheck" << endl;

	return pts;
}

cv::Mat ImgOp::triangulationImg(cv::Mat* img, vector<point> pts) {
	cv::Mat triImg = img->clone();
	cv::Point p1(1,1);
	cv::Point p2(1,1);

	for(int it=0; it<pts.size()-1; it+=3) {
		for(int ip=0; ip<3; ip++) {
			p1.x = pts.at(it+ip%3).x;  p1.y = pts.at(it+ip%3).y;
			p2.x = pts.at(it+(ip+1)%3).x;  p2.y = pts.at(it+(ip+1)%3).y;
			cv::line( triImg, p1, p2, cv::Scalar(0,0,255), 1, CV_AA );
			//cv::circle( triImg, p1, 4, cv::Scalar(0,0,255) );
		}
	}

	return triImg;
}

cv::Mat ImgOp::linksImg( cv::Mat* img, vector< tuple<float,int,int> >* weightLinks, FastVec<point_t>* points_vec ) {
	cv::Mat triImg = img->clone();
	cv::Point p1(1,1);
	cv::Point p2(1,1);

	for( vector< tuple<float,int,int> >::iterator it_t = weightLinks->begin(); it_t != weightLinks->end(); it_t++ ) {
		p1.x = (*points_vec)[ get<1>(*it_t) ].x;  p1.y = (*points_vec)[ get<1>(*it_t) ].y;
		p2.x = (*points_vec)[ get<2>(*it_t) ].x;  p2.y = (*points_vec)[ get<2>(*it_t) ].y;
		cv::line( triImg, p1, p2, cv::Scalar( get<0>(*it_t), get<0>(*it_t), get<0>(*it_t) ), 1, CV_AA );
	}

	return triImg;
}
