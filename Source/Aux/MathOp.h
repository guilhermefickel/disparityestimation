#ifndef MATHOP_H
#define MATHOP_H

#include "../DataStructure/point_t.h"
#include "../DataStructure/point.h"
#include "../DataStructure/triang.h"
#include "../DataStructure/region.h"
#include "../DataStructure/FastVec.h"

#include <opencv2/core.hpp>


using namespace std;

class MathOp {
private:
	point_t p1, p2, pf; // used in the functions, don't have to reallocated every time

public:
	MathOp();
	~MathOp();

	float distBetweenTriangs(FastVec<point_t>* points_vec, triang& t1, triang& t2);
	float magnitudeVec(point_t v1);
	void matrix_inverse(float **Min, float **Mout, int actualsize, int width, int threadIdx);
	void matrix_mul(float** m1, float** m2, float** res, int rows, int cols, int rep, int sizeMat, int threadIdx);
	point crossProduct(point p1, point p2);
	int dotProduct(point p1, point p2);
	int quickL2(int dx, int dy);
	point pointSub(point p1, point p2);
    float perimeter( int y[3], int x[3] );
    float perimeter( FastVec<region>::iterator it );
    float area( int y[3], int x[3] );
    float area( FastVec<region>::iterator it );
};

#endif
