#ifndef IMAGESDAEMON_H
#define IMAGESDAEMON_H

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <vector>
#include "ImgOp.h"
#include <sbl/core/Config.h>
#include <sbl/core/String.h>
#include <sbl/core/StringUtil.h>

class ImagesDaemon {
private:
	sbl::Config* config;
	ImgOp* imgOp;
	int firstFrame;
	int currFrame;
	int numFrames;
	int tempWindow;
	int centerCam;
	int leftCam;
	int numSideCams;
	int maxNumSideCams;
	bool inverseCamNumeration;
    int numFormatParams;

	std::vector< std::vector<cv::Mat> > sideVec;
	std::vector< cv::Mat > centerImgVec;
    std::vector< std::vector<cv::Mat> > blurredSideVec;
    std::vector< cv::Mat > blurredCenterImgVec;
    std::vector< std::vector<cv::Mat> > labSideVec;
    std::vector< cv::Mat > labCenterImgVec;
	sbl::String imgName;

	sbl::String getImName(int centerCam, int frameNum);
	void readImgs();

public:
	ImagesDaemon();
	~ImagesDaemon();

	void setConfig( sbl::Config* config );

	cv::Mat* getCenterImg();
	std::vector<cv::Mat>* getSideImgs();
    cv::Mat* getBlurredCenterImg();
    cv::Mat* getLabCenterImg();
    std::vector<cv::Mat>* getBlurredSideImgs();
    std::vector<cv::Mat>* getLabSideImgs();
	void nextFrame();
	int getCurrentFrame();
	int getCurrentTime();
	int getFirstFrame();
	int getTempWindow();
	int getMaxNumSideCams();
	int getNumCameras();
	cv::Mat getImg( int frameIndex );
};


#endif
