#ifndef CONSTANTS_H
#define CONSTANTS_H

#define MAX_NUM_THREADS				16
#define MAX_EDGES_TO_POINT			128
#define MAX_TRIANGS_TO_POINT		128
#define MAX_NUM_TRIANG_NEIGHBORS	512
#define NUM_MAX_TRIANGS				55000
#define MAX_NUM_TRIANGS				55000
#define NUM_MAX_POINTS				35000
#define NUM_MAX_EDGES				85000

#endif
