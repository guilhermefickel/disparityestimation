#ifndef IMGOP_H
#define IMGOP_H

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
// #include <opencv2/gpu/gpu.hpp>
// #include <opencv2/cudaimgproc.hpp>


#include <vector>
#include <tuple>

#include "../DataStructure/point_t.h"
#include "../DataStructure/point.h"
#include "../Aux/Raster.h"

using namespace std;

class ImgOp {
private:
	cv::Mat* img;
	Raster* raster;

public:
	ImgOp();
	~ImgOp();

	void config(Raster* raster);
    void calcRegionRGB(cv::Mat* img, cv::Mat *imgLab, int it, int y1, int x1, int y2, int x2, int y3, int x3, float meanRGB[3], float meanLab[3], int& numPts, int** triangsIdx);
	std::vector<point_t> disparityComparsion(cv::Mat& dispMapL, cv::Mat& dispMapR, float mult, float mult2 = -1);
	std::vector<point_t> crossCheck(cv::Mat& dispMapL, cv::Mat& dispMapR, float mult, float mult2 = -1);
	//cv::Mat* rasterDispMap(float dispMult, vector<triang_mesh>& triangs_mesh, vector<triang>& triangs_vec, int rows, int cols);
	//region calcRegionData(vector<triang>::iterator it, int delta, vector<cv::Mat>& planes);
    cv::Mat safeOpenImage(const char name[512], int cropSize=0, bool blur = false);
    cv::Mat safeOpenImageLab(const char name[512], int cropSize=0, bool blur = false);
	cv::Mat triangulationImg(cv::Mat* img, vector<point> pts);
	cv::Mat linksImg( cv::Mat* img, vector< tuple<float,int,int> >* weightLinks, FastVec<point_t>* points_vec );

};

#endif
