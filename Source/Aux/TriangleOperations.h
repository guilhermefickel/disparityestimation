#ifndef TRIANGLEOPERATIONS_H
#define TRIANGLEOPERATIONS_H

#include <iostream>
#include <queue>
#include <set>
#include "../DataStructure/FastVec.h"
#include "../DataStructure/edge.h"
#include "../DataStructure/point_t.h"
#include "../DataStructure/point.h"
#include "../DataStructure/triang.h"
#include "../DataStructure/ortho_triang.h"
#include "../DataStructure/region.h"
#include "Raster.h"
#include "MathOp.h"
#include "ImgOp.h"
#include "Constants.h"
#include "ImagesDaemon.h"
#include <sbl/core/Config.h>

using namespace std;


class TriangleOperations {
private:
	sbl::Config* config;
	MathOp* mathOp;
	ImgOp* imgOp;
	FastVec<point_t>* points_vec;
	FastVec<edge>* edges_vec;
	FastVec<triang>* triangs_vec;
	FastVec<region>* regions_vec;
	FastVec<orthoTriang>* orthoTriangs_vec;
	ImagesDaemon* imgDaemon;
	Raster* raster;
	int** ptsIdx;
	int** triangsIdx;
	int numParticles;

	vector< vector<int> >* adj_list; // used in search functions

	bool isInit;
	int maxDisp;
	int height, width; // height and width of the image, therefore of the triangulation also

	// local variables for the search algorithm, one for each thread
	int marked_vec[MAX_NUM_THREADS][MAX_NUM_TRIANG_NEIGHBORS];
	int endVec[MAX_NUM_THREADS];
	int Q[MAX_NUM_THREADS][MAX_NUM_TRIANG_NEIGHBORS];
	int beginQ[MAX_NUM_THREADS];
	int endQ[MAX_NUM_THREADS];
	// local variables for the search algorithm

	void findTriangToRmFromEdge(int eIdx, int tIdx);

public:
	TriangleOperations();
	~TriangleOperations();

	// huge function, I know... but is (at least should be) only called once.
	void setConfig(sbl::Config* config, FastVec<point_t>* points_vec, FastVec<edge>* edges_vec, FastVec<triang>* triangs_vec, FastVec<orthoTriang>* orthoTriangs_vec, FastVec<region>* regions_vec, Raster* raster, ImagesDaemon* imgDaemon, int height, int width, int** ptsIdx, int** triangsIdx);

	// Modify the mesh
	void clearTriangulation();
    int insert_point_xy(int x,int y, int radius, int particleIdx=-1, bool isEdge=false);
	// warning: I change badPtsVec!!!!
	int insert_point_xy_realloc(int x,int y);
	void insert_triang(int points[3]) ;
    void insert_region(int idx, int points[3]);
	void update_region(int idx);
	void insert_edge(int p1, int p2); 
	void createAdjList();
	void closeMesh3D(FastVec<region>* regions_vec);
    bool rmTriang( int tIdx, bool addToVec=false );
    bool rmEdge( int eIdx );
    bool rmPt( int pIdx );

	// Access the mesh
	bool points_connected(int p1, int p2);
	int find_edge(int p1, int p2);
	int find_triang_neighbors(int index, int** list_neighbors, float radius, int threadIdx=0);
	void find_triangs(int index, int vertex, vector<int>& list_triangs, bool insertMyself = true);
	void find_triangs(int vertex, int list_triangs[MAX_NUM_THREADS][MAX_NUM_TRIANG_NEIGHBORS], int endList[MAX_NUM_THREADS], int threadIdx, bool insertMyself = true);
	void find_vertices(int index, int vertex, vector<int>& list_triangs, vector<int>& list_vertices);
	void find_vertices(int vertex, int list_triangs[MAX_NUM_THREADS][MAX_NUM_TRIANG_NEIGHBORS], int endListTriangs[MAX_NUM_THREADS], int list_vertices[MAX_NUM_THREADS][MAX_NUM_TRIANG_NEIGHBORS], int endListVertices[MAX_NUM_THREADS], int threadIdx);
	int find_triang_from_pt(int y, int x);
	bool PointInTriangle(point p, point a, point b, point c);
	int getTriangFromPts(int points[3]);


	// Utility functions
	bool sameSide(point p1, point p2, point a, point b);
    int sameTriangle( int points[3] );
	bool cmp_points(point_t& p1, point_t& p2);
	int calcTriangleArea(int it);
	void order_points(int points[3], int order[3]);
	void order_points(int it, int order[3]);
	void setRmPtsVec( vector<point_t>* rmPtsVec );
    float symBtwTriangs(int t1, int t2);
};


#endif
