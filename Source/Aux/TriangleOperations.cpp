﻿#include "TriangleOperations.h"

TriangleOperations::TriangleOperations() {
	mathOp = new MathOp;
	imgOp = new ImgOp;
	isInit = false;


	adj_list = new vector<vector<int>>;
	adj_list->resize(NUM_MAX_TRIANGS);
	numParticles = 0;
}

TriangleOperations::~TriangleOperations() {

}

void TriangleOperations::setConfig(sbl::Config* config, FastVec<point_t>* points_vec, FastVec<edge>* edges_vec, FastVec<triang>* triangs_vec, FastVec<orthoTriang>* orthoTriangs_vec, FastVec<region>* regions_vec, Raster* raster, ImagesDaemon* imgDaemon, int height, int width, int** ptsIdx, int** triangsIdx) {
	this->config = config;
	this->points_vec = points_vec;
	this->edges_vec = edges_vec;
	this->triangs_vec = triangs_vec;
	this->orthoTriangs_vec = orthoTriangs_vec;
	this->regions_vec = regions_vec;
	this->height = height;
	this->width = width;
	this->ptsIdx = ptsIdx;
	this->triangsIdx = triangsIdx;
	this->raster = raster;
	this->imgDaemon = imgDaemon;

	imgOp->config(raster);
	isInit = true;
	maxDisp = config->readInt( "maxDisp" );
}

void TriangleOperations::clearTriangulation() {
    //points_vec->clear();
	edges_vec->clear();
	triangs_vec->clear();
	regions_vec->clear();
	orthoTriangs_vec->clear();

    for( FastVec<point_t>::iterator it = points_vec->begin(); it != points_vec->end(); it++ ) {
        it->numEdges = 0;
        it->numTriangs = 0;
    }


	int H = imgDaemon->getCenterImg()->rows;
	int W = imgDaemon->getCenterImg()->cols;

	for(int i=0; i<H; i++) {
		for(int j=0; j<W; j++) {
            //ptsIdx[i][j] = -1;
			triangsIdx[i][j] = -1;
		}
	}
}

int TriangleOperations::insert_point_xy(int x, int y, int radius, int particleIdx, bool isEdge) {
	FastVec<point_t>::iterator it_p;
	int ptIdx;

    if( ptsIdx[ y ][ x ] != -1 ) {
		cout << "Point already exist!! points_vec[" << ptsIdx[ y ][ x ] << "] = [";
		cout << (*points_vec)[ ptsIdx[ y ][ x ] ].y << "," << (*points_vec)[ ptsIdx[ y ][ x ] ].x << "]" << endl;
		//system("PAUSE");
		return -1;
	}

	it_p = points_vec->itInsert(ptIdx);
	if( particleIdx == -1 )
		it_p->particleIdx = numParticles++;
	else
		it_p->particleIdx = particleIdx;

	it_p->x = it_p->oldX = x;
	it_p->y = it_p->oldY = y;
	it_p->numEdges = 0;
	it_p->numTriangs = 0;
	it_p->z[0] = -1;
	it_p->z[1] = -1;
	it_p->isNew = true;
	it_p->used = true;
	it_p->dispDisc = false;
	it_p->edge = isEdge;
    it_p->radius = radius;
	it_p->iter = 0;
	for(int i=0; i<MAX_NUM_THREADS; i++) it_p->mark[i] = false;
	ptsIdx[y][x] = ptIdx;
	if(ptIdx < 0) {
		cout << "Big problem in insert_point_xy! ptIdx < 0" << endl;
		system("PAUSE");
	}

	return ptIdx;
}

void TriangleOperations::findTriangToRmFromEdge(int eIdx, int tIdx) {
	for(int t=0; t<(*edges_vec)[eIdx].numTriangs; t++) {
		if((*triangs_vec)[(*edges_vec)[eIdx].triangs_vec[t]].stillExists == false && (*edges_vec)[eIdx].triangs_vec[t] != tIdx) {
			rmTriang((*edges_vec)[eIdx].triangs_vec[t], true);
			return;
		}
	}
	cout << "Didn't found a triangle to remove :(" << endl;
	system("PAUSE");
}

// warning: I change badPtsVec!!!!
int TriangleOperations::insert_point_xy_realloc(int x,int y) {
	FastVec<point_t>::iterator it_p;
	int ptIdx;

	if(ptsIdx[y][x] != -1) {
		cout << "Should be a bad and removed point!!!" << endl;
		system("PAUSE");
	}

	it_p = points_vec->itInsert(ptIdx);

	it_p->x = x;
	it_p->y = y;
	it_p->numEdges = 0;
	it_p->numTriangs = 0;
	it_p->z[0] = -1;
	it_p->z[1] = -1;
	it_p->isNew = true;
	it_p->used = true;
	it_p->dispDisc = false;
	for(int i=0; i<MAX_NUM_THREADS; i++) it_p->mark[i] = false;
	ptsIdx[y][x] = (ptIdx);

	return ptIdx;
}

void TriangleOperations::insert_triang(int points[3]) {
	int order[3]; // to add points from the higher to the lower (y coordinate)
	int tIdx, tIdx2;
	FastVec<triang>::iterator it_t;
	FastVec<region>::iterator it_r;

	if( points[0] < 0 || points[1] < 0 || points[2] < 0 ) {
		cout << "Invalid points..." << endl;
		system("PAUSE");
	}

	it_t = triangs_vec->itInsert(tIdx);
	it_r = regions_vec->itInsert(tIdx2);

    it_t->bad = false;

	if(points_connected(points[0], points[1]) == false)
		insert_edge(points[0], points[1]);
	if(points_connected(points[0], points[2]) == false)
		insert_edge(points[0], points[2]);
	if(points_connected(points[1], points[2]) == false)
		insert_edge(points[1], points[2]);

	it_t->used = true;
	it_t->stillExists = true;
    it_t->lastFrame = it_t->firstFrame = imgDaemon->getCurrentTime();

	it_t->edges_vec[0] = find_edge(points[0], points[1]);
	it_t->edges_vec[1] = find_edge(points[0], points[2]);
	it_t->edges_vec[2] = find_edge(points[1], points[2]);

	int e0 = it_t->edges_vec[0];
	int e1 = it_t->edges_vec[1];
	int e2 = it_t->edges_vec[2];

	if((*edges_vec)[e0].numTriangs > 1 || (*edges_vec)[e1].numTriangs > 1 || (*edges_vec)[e2].numTriangs > 1) {
		if((*edges_vec)[e0].numTriangs > 1)
			findTriangToRmFromEdge(e0, tIdx);
		if((*edges_vec)[e1].numTriangs > 1)
			findTriangToRmFromEdge(e1, tIdx);
		if((*edges_vec)[e2].numTriangs > 1)
			findTriangToRmFromEdge(e2, tIdx);
		// I need to find the edges that have >1 triangles, find the one that stillExists==false and remove it!
	}
	if((*points_vec)[points[0]].numTriangs >= MAX_TRIANGS_TO_POINT || (*points_vec)[points[1]].numTriangs >= MAX_TRIANGS_TO_POINT || (*points_vec)[points[2]].numTriangs >= MAX_TRIANGS_TO_POINT) {
		cout << "Catastrofical error! (*points_vec)[points[0]].numTriangs >= MAX_TRIANGS_TO_POINT!" << endl;
		cout << "used: " << (*points_vec)[points[0]].used << ";" << (*points_vec)[points[1]].used << ";" << (*points_vec)[points[2]].used << endl;
		system("PAUSE");
	}

	(*edges_vec)[e0].triangs_vec[(*edges_vec)[e0].numTriangs] = tIdx;
	(*edges_vec)[e0].numTriangs++;
	(*edges_vec)[e1].triangs_vec[(*edges_vec)[e1].numTriangs] = tIdx;
	(*edges_vec)[e1].numTriangs++;
	(*edges_vec)[e2].triangs_vec[(*edges_vec)[e2].numTriangs] = tIdx;
	(*edges_vec)[e2].numTriangs++;


	order_points(points,order); // inserting points in order!! Important!!
	it_t->points_vec[0] = points[order[0]];
	it_t->points_vec[1] = points[order[1]];
	it_t->points_vec[2] = points[order[2]];

	for(int i=0; i<MAX_NUM_THREADS; i++)
		it_t->mark[i] = false;

	(*points_vec)[points[0]].triangs_vec[(*points_vec)[points[0]].numTriangs] = tIdx;
	(*points_vec)[points[0]].numTriangs++;
	(*points_vec)[points[1]].triangs_vec[(*points_vec)[points[1]].numTriangs] = tIdx;
	(*points_vec)[points[1]].numTriangs++;
	(*points_vec)[points[2]].triangs_vec[(*points_vec)[points[2]].numTriangs] = tIdx;
	(*points_vec)[points[2]].numTriangs++;


	for(int e=0; e<3; e++) {
		for(int i=0; i<2; i++) {
				if((*edges_vec)[it_t->edges_vec[e]].points_vec[i] != it_t->points_vec[0] &&
					(*edges_vec)[it_t->edges_vec[e]].points_vec[i] != it_t->points_vec[2] &&
					(*edges_vec)[it_t->edges_vec[e]].points_vec[i] != it_t->points_vec[1]) {
						cout << "I'm inserting a triangle with a loose edge!!" << endl;
				}

		}
	}

    insert_region(tIdx,it_t->points_vec);

	if((*edges_vec)[e0].numTriangs > 2 || (*edges_vec)[e1].numTriangs > 2 || (*edges_vec)[e2].numTriangs > 2) {
		cout << "Catastrofical error! (*edges_vec)[e0].numTriangs > 2!" << endl;
		cout << "used: " << (*edges_vec)[e1].used << ";" << (*edges_vec)[e0].used << ";" << (*edges_vec)[e2].used << endl;
		system("PAUSE");
	}
	if((*points_vec)[points[0]].numTriangs >= MAX_TRIANGS_TO_POINT || (*points_vec)[points[1]].numTriangs >= MAX_TRIANGS_TO_POINT || (*points_vec)[points[2]].numTriangs >= MAX_TRIANGS_TO_POINT) {
		cout << "Catastrofical error! (*points_vec)[points[0]].numTriangs >= MAX_TRIANGS_TO_POINT!" << endl;
		cout << "used: " << (*points_vec)[points[0]].used << ";" << (*points_vec)[points[1]].used << ";" << (*points_vec)[points[2]].used << endl;
		system("PAUSE");
	}
}

void TriangleOperations::insert_region(int idx, int points[3]) {
	FastVec<region>::iterator it_r;
	int order[3];


	//it_r = regions_vec->itInsert(auxTrash);
	it_r = regions_vec->getIt(idx);


	// saving the three points of the triangle. Possibly ordenate it first? For easier
	// access on rastering this region?
    it_r->x[0] = (*points_vec)[points[0]].x;
    it_r->y[0] = (*points_vec)[points[0]].y;
    it_r->x[1] = (*points_vec)[points[1]].x;
    it_r->y[1] = (*points_vec)[points[1]].y;
    it_r->x[2] = (*points_vec)[points[2]].x;
    it_r->y[2] = (*points_vec)[points[2]].y;
	it_r->z[0] = it_r->z[1] = it_r->z[2] = 0;
    it_r->ptIdx[0] = points[0];
    it_r->ptIdx[1] = points[1];
    it_r->ptIdx[2] = points[2];


    imgOp->calcRegionRGB(imgDaemon->getCenterImg(), imgDaemon->getLabCenterImg(), it_r->idx, it_r->y[0], it_r->x[0], it_r->y[1], it_r->x[1], it_r->y[2], it_r->x[2], it_r->meanRGB, it_r->meanLab, it_r->numPts, triangsIdx);
    it_r->meanR.clear();
    it_r->meanG.clear();
    it_r->meanB.clear();
    it_r->meanL.clear();
    it_r->meana.clear();
    it_r->meanb.clear();

    it_r->meanR.push_back( it_r->meanRGB[2] );
    it_r->meanG.push_back( it_r->meanRGB[1] );
    it_r->meanB.push_back( it_r->meanRGB[0] );
    it_r->meanL.push_back( it_r->meanLab[0] );
    it_r->meana.push_back( it_r->meanLab[1] );
    it_r->meanb.push_back( it_r->meanLab[2] );

	it_r->probs.clear();
	it_r->probsAgg.clear();
    it_r->probsTime.clear();
	it_r->ratioCost.clear();
    it_r->depth.clear();
    it_r->sum_costs.clear();
    it_r->sum_costs_agg.clear();


	it_r->used = true;
	it_r->stable = false;
    it_r->age = 0;

    it_r->kalmanX = -1;
    it_r->kalmanP = -1;

    for( int i=0; i<3; i++ ) {
        it_r->zKalmanP[i] = it_r->zKalmanX[i] = -1;
    }

    it_r->initPerimeter = mathOp->perimeter( it_r->y, it_r->x );
    it_r->initArea = mathOp->area( it_r->y, it_r->x );
}

void TriangleOperations::update_region(int idx) {
	FastVec<region>::iterator it_r;
	int order[3];
    int moveTo[3];
    double kX[3];
    double kP[3];

    it_r = regions_vec->getIt(idx);

    (*triangs_vec)[idx].lastFrame = imgDaemon->getCurrentTime();

	order_points((*triangs_vec)[it_r->idx].points_vec,order);


    // originalOrder
    // 3 2 1
    // order
    // 2 3 1


	// saving the three points of the triangle. Possibly ordenate it first? For easier
	// access on rastering this region?
	it_r->x[0] = (*points_vec)[(*triangs_vec)[it_r->idx].points_vec[order[0]]].x;
	it_r->y[0] = (*points_vec)[(*triangs_vec)[it_r->idx].points_vec[order[0]]].y;
	it_r->x[1] = (*points_vec)[(*triangs_vec)[it_r->idx].points_vec[order[1]]].x;
	it_r->y[1] = (*points_vec)[(*triangs_vec)[it_r->idx].points_vec[order[1]]].y;
	it_r->x[2] = (*points_vec)[(*triangs_vec)[it_r->idx].points_vec[order[2]]].x;
	it_r->y[2] = (*points_vec)[(*triangs_vec)[it_r->idx].points_vec[order[2]]].y;
	it_r->z[0] = it_r->z[1] = it_r->z[2] = 0;


    for( int i=0; i<3; i++ ) {
        for( int j=0; j<3; j++ ) {
            int ptIdx = (*triangs_vec)[it_r->idx].points_vec[order[i]];
            if( ptIdx == it_r->ptIdx[j] )
                moveTo[j] = i;
        }
        kX[i] = it_r->zKalmanX[i];
        kP[i] = it_r->zKalmanP[i];
    }

    it_r->ptIdx[0] = (*triangs_vec)[it_r->idx].points_vec[order[0]];
    it_r->ptIdx[1] = (*triangs_vec)[it_r->idx].points_vec[order[1]];
    it_r->ptIdx[2] = (*triangs_vec)[it_r->idx].points_vec[order[2]];

    for( int i=0; i<3; i++ ) {
        it_r->zKalmanX[moveTo[i]] = kX[i];
        it_r->zKalmanP[moveTo[i]] = kP[i];
    }


    (*triangs_vec)[it_r->idx].points_vec[0] = it_r->ptIdx[0];
    (*triangs_vec)[it_r->idx].points_vec[1] = it_r->ptIdx[1];
    (*triangs_vec)[it_r->idx].points_vec[2] = it_r->ptIdx[2];

    imgOp->calcRegionRGB(imgDaemon->getCenterImg(), imgDaemon->getLabCenterImg(), it_r->idx, it_r->y[0], it_r->x[0], it_r->y[1], it_r->x[1], it_r->y[2], it_r->x[2], it_r->meanRGB, it_r->meanLab, it_r->numPts, triangsIdx);

    it_r->meanR.push_back(it_r->meanRGB[2]);
    it_r->meanG.push_back(it_r->meanRGB[1]);
    it_r->meanB.push_back(it_r->meanRGB[0]);

    it_r->meanL.push_back(it_r->meanLab[0]);
    it_r->meana.push_back(it_r->meanLab[1]);
    it_r->meanb.push_back(it_r->meanLab[2]);

	it_r->used = true;
    it_r->stable = false;
    it_r->age = (*triangs_vec)[idx].lastFrame - (*triangs_vec)[idx].firstFrame;
}

void TriangleOperations::insert_edge(int p1, int p2) {
	int eIdx;

	FastVec<edge>::iterator it_e;
	it_e = edges_vec->itInsert(eIdx);

	it_e->points_vec[0] = p1;
	it_e->points_vec[1] = p2;
	it_e->numTriangs = 0;
	it_e->used = true;
    it_e->constrained = false;


	// numEdgesp1 = (*points_vec)[p1].numEdges;
	// (*points_vec)[p1].edges_vec[numEdgesp1] = lastEdgeAdded; Same for p2.
	(*points_vec)[p1].edges_vec[(*points_vec)[p1].numEdges] = eIdx;
	(*points_vec)[p2].edges_vec[(*points_vec)[p2].numEdges] = eIdx;
	(*points_vec)[p1].numEdges++;
	(*points_vec)[p2].numEdges++;

	if((*points_vec)[p1].numEdges >= MAX_EDGES_TO_POINT || (*points_vec)[p2].numEdges >= MAX_EDGES_TO_POINT) {
		cout << "Catastrofical error! (*points_vec)[i].numEdges >= MAX_EDGES_TO_POINT!!!" << endl;
		system("PAUSE");
	}
}

void TriangleOperations::createAdjList() {
	vector<int> adj;
	vector<int> triangsToRm;
	FastVec<triang>::iterator it_t;

	// for all triangles
	for(it_t=triangs_vec->begin(); it_t != triangs_vec->end(); it_t++) {
		if(it_t->used == false) { // only for DEBUG, should NEVER happen
			cout << "TriangleOperations::createAdjList() -> found a invalid triangle in triangs_vec. This should be continuous." << endl;
			system("PAUSE");
			continue;
		}
		if(it_t->stillExists == false) {
			triangsToRm.push_back(it_t->idx);
			continue;
		}
		adj.clear();
		adj_list->at(it_t->idx).clear();
		for(int i=0; i<3; i++) {
			if(it_t->edges_vec[i] >= 0) {
				for(int j=0; j<(*edges_vec)[it_t->edges_vec[i]].numTriangs; j++) {
					if((*edges_vec)[it_t->edges_vec[i]].triangs_vec[j] != it_t->idx && (*edges_vec)[it_t->edges_vec[i]].triangs_vec[j] >=0 && (*edges_vec)[it_t->edges_vec[i]].triangs_vec[j] < triangs_vec->size())
						adj.push_back((*edges_vec)[it_t->edges_vec[i]].triangs_vec[j]);
				}
			}
		}
        if( adj.size() > 3 ) {
            cout << "WTF!? Lot of neighbors!?" << endl;
        }
		adj_list->at(it_t->idx) = adj;
	}

	for(int i=0; i<(int)triangsToRm.size(); i++)
		rmTriang(triangsToRm[i]);
}

void TriangleOperations::closeMesh3D(FastVec<region>* regions_vec) {
	int list_triangs[MAX_NUM_THREADS][MAX_NUM_TRIANG_NEIGHBORS];
	int endListTriangs[MAX_NUM_THREADS];
	int list_vertices[MAX_NUM_THREADS][MAX_NUM_TRIANG_NEIGHBORS];
	int endListVertices[MAX_NUM_THREADS];
	float currentDisp;
	int numUnstablePixels=0;

    orthoTriangs_vec->clear();

	// FIRST: join points
	for(FastVec<point_t>::iterator it_p = points_vec->begin(); it_p != points_vec->end(); it_p++) { // for every point
		if(it_p->used == false) { // only for DEBUG, should NEVER happen
			cout << "TriangleOperations::closeMesh3D() -> found a invalid point in points_vec. This should be continuous." << endl;
			system("PAUSE");
			continue;
		}
		int vertex = it_p->idx;
		IplImage* im1;
		IplImage* im2;
		uchar* ptr;
		float sum=0;
		int threadIdx = 0;//(int)omp_get_thread_num();
		endListTriangs[threadIdx] = endListVertices[threadIdx] = 0;
		it_p->iter = 0;


		find_triangs(vertex, list_triangs, endListTriangs, threadIdx);
		find_vertices(vertex, list_triangs, endListTriangs, list_vertices, endListVertices, threadIdx);

		im1 = cvCreateImage(cvSize(endListTriangs[threadIdx]+1,1),IPL_DEPTH_8U,1);
		im2 = cvCreateImage(cvSize(endListTriangs[threadIdx]+1,1),IPL_DEPTH_8U,1);  // release after!!!!
		ptr = (uchar*)im1->imageData;


		float mean = 0.0f; float max = -9999.0f; float min = 9999.0f;
		for(int i=0; i<=endListTriangs[threadIdx]; i++) {
            currentDisp = (*regions_vec)[ list_triangs[threadIdx][i] ].zRef[ list_vertices[threadIdx][i] ];
			mean += currentDisp;
			if(max < currentDisp)
				max = currentDisp;
			if(min > currentDisp)
				min = currentDisp;
			ptr[i] = uchar((currentDisp*(255.0/maxDisp)));
			numUnstablePixels++;
		}
		mean /= float(endListTriangs[threadIdx]+1);

		cvThreshold(im1, im2, 128, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);

		if(max-min < 2.5 || endListTriangs[threadIdx]==0) { // unify all points  WARNING: parameter hard-coded
			for(int i=0; i<=endListTriangs[threadIdx]; i++) {
				(*regions_vec)[ list_triangs[threadIdx][i] ].zClosed[ list_vertices[threadIdx][i] ] = mean;
			}
			it_p->iter = 1;
			it_p->z[0] = it_p->z[1] = mean;
			//cout << "Unify all points always true..." << endl;
		}
		else { // devide into two groups defined by otsu threshold
			float mean = 0;
			float mean2 = 0;
			int size1 = 0;
			int size2 = 0;
			ptr = (uchar*)im2->imageData;
			for(int i=0; i<=endListTriangs[threadIdx]; i++) {
				if(ptr[i] < 128) { // if it's 'class white'
                    mean += (*regions_vec)[ list_triangs[threadIdx][i] ].zRef[ list_vertices[threadIdx][i] ];
					size1++; // calculate it's mean value
				} else {
                    mean2 += (*regions_vec)[ list_triangs[threadIdx][i] ].zRef[ list_vertices[threadIdx][i] ];
					size2++;
				}
			}

			mean /= float(size1);
			mean2 /= float(size2);

			if(size1==0 || size2==0) {
				cout << "Something wrong with OTSU threshold..." << endl;
				system("PAUSE");
			}


			for(int i=0; i<=endListTriangs[threadIdx]; i++) {
				if(ptr[i] < 128)
					(*regions_vec)[ list_triangs[threadIdx][i] ].zClosed[ list_vertices[threadIdx][i] ] = mean;
				else
					(*regions_vec)[ list_triangs[threadIdx][i] ].zClosed[ list_vertices[threadIdx][i] ] = mean2;
				// bimodal
				it_p->iter = 2;
				it_p->z[0] = mean;
				it_p->z[1] = mean2;
			}
		}

		cvReleaseImage(&im1);
		cvReleaseImage(&im2);
	}


// for each edge, find the vertices (shouldn't be more than 4) and close it vertical wise
// SECOND: insert triangles in gaps
    //for(FastVec<edge>::iterator it_edge = (*edges_vec).begin(), cont=0; it_edge != (*edges_vec).end(); it_edge++, cont++) {
    for(FastVec<edge>::iterator it_edge = (*edges_vec).begin(); it_edge != (*edges_vec).end(); it_edge++) {
		// find triangles
		// find points / vertices in triangs_mesh (that's why we need find the triangle indices)
		// for each one, discover if it has one or two heights
		if(it_edge->numTriangs <= 1) // no gap, only one edge
			continue;

		int tri1 = it_edge->triangs_vec[0];
		int tri2 = it_edge->triangs_vec[1];

		// devil's number! Also negative, which only makes it worse
		int pt11, pt12, pt21, pt22;
		pt11 = pt12 = pt21 = pt22 = -666;

		// for all points within the first triangles
		for(int i=0; i<3; i++) {
			//if((*triangs_vec)[tri1].points_vec[i] == it_edge->points_vec[0])
			if((*regions_vec)[tri1].x[i] == (*points_vec)[it_edge->points_vec[0]].x && (*regions_vec)[tri1].y[i] == (*points_vec)[it_edge->points_vec[0]].y )
				pt11 = i;
			//if((*triangs_vec)[tri1].points_vec[i] == it_edge->points_vec[1])
			if((*regions_vec)[tri1].x[i] == (*points_vec)[it_edge->points_vec[1]].x && (*regions_vec)[tri1].y[i] == (*points_vec)[it_edge->points_vec[1]].y )
				pt12 = i;
		}

		for(int i=0; i<3; i++) {
			//if((*triangs_vec)[tri2].points_vec[i] == it_edge->points_vec[0])
			if((*regions_vec)[tri2].x[i] == (*points_vec)[it_edge->points_vec[0]].x && (*regions_vec)[tri2].y[i] == (*points_vec)[it_edge->points_vec[0]].y )
				pt21 = i;
			//if((*triangs_vec)[tri2].points_vec[i] == it_edge->points_vec[1])
			if((*regions_vec)[tri2].x[i] == (*points_vec)[it_edge->points_vec[1]].x && (*regions_vec)[tri2].y[i] == (*points_vec)[it_edge->points_vec[1]].y )
				pt22 = i;
		}

		if(pt11 == -666 || pt12 == -666 || pt21 == -666 || pt22 == -666) {
			cout << "Problem in closeMesh3D(), pt not found" << endl;
			continue;
		}


		if((*points_vec)[it_edge->points_vec[0]].iter != 2 && (*points_vec)[it_edge->points_vec[1]].iter != 2) // if(1 1) done (both unimodal)
			continue;

		float z11 = (*points_vec)[it_edge->points_vec[0]].z[0];
		float z21 = (*points_vec)[it_edge->points_vec[0]].z[1];
		float z12 = (*points_vec)[it_edge->points_vec[1]].z[0];
		float z22 = (*points_vec)[it_edge->points_vec[1]].z[1];

		if( (*points_vec)[it_edge->points_vec[0]].iter == 2 ) {
			//cpyPointMesh3D(tmesh.points[0], (*triangs_mesh)[tri1].points[pt12]);
			//cpyPointMesh3D(tmesh.points[1], (*triangs_mesh)[tri1].points[pt11]);
			//cpyPointMesh3D(tmesh.points[2], (*triangs_mesh)[tri2].points[pt21]);

			//(*triangs_mesh).push_back(tmesh);
			FastVec<orthoTriang>::iterator oT;
			int itIdx;
			oT = orthoTriangs_vec->itInsert(itIdx);

			oT->x[0] = (*regions_vec)[tri1].x[pt12];  oT->x[1] = (*regions_vec)[tri1].x[pt11];  oT->x[2] = (*regions_vec)[tri2].x[pt21];
			oT->y[0] = (*regions_vec)[tri1].y[pt12];  oT->y[1] = (*regions_vec)[tri1].y[pt11];  oT->y[2] = (*regions_vec)[tri2].y[pt21];
			//oT->z[0] = (*regions_vec)[tri1].zClosed[pt12];  oT->z[1] = (*regions_vec)[tri1].zClosed[pt11];  oT->z[2] = (*regions_vec)[tri2].zClosed[pt21];
			oT->z[0] = z12;  oT->z[1] = z11;  oT->z[2] = z21;

			//(*points_vec)[it_edge->points_vec[0]].iter = 3;
		}

		if( (*points_vec)[it_edge->points_vec[1]].iter == 2 ) {
			//cpyPointMesh3D(tmesh.points[0], (*triangs_mesh)[tri1].points[pt12]);
			//cpyPointMesh3D(tmesh.points[1], (*triangs_mesh)[tri2].points[pt21]);
			//cpyPointMesh3D(tmesh.points[2], (*triangs_mesh)[tri2].points[pt22]);
			//
			//(*triangs_mesh).push_back(tmesh);
			FastVec<orthoTriang>::iterator oT;
			int itIdx;
			oT = orthoTriangs_vec->itInsert(itIdx);

			oT->x[0] = (*regions_vec)[tri1].x[pt12];  oT->x[1] = (*regions_vec)[tri2].x[pt21];  oT->x[2] = (*regions_vec)[tri2].x[pt22];
			oT->y[0] = (*regions_vec)[tri1].y[pt12];  oT->y[1] = (*regions_vec)[tri2].y[pt21];  oT->y[2] = (*regions_vec)[tri2].y[pt22];
			//oT->z[0] = (*regions_vec)[tri1].zClosed[pt12];  oT->z[1] = (*regions_vec)[tri2].zClosed[pt21];  oT->z[2] = (*regions_vec)[tri2].zClosed[pt22];
			oT->z[0] = z12;  oT->z[1] = z21;  oT->z[2] = z22;

			//(*points_vec)[it_edge->points_vec[1]].iter = 3;
		}
	}
}

bool TriangleOperations::rmTriang(int tIdx, bool addToVec ) {
	FastVec<triang>::iterator it_t;
	it_t = triangs_vec->getIt(tIdx);
	if(it_t->used == false)
		return false;

    it_t->used = false;
    // for all points...
    for(int ip=0; ip<3; ip++) {
        //int pIdx = it_t->points_vec[ip];
        FastVec<point_t>::iterator it_p = points_vec->getIt(it_t->points_vec[ip]);

        for(int j=0; j<it_p->numTriangs; j++) {
            if(it_p->triangs_vec[j] == tIdx) {
                it_p->triangs_vec[j] = it_p->triangs_vec[it_p->numTriangs-1];
                it_p->numTriangs--;
                j--;
                break;
            }
        }
        //if(it_p->numTriangs == 0)
        //	rmPt(it_p->idx, addRmPtsVec);
    }


	// for all edges...
	for(int ie=0; ie<3; ie++) {
		int eIdx = it_t->edges_vec[ie];
		for(int j=0; j<(*edges_vec)[eIdx].numTriangs; j++) {
			if((*edges_vec)[eIdx].triangs_vec[j] == tIdx) {
				(*edges_vec)[eIdx].triangs_vec[j] = (*edges_vec)[eIdx].triangs_vec[(*edges_vec)[eIdx].numTriangs-1];
				(*edges_vec)[eIdx].numTriangs--;
				j--;
			}
		}
		if((*edges_vec)[eIdx].numTriangs == 0)
            rmEdge(eIdx);
	}


	triangs_vec->rm(tIdx);
	regions_vec->rm(tIdx);

	return true;
}

bool TriangleOperations::rmEdge(int eIdx ) {
	FastVec<edge>::iterator it_e;
	it_e = edges_vec->getIt(eIdx);

	if(it_e->used == false)
		return false;

	it_e->used = false;

    // for all points...
    for(int ip=0; ip<2; ip++) {
        int pIdx = it_e->points_vec[ip];
        for(int j=0; j<(*points_vec)[pIdx].numEdges; j++) {
            if((*points_vec)[pIdx].edges_vec[j] == eIdx) {
                (*points_vec)[pIdx].edges_vec[j] = (*points_vec)[pIdx].edges_vec[(*points_vec)[pIdx].numEdges-1];
                (*points_vec)[pIdx].numEdges--;
                j--;
            }
            //if((*points_vec)[pIdx].numEdges == 0)
            //	rmPt(pIdx, addRmPtsVec);
        }
    }

	// new stuff... is it working???
	for(int it=0; it<it_e->numTriangs; it++) {
		bool triangRm;
		FastVec<triang>::iterator it_t = triangs_vec->getIt(it_e->triangs_vec[it]);
		//for(int j=0; j<3; j++) {
			//if(it_t->edges_vec[j] == eIdx)
        triangRm = rmTriang(it_t->idx, false);
		if( triangRm ) it--;
		//}
	}

	edges_vec->rm(eIdx);

	return true;
}

bool TriangleOperations::rmPt(int pIdx ) {
	FastVec<point_t>::iterator it_p;
	it_p = points_vec->getIt(pIdx);
	// won't remove this point if it is an invalid point OR if it lies in the
	// boundaries of the image.
	if(it_p->used == false) {// || it_p->x==0 || it_p->y==0 || it_p->x==width-1 || it_p->y==height-1)
		ptsIdx[it_p->y][it_p->x] = -1;
		return false;
	}

	it_p->used = false;
	ptsIdx[it_p->y][it_p->x] = -1;


	// for all edges...
	for(int ie=0; ie<it_p->numEdges; ie++) {
		bool edgeRm;
		int eIdx = it_p->edges_vec[ie];
		//for(int j=0; j<2; j++) {
		//	if((*edges_vec)[eIdx].points_vec[j] == pIdx)
        edgeRm = rmEdge(eIdx);
		//}
		//cout << "ie: " << ie << " ; numEdges: " << it_p->numEdges << endl;
		if( edgeRm ) ie--;
	}

	// for all triangles...
	int lastNumTriangs = -999;
	for(int it=0; it<it_p->numTriangs; it++) {
		bool triangRm;
		int tIdx = it_p->triangs_vec[it];
		//for(int j=0; j<3; j++) {
		//	if((*triangs_vec)[tIdx].points_vec[j] == pIdx)
                triangRm = rmTriang(tIdx, true);
		//}
		if( lastNumTriangs == it_p->numTriangs ) {
            //cout << "Something went bad!" << endl;
            // POSSIBLE BUG!!!! Don't know what it means....
		}
		lastNumTriangs = it_p->numTriangs;
		//cout << "it: " << it << " ; numTriangs: " << it_p->numTriangs << endl;
		if( triangRm ) it--;

	}

	points_vec->rm(pIdx);

	return true;
}



/**************************************************************************************
**********************        Access the Mesh3D        **********************************
**************************************************************************************/

bool TriangleOperations::points_connected(int p1, int p2) {
	// for all edges in p1, for all edges in p2, find if they are connected
	for(int i=0; i<(*points_vec)[p1].numEdges; i++)
		// for all the edges inside p1 ((*points_vec)[p1].edges_vec[i]), check if some is connected to p2 (checking both points of each edge)
		if(p2 == (*edges_vec)[(*points_vec)[p1].edges_vec[i]].points_vec[0]  ||  p2 == (*edges_vec)[(*points_vec)[p1].edges_vec[i]].points_vec[1])
			return true;

	return false;
}

int TriangleOperations::find_edge(int p1, int p2) {
	// for all edges of p1
//	if((*points_vec)[p1].numEdges >= 45) {
//		cout << "Deu merda no ponto " << p1 << " pois tem " << (*points_vec)[p1].numEdges << " arestas..." << endl;
//	}
	for(int i=0; i<(*points_vec)[p1].numEdges; i++)
		if(p2 == (*edges_vec)[(*points_vec)[p1].edges_vec[i]].points_vec[0] || p2 == (*edges_vec)[(*points_vec)[p1].edges_vec[i]].points_vec[1])
			return (*points_vec)[p1].edges_vec[i];  // the edge '(*points_vec)[p1].edges_vec[i]' connects both p1 (it's an edge of p1) and p2

	// this should never occur, something is really bad/wrong :'(
	std::cout << "ERROR: find_edge(int p1, int p2) ::> did not found a connecting edge between points " << p1 << " and " << p2 << std::endl << std::endl;
	return 0;
}

//int TriangleOperations::find_triang_neighbors(int index, int list_neighbors[MAX_NUM_THREADS][MAX_NUM_TRIANG_NEIGHBORS], float radius, int threadIdx) {
int TriangleOperations::find_triang_neighbors(int index, int** list_neighbors, float radius, int threadIdx) {
    int t, o;
	int num_neighbors=0;

	beginQ[threadIdx] = endQ[threadIdx] = endVec[threadIdx] = 0;

//3      enqueue v onto Q
	Q[threadIdx][endQ[threadIdx]++] = index; // endQ points to the next empty space in Q

//4      mark v
	(*triangs_vec)[index].mark[threadIdx] = true;
	marked_vec[threadIdx][endVec[threadIdx]] = index;  // endVec points to the last element
//5      while Q is not empty:
    while(endQ[threadIdx] != beginQ[threadIdx] && num_neighbors < MAX_NUM_TRIANG_NEIGHBORS) { //  if they are equal, the queue is empty!!!
//6          t ← Q.dequeue()
		t = Q[threadIdx][beginQ[threadIdx]];
		beginQ[threadIdx] = (beginQ[threadIdx]+1) % MAX_NUM_TRIANG_NEIGHBORS;

		list_neighbors[threadIdx][num_neighbors++] = t;
		//return;

//9          for all edges e in G.incidentEdges(t) do
		for(int i=0; i<(int)adj_list->at(t).size(); i++) {
//10             o ← G.opposite(t,e)
			o = adj_list->at(t)[i];
//11             if o is not marked:
            if((*triangs_vec)[o].mark[threadIdx] == false && mathOp->distBetweenTriangs(points_vec, (*triangs_vec)[index], (*triangs_vec)[o]) <= radius ) {
//12                  mark o
//13                  enqueue o onto Q
				(*triangs_vec)[o].mark[threadIdx] = true;
				marked_vec[threadIdx][++endVec[threadIdx]] = o;
				Q[threadIdx][endQ[threadIdx]] = o;
				endQ[threadIdx] = (endQ[threadIdx]+1) % MAX_NUM_TRIANG_NEIGHBORS;
			}
		}
	}

	// clear marks
	for(int i=0; i<=endVec[threadIdx]; i++)
		(*triangs_vec)[marked_vec[threadIdx][i]].mark[threadIdx] = false;
	(*triangs_vec)[index].mark[threadIdx] = false;

	return num_neighbors;
}

void TriangleOperations::find_triangs(int vertex, int list_triangs[MAX_NUM_THREADS][MAX_NUM_TRIANG_NEIGHBORS], int endList[MAX_NUM_THREADS], int threadIdx, bool insertMyself) {
	endList[threadIdx]=-1;
	for(int it_t = 0; it_t < (*points_vec)[vertex].numTriangs; it_t++) // for all edges that share vertex 'vertex'
		list_triangs[threadIdx][++endList[threadIdx]] = (*points_vec)[vertex].triangs_vec[it_t];

}

void TriangleOperations::find_triangs(int index, int vertex, vector<int>& list_triangs, bool insertMyself) {
	set<int> s;
	set<int>::iterator it_s;


	int num;

	for(int it_e = 0; it_e < (*points_vec)[(*triangs_vec)[index].points_vec[vertex]].numEdges; it_e++) {// for all edges that share vertex 'vertex'
		num = (*edges_vec)[it_e].numTriangs;
		int iEdge = (*points_vec)[(*triangs_vec)[index].points_vec[vertex]].edges_vec[it_e];
		for(int it_t = 0; it_t<(*edges_vec)[iEdge].numTriangs; it_t++) { // for all triangles of each edge (two at most)
			int iTriang = (*edges_vec)[iEdge].triangs_vec[it_t];
			if(iTriang != index)
				s.insert(iTriang);
		}
	}

	if(insertMyself) list_triangs.push_back(index);
	for(it_s = s.begin(); it_s != s.end(); it_s++)
		list_triangs.push_back(*it_s);

}

void TriangleOperations::find_vertices(int vertex, int list_triangs[MAX_NUM_THREADS][MAX_NUM_TRIANG_NEIGHBORS], int endListTriangs[MAX_NUM_THREADS], int list_vertices[MAX_NUM_THREADS][MAX_NUM_TRIANG_NEIGHBORS], int endListVertices[MAX_NUM_THREADS], int threadIdx) {
	endListVertices[threadIdx]=-1;
	for(int i=0; i<=endListTriangs[threadIdx]; i++)
		for(int v=0; v<3; v++)
            if(vertex == (*triangs_vec)[list_triangs[threadIdx][i]].points_vec[v])
				list_vertices[threadIdx][++endListVertices[threadIdx]] = v;

	if(endListVertices[threadIdx] != endListTriangs[threadIdx]) {
		cout << "Something bad in find vertices!!! Different number of vertices and triangles!" << endl;
		//if(endListVertices[threadIdx] < endListTriangs[threadIdx])
		//	endListTriangs[threadIdx] = endListVertices[threadIdx];
		//else
		//	endListVertices[threadIdx] = endListTriangs[threadIdx];

		cout << "For (*points_vec)["<<vertex<<"] I have " <<(*points_vec)[vertex].numTriangs << " triangles: ";
		for(int i=0; i<(*points_vec)[vertex].numTriangs; i++)
			cout << (*points_vec)[vertex].triangs_vec[i] << "  ";

		cout << endl << "I found " << endListVertices[threadIdx] << " and found " << endListTriangs[threadIdx]<<" triangs. "<< endl;

		for(int t=0; t<=endListTriangs[threadIdx]; t++) {
			int tIdx=list_triangs[threadIdx][t];
			cout << "(*triangs_vec)["<<tIdx<<"]: " << (*triangs_vec)[tIdx].points_vec[0] << ";" <<
				 (*triangs_vec)[tIdx].points_vec[1] << ";" << (*triangs_vec)[tIdx].points_vec[2] << endl;
		}
		system("PAUSE");
	}
}

void TriangleOperations::find_vertices(int index, int vertex, vector<int>& list_triangs, vector<int>& list_vertices) {
	bool add;
	for(unsigned int i=0; i<list_triangs.size(); i++) {
		add = false;
		for(int v=0; v<3; v++) {
			if(cmp_points((*points_vec)[(*triangs_vec)[index].points_vec[vertex]], (*points_vec)[(*triangs_vec)[list_triangs[i]].points_vec[v]]) == true) {
				list_vertices.push_back(v);
				add = true;
			}
		}
	}
}

bool TriangleOperations::PointInTriangle(point p, point a, point b, point c) {
    if( sameSide(p,a, b,c) && sameSide(p,b, a,c) && sameSide(p,c, a,b) )
		return true;
    else
		return false;
}

int TriangleOperations::find_triang_from_pt(int y, int x) {
	point p, a, b, c;

	p.x = (float)x; p.y = (float)y; p.z = 0.0f;

	for(FastVec<triang>::iterator it_t = triangs_vec->begin(); it_t != triangs_vec->end(); it_t++) {
		a.x = (float)(*points_vec)[it_t->points_vec[0]].x;  a.y = (float)(*points_vec)[it_t->points_vec[0]].y;  a.z = 0;
		b.x = (float)(*points_vec)[it_t->points_vec[1]].x;  b.y = (float)(*points_vec)[it_t->points_vec[1]].y;  b.z = 0;
		c.x = (float)(*points_vec)[it_t->points_vec[2]].x;  c.y = (float)(*points_vec)[it_t->points_vec[2]].y;  c.z = 0;

		if(PointInTriangle(p, a, b, c))
			return it_t->idx;
	}
	return 0;
}

int TriangleOperations::getTriangFromPts(int points[3]) {
	int X = ((*points_vec)[points[0]].x+(*points_vec)[points[1]].x+(*points_vec)[points[2]].x)/3;
	int Y = ((*points_vec)[points[0]].y+(*points_vec)[points[1]].y+(*points_vec)[points[2]].y)/3;
	if(triangsIdx[Y][X] == -1) {
		cout << "triangsIdx[][] not yet initialized! Probably should first call createRegionsVec." << endl;
		system("PAUSE");
		return 0;
	}
	else
		return triangsIdx[Y][X];
}



/**************************************************************************************
**********************        Utility Functions        ********************************
**************************************************************************************/

bool TriangleOperations::sameSide(point p1, point p2, point a, point b) {
    point cp1 = mathOp->crossProduct(mathOp->pointSub(b,a), mathOp->pointSub(p1,a));
    point cp2 = mathOp->crossProduct(mathOp->pointSub(b,a), mathOp->pointSub(p2,a));
    if (mathOp->dotProduct(cp1, cp2) >= 0 )
		return true;
    else
		return false;
}

int TriangleOperations::sameTriangle( int points[3] ) {
    FastVec<point_t>::iterator it_p1 = (*points_vec).getIt( points[0] );
    FastVec<point_t>::iterator it_p2 = (*points_vec).getIt( points[1] );
    FastVec<point_t>::iterator it_p3 = (*points_vec).getIt( points[2] );

    for( int t1=0; t1<it_p1->numTriangs; t1++ ) {
        for( int t2=0; t2<it_p2->numTriangs; t2++ ) {
            for( int t3=0; t3<it_p3->numTriangs; t3++ ) {
                if( (it_p1->triangs_vec[t1] == it_p2->triangs_vec[t2]) && (it_p2->triangs_vec[t2] == it_p3->triangs_vec[t3]) )
                    return it_p1->triangs_vec[t1];
            }
        }
    }

    return -1;
}

bool TriangleOperations::cmp_points(point_t& p1, point_t& p2) {
	return (p1.x == p2.x && p1.y == p2.y);
}

int TriangleOperations::calcTriangleArea(int it) {
	int x1 = (*points_vec)[(*triangs_vec)[it].points_vec[0]].x;
	int y1 = (*points_vec)[(*triangs_vec)[it].points_vec[0]].y;
	int x2 = (*points_vec)[(*triangs_vec)[it].points_vec[1]].x;
	int y2 = (*points_vec)[(*triangs_vec)[it].points_vec[1]].y;
	int x3 = (*points_vec)[(*triangs_vec)[it].points_vec[2]].x;
	int y3 = (*points_vec)[(*triangs_vec)[it].points_vec[2]].y;

	return abs(-x2*y1 + x3*y1 + x1*y2 - x3*y2 - x1*y3 + x2*y3)/2;
}

void TriangleOperations::order_points(int points[3], int order[3]) {
	int up, low;

	if((*points_vec)[points[0]].y == (*points_vec)[points[1]].y && (*points_vec)[points[0]].y == (*points_vec)[points[2]].y) {
		order[0] = 0;  order[1] = 1;  order[2] = 2;
		return;
	}

	if((*points_vec)[points[0]].y > (*points_vec)[points[1]].y) {
		if((*points_vec)[points[0]].y > (*points_vec)[points[2]].y)
			up = 0;
		else
			up = 2;
	}
	else {
		if((*points_vec)[points[1]].y > (*points_vec)[points[2]].y)
			up = 1;
		else
			up = 2;
	}

	if((*points_vec)[points[0]].y < (*points_vec)[points[1]].y) {
		if((*points_vec)[points[0]].y < (*points_vec)[points[2]].y)
			low = 0;
		else
			low = 2;
	}
	else {
		if((*points_vec)[points[1]].y < (*points_vec)[points[2]].y)
			low = 1;
		else
			low = 2;
	}
	order[0] = low;
	order[2] = up;
	order[1] = 3 - (up + low);
}

float TriangleOperations::symBtwTriangs(int t1, int t2) {
    float d = mathOp->distBetweenTriangs(points_vec, (*triangs_vec)[t1], (*triangs_vec)[t2]);
    int idx1 = (*regions_vec)[t1].depth.size()-1;
    int idx2 = (*regions_vec)[t2].depth.size()-1;
    float disp = abs((*regions_vec)[t1].depth[idx1] - (*regions_vec)[t2].depth[idx2]);

    float color = sqrt(((*regions_vec)[t1].meanLab[0] - (*regions_vec)[t2].meanLab[0])*((*regions_vec)[t1].meanLab[0] - (*regions_vec)[t2].meanLab[0]) +
                  ((*regions_vec)[t1].meanLab[1] - (*regions_vec)[t2].meanLab[1])*((*regions_vec)[t1].meanLab[1] - (*regions_vec)[t2].meanLab[1]) +
                  ((*regions_vec)[t1].meanLab[2] - (*regions_vec)[t2].meanLab[2])*((*regions_vec)[t1].meanLab[2] - (*regions_vec)[t2].meanLab[2]));

    //return exp(-color/15.0f)*exp(-d/350.0f)*exp(-disp/2.0f);
    return exp(-color/5.0f);
}
