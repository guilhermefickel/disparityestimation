#include "Timer.h"

void Timer::setName(const char* name) {
    strcpy(this->name, name);
}

void Timer::start(){
    gettimeofday(&startTime, NULL);
}

void Timer::pause() {
    gettimeofday(&lastTime, NULL);
}

void Timer::init() {
    gettimeofday(&startTime, NULL);
    lastTime = startTime;
}

void Timer::cont() {
    timeval aux;
    gettimeofday(&aux, NULL);
    startTime.tv_sec = aux.tv_sec-(lastTime.tv_sec-startTime.tv_sec);
    startTime.tv_usec = aux.tv_usec-(lastTime.tv_usec-startTime.tv_usec);
    lastTime.tv_sec = aux.tv_sec;
    lastTime.tv_usec = aux.tv_usec;
}

double Timer::stop(){
    timeval endTime;
    long seconds, nseconds;
    double duration;

    gettimeofday(&endTime, NULL);

    seconds  = endTime.tv_sec  - startTime.tv_sec;
    nseconds = endTime.tv_usec - startTime.tv_usec;

    duration = seconds + nseconds/1000000.0;

    return duration;
}

double Timer::totalTime(){
    long seconds, nseconds;
    double duration;

    seconds  = lastTime.tv_sec  - startTime.tv_sec;
    nseconds = lastTime.tv_usec - startTime.tv_usec;

    duration = seconds + nseconds/1000000.0;

    return duration;
}

void Timer::printTime(double duration){
    //printf("\n\n%s-> %5.6f seconds.\n", name, duration);
}

void Timer::printTime(){
    printf("\n\n%s-> %5.6f seconds.\n", name, totalTime());
}
