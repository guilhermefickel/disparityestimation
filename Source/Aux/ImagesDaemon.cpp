#include "ImagesDaemon.h"

ImagesDaemon::ImagesDaemon() {
	currFrame = 1;
	tempWindow = 10;
	imgOp = new ImgOp;
}

ImagesDaemon::~ImagesDaemon() {

}

void ImagesDaemon::setConfig( sbl::Config* config ) {
	std::vector< cv::Mat > sideImgs;
	this->config = config;
	imgName = config->readString( "imgName" );
	centerCam = config->readInt( "centerCam" );
	leftCam = config->readInt( "leftCam" );
	numSideCams = config->readInt( "numSideCams" );
	firstFrame = currFrame = config->readInt( "initialFrame", 0 );
	numFrames = config->readInt( "numFrames", 0 );
	tempWindow = config->readInt( "tempWindow", 1 );
	maxNumSideCams = ( abs(leftCam - centerCam) < ceil( (float)numSideCams/2.0f ) ) ? numSideCams - abs(leftCam - centerCam) : abs(leftCam - centerCam);

	if( leftCam < centerCam )
		inverseCamNumeration = false;
	else
		inverseCamNumeration = true;

    // numFormatParams = imgName.howMany( sbl::String("*d") );
    // BUG: Hardcoded formatation
    std::cout << "numFormatParams SETTING THIS VALUE: " << (numFormatParams = 2) << std::endl;
    //cout << "INPUT img: |" << imgName.c_str() << "| contei " << numFormatParams << endl;

	for(int i=0; i<tempWindow+1; i++) {
		centerImgVec.push_back( cv::Mat::zeros( 10, 10, CV_8UC3 ) );
        blurredCenterImgVec.push_back( cv::Mat::zeros( 10, 10, CV_8UC3 ) );
        labCenterImgVec.push_back( cv::Mat::zeros( 10, 10, CV_8UC3 ) );
        for(int j=0; j<numSideCams; j++) {
			sideImgs.push_back( cv::Mat::zeros( 10, 10, CV_8UC3 ) );
        }
        blurredSideVec.push_back( sideImgs );
        labSideVec.push_back( sideImgs );
        sideVec.push_back( sideImgs );
		sideImgs.clear();
	}

	readImgs();
}

sbl::String ImagesDaemon::getImName(int centerCam, int frameNum) {
	sbl::String name;
    sbl::String sFormat;
    int space[3];
    char c_sFormat[1024];

    sFormat = config->readString( "stringFormat" );

    strcpy( c_sFormat, sFormat.c_str() );

    for( int i=0; i<numFormatParams; i++ )
        space[i] = int(c_sFormat[i]) - 48;


	if( frameNum < firstFrame )
		frameNum = firstFrame;

    switch( numFormatParams ) {
        case 1: name = sbl::sprintF( imgName.c_str(), space[0], centerCam ); break;
        case 2: name = sbl::sprintF( imgName.c_str(), space[0], centerCam, space[1], frameNum ); break;
        case 3: name = sbl::sprintF( imgName.c_str(), space[0], centerCam, space[1], centerCam, space[2], frameNum ); break;
        default: cout << "Incorrect image format. Cant read image with " << numFormatParams << " parameters." << endl; break;
    }

   	return name;
}

cv::Mat* ImagesDaemon::getCenterImg() {
	return &centerImgVec.at(1);
}

cv::Mat* ImagesDaemon::getBlurredCenterImg() {
    return &blurredCenterImgVec.at(1);
}

cv::Mat* ImagesDaemon::getLabCenterImg() {
    return &labCenterImgVec.at(1);
}

cv::Mat ImagesDaemon::getImg( int frameIndex ) {
	return centerImgVec[ frameIndex - (currFrame-firstFrame) ];
}

std::vector<cv::Mat>* ImagesDaemon::getSideImgs() {
	return &sideVec.at(1);
}

std::vector<cv::Mat>* ImagesDaemon::getBlurredSideImgs() {
    return &blurredSideVec.at(1);
}

std::vector<cv::Mat>* ImagesDaemon::getLabSideImgs() {
    return &labSideVec.at(1);
}

void ImagesDaemon::nextFrame() {
	currFrame++;
	readImgs();
}

void ImagesDaemon::readImgs() {
	int count_im;
    int inc = (leftCam <= centerCam ? 1 : -1);
	int lastCam = (inc==1 ? numSideCams+leftCam+1 : leftCam-numSideCams-1);

	int firstFrameRead = currFrame-1;
	int lastFrameRead = currFrame+tempWindow;

    for(int f=firstFrameRead; f<lastFrameRead; f++) {
		for(int c=leftCam; c!=lastCam; c+=inc) {
			cv::Mat aux;
            if( leftCam <= centerCam )
				count_im = (c<centerCam ? count_im=c-leftCam : c-leftCam-1);
			else
				count_im = (c>centerCam ? count_im=leftCam-c : leftCam-c-1);

            if(c == centerCam) {
                centerImgVec[f-firstFrameRead] = imgOp->safeOpenImage( getImName( c, f ).c_str() );
                blurredCenterImgVec[f-firstFrameRead] = imgOp->safeOpenImage( getImName( c, f ).c_str(), 0, true );
                labCenterImgVec[f-firstFrameRead] = imgOp->safeOpenImageLab( getImName( c, f ).c_str(), 0, false );
            }
			else {
                aux = imgOp->safeOpenImage( getImName( c, f ).c_str() );
				sideVec[f-firstFrameRead][count_im] = aux;
                aux = imgOp->safeOpenImage( getImName( c, f ).c_str(), 0, true );
                blurredSideVec[f-firstFrameRead][count_im] = aux;
                aux = imgOp->safeOpenImageLab( getImName( c, f ).c_str(), 0, false );
                labSideVec[f-firstFrameRead][count_im] = aux;
			}
		}
	}
}

int ImagesDaemon::getCurrentFrame() {
	return currFrame;
}

int ImagesDaemon::getCurrentTime() {
	return currFrame-firstFrame;
}

int ImagesDaemon::getFirstFrame() {
	return firstFrame;
}

int ImagesDaemon::getTempWindow() {
	return tempWindow;
}

int ImagesDaemon::getMaxNumSideCams() {
	return maxNumSideCams;
}

int ImagesDaemon::getNumCameras() {
	return numSideCams;
}
