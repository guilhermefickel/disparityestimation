#include "region.h"
#include <iostream>

region::region() {
	numPts=0; // number os pts, i.e. its area
	used=false; // was deleted?
	stable=false; // is temporally stable?
}

region::~region() {

}

region& region::operator=(const region &rhs) {
	// Only do assignment if RHS is a different object from this.
	if (this != &rhs) {
		std::cout << "Hei, this is way to slow! Do not use the operator =  to copy regions!!" << std::endl;
		system("PAUSE");
	}

	return *this;
}