#ifndef RASTER_H
#define RASTER_H

#include "FastVec.h"
#include "region.h"
#include "Constants.h"
#include <iostream>


class Raster {
private:
	FastVec<region>* regions_vec;
	//int ContourX[512][2];
	int*** ContourX;
	int* deltaY;
	int top, bot;
    int minINT, maxINT;
	//int currentTriang;

	void scanLine(int threadPos, int x1, int y1, int x2, int y2);

public:
	Raster();
	~Raster();

	void setRegions(FastVec<region>* regions_vec);
	void line(int x0, int y0, int x1, int y1);
	void setTriangle(int it, int threadPos=0);
	void setTriangle(int y1, int x1, int y2, int x2, int y3, int x3, int threadPos=0);
	void getHorizLimits(int row, int& left, int& right, int threadPos=0);
	void getVertLimits(int &top, int &bot);
};


#endif
