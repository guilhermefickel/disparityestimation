#include "edge.h"

edge::edge() {
	numTriangs = 0; 
	used = false;
}

edge::~edge() {

}

edge& edge::operator=(const edge &rhs) {
	// Only do assignment if RHS is a different object from this.
	if (this != &rhs) {
		this->numTriangs = rhs.numTriangs;
		this->used = rhs.used;
		
		for(int i=0; i<rhs.numTriangs; i++)
			this->triangs_vec[i] = rhs.triangs_vec[i];

		this->points_vec[0] = rhs.points_vec[0];
		this->points_vec[1] = rhs.points_vec[1];
	}

	return *this;
}