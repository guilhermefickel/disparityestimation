#ifndef QT_UTIL_H
#define QT_UTIL_H

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <qimage.h>
#include <qcolor.h>
#include <vector>

class QT_Util {
private:

public:
	QT_Util();
	~QT_Util();

	cv::Mat QImage32Mat(QImage &src);
    QImage& Mat2QImage3(cv::Mat &src);
    QImage& Mat2QImage1(cv::Mat &src);
	QImage cropPixmap(QImage& img, int topleftX, int topleftY, int botrightX, int botrightY);

};

#endif
