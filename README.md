#### Disparity Estimation Using Image Domain Triangulation

This code was made during my PhD, and it is quite messy. It was my programming playground, so there are lot's of stuff that I tried that perhaps didn't pan out great such as my FastVec data structure, using SBL for configuration input, QT for interface, etc.

I'm trying to let this code cleaner to release it to public. I will try to do the following changes:

* Use JSON for input configuration and remove SBL dependency
* Change configuration format to be more user friendly
* Change some hardcoded variables to be on config


This code was mainly developed on VisualStudio, however I am now using it on Linux and it compiles/works just fine.